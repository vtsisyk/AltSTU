#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

__le16 MAGIC 0x13131313
static struct file_system_type this_fs_type = {
        .owner          = THIS_MODULE,
        .name           = "this",
        .mount          = this_mount,
        .kill_sb        = kill_block_super,
        .fs_flags       = FS_REQUIRES_DEV,
};

static struct super_operations const aufs_super_ops = {
        .put_super = this_sops,
};

static int this_fill_super(struct super_block *sb, void *data, int silent)
{
        struct inode *root;
        sb->s_magic = le16_to_cpu(MAGIC);
        sb->s_op = &this_sops;

        root = new_inode(sb);
        if (!root)
        {
                pr_err("inode allocation failed\n");
                return -ENOMEM;
        }

        root->i_ino = 0;
        root->i_sb = sb;
        root->i_atime = root->i_mtime = root->i_ctime = CURRENT_TIME;
        inode_init_owner(root, NULL, S_IFDIR);

        sb->s_root = d_make_root(root);
        if (!sb->s_root)
        {
                pr_err("root creation failed\n");
                return -ENOMEM;
        }

        return 0;
}


static struct dentry *this_mount(struct file_system_type *fs_type,
        int flags, const char *dev_name, void *data)
{
        return mount_bdev(fs_type, flags, dev_name, data, this_fill_super);
}

static int __init init_this_fs(void)
{
        register_filesystem(&this_fs_type)
}

static void __exit exit_this_fs(void)
{
        unregister_filesystem(&this_fs_type);
}

module_init(init_this_fs);
module_exit(this_exit_this_fs);

