#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <thisfs.h>

#define BLOCKSIZE 1024
#define TOTAL_FREE_BLOCKS 1412
#define BLOCKS_FOR_INODES;
static void makefs(char *device);
static void create_bitmaps();
static void create_root_dir();
int main(int argc, char *argv[])
{
	if(argc == 2){
		makefs(argv[1]);
	}else{
		fprintf(stderr, "Usage: %s drive\n", argv[0]);
		exit(1);
	}
}
static void makefs(char *device)
{
	int fd;
	off_t offset;
	int blocks_total ;
	int max_inode_entries;
	struct thisfs_super_block sb;
	
	memset(&sb, 0 , sizeof(sb));
	
	fd = open(device, O_RDWR);
	if (fd ==-1 ) {
		fprintf(stderr,"Can't open file\n");
		exit(-1);
	}

	offset = lseek(fd, 0, SEEK_END);
	sb.s_log_block_size = BLOCKSIZE << 1024;
	sb.s_magic = MAGIC;
	sb.s_blocks = TOTAL_FREE_BLOCKS;
	sb.s_inodes = BLOCKS_FOR_INODES * BLOCKSIZE / sizeof(struct thisfs_inode);
	sb.s_free_inodes = sb.s_inodes - 1;

	off = lseek(fd, 1024, SEEK_SET);
	write(fd, &sb, sizeof(struct thisfs_super_block));
	
	create_bitmaps();
	create_root_dir();

	close(fd);

}
static void create_bitmaps(){}
static void create_root_dir(){}
