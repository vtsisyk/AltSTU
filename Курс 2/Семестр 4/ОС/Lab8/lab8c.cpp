#include <iostream>
#include <queue>
#include <algorithm>
#include <vector>
#include <stdlib.h>
#include <ctime>

static const int quant = 15;

/* флаги состояний */
enum state{WAIT, READY, PROCESSING, DEAD};
struct task_struct_model {
	
	short state;		// состояние процесса
	int index;		// номер процесса. Можно использовать уникальное "имя"
	int time_needed;	// сколько времени требуется для работы
	int time_arrival;	// время прибытия
	int time_left;		// сколько осталось работать

	friend bool operator <(task_struct_model & a, task_struct_model & b){
		return a.time_arrival < b.time_arrival ? true : false;
	}
};


int model_init(int processes, std::vector<task_struct_model> *vec);
int model_work(std::vector<task_struct_model> vec);

int main(int argc, char *argv[]){
	unsigned processes;
	std::cout << "Введите количество процессов: ";
	std::cin >> processes;
	std::vector<task_struct_model> vec;
	struct task_struct_model buf;
	model_init(processes, &vec);
	sort(vec.begin(), vec.end());	
	model_work(vec);
	std::cout<< "-==================-\n";	
	std::cout<< "Quant of time: " << quant << "\n";	
	std::cout<< "-==================-\n";	
	std::cout << "Processes stats\n";
	for(int i = 0; i < vec.size(); i++){
		std::cout<< "Process index: " << vec[i].index << "\n";
		std::cout<< "Time required: " << vec[i].time_needed << "\n";
		std::cout<< "Time of arrival: " << vec[i].time_arrival << "\n";
		std::cout<< "-==================-\n";	
	}
}

/* Создаем новые процессы со случайным требуемым временем работы */
int model_init(int processes, std::vector<task_struct_model> *vec)
{
	struct task_struct_model buf;
	srand (time(NULL));
	for(int i = 0; i < processes; i++ ){
		buf.state = READY;
		buf.index = i;
		buf.time_needed =  rand() % 20 + 5;
		buf.time_arrival = rand() % 10;
		buf.time_left = buf.time_needed;
		vec->push_back(buf);		
	}
	return 0;
}

/* имитация работы процессов */
int model_work(std::vector<task_struct_model> vec)
{

	struct task_struct_model buf;
	int time_of_work;	// сколько будет работать процесс
	int time = 0;
	char wait;

	/* запихиваем первый элемент в очередь */
	std::queue<task_struct_model> que;
	que.push(vec.front());
	vec.erase(vec.begin(), vec.begin()+ 1);

	time = que.front().time_arrival;
	while(1){

		/* может быть случай, когда очередь пуста, а не все процессы обработаны */
		if(que.empty() && vec.empty())
			break;

		buf = que.front();	

		/* считаем время работы */
		if(buf.time_left <= quant){
			time_of_work = buf.time_left;
		}else if(buf.time_left > quant){
			time_of_work = quant;
		}else if (!wait){
			/* Может возникнуть необходимость во время работы процесса отправить его в режим ожидания.
			 * Случайное значение переменной 0 <= time_of_work <= quant говорит о том, что посередине обработки
			 * процесса такая необходимость возникла.
			 */
			time_of_work = 0;
		}	

		std::cout<< "Processing from " << time << " to " << time + time_of_work <<". Process index: " << buf.index << "\n";
		/* процесс получил процессорное время */
		buf.state = PROCESSING;
		
		/* случайным образом определяем, нужно ли отправить процесс в режим ожидания  */
		wait = rand() % 10;
		if(buf.time_left == time_of_work){
			/* процесс закончился */
			buf.state = DEAD;
			std::cout<< "Process №: " << buf.index<< " is DEAD" <<"\n" ;
		} else {
			if(!wait){
				buf.state = WAIT;
			} else{
				buf.state = READY;
			}
		}
		/* отнимаем отработавшее время */
		buf.time_left -= time_of_work;

		time += time_of_work;

		/* Поступили ли новые процессы? */
		for(int i = 0 ; i < vec.size(); i++){
			if(vec[i].time_arrival <= time ){
				que.push(vec.front());
				vec.erase(vec.begin(), vec.begin()+ 1);
			}
		}
		// выпихиваем из очереди обработанный процесс.
		que.pop();
		if(buf.state != DEAD)
			que.push(buf);
	}
}
