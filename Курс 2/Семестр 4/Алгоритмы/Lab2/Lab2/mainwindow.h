#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "graph.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

signals:
	void setname(QString);
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
public slots:
	void read();
	void getanswer(unsigned);
	void getmax(unsigned);
	void getpicture();
	void getgraph();

private:
	Graph *te;
	Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
