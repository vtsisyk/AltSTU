#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent),ui(new Ui::MainWindow)
{
	te = new Graph();
	ui->setupUi(this);
	ui->spinBox->setMinimum(0);
	ui->label->setHidden(true);
	ui->label_5->setHidden(true);
	connect(ui->action, SIGNAL(triggered()),this, SLOT(read()));
	connect(this, SIGNAL(setname(QString)), te, SLOT(getname(QString)));
	connect(te, SIGNAL(setanswer(unsigned)), this, SLOT(getanswer(unsigned)));
	connect(ui->spinBox, SIGNAL(valueChanged(int)), te, SLOT(setstart(int)));
	connect(te, SIGNAL(setmax(unsigned)), this, SLOT(getmax(unsigned)));
	connect(te, SIGNAL(setpicture()), this, SLOT(getpicture()));
	connect(te, SIGNAL(setgraph()), this, SLOT(getgraph()));


}

void MainWindow::read()
{
	QString filename = QFileDialog::getOpenFileName(
	this,
	tr("Открыть файл"),
	tr("/home/vlad/Empty"), /* путь в файловой системе. "" для корня */
	tr("Файлы (*.txt)")
	);
	emit setname(filename);

}

void MainWindow::getanswer(int answer)
{
	ui->label_3->setText( QString::number(answer));
}
void MainWindow::getmax(unsigned max)
{
	ui->spinBox->setMaximum(max);
}

void MainWindow::getpicture()
{
	ui->label->setPixmap(QString("graph.dot.png"));
	ui->label->show();
	update();

}

void MainWindow::getgraph()
{
	ui->label_5->setPixmap(QString("graf.dot.png"));
	ui->label_5->show();
	update();


}
MainWindow::~MainWindow()
{
	delete ui;
}
