#include "graph.h"

Graph::Graph(QObject *parent) : QObject(parent)
{

}

void Graph::getname(QString name)
{
	file = new QFile(name);
	readdata();
}


/*
 * Читаем данные из файла. Проверяем каким образом задан список("a" - для
 * матрицы смежности, "i" - для списка вершин и дуг).
 * Соотвествующие данные пихаем в разные виды контейнеров.
 * Запускаем алгоритм поиска минимального каркаса из первой(нулевой) вершины.
 */

void Graph::readdata()
{
	adj.clear();
	inc.clear();
	QRegExp rx("[\\s]");
	if(file->open(QIODevice::ReadOnly)){
		QTextStream in(file);
		QString line = in.readLine().trimmed();
		if(line == "a"){
			adjacency = 1;
			while(!in.atEnd()){
				QString line = in.readLine().trimmed();
				QStringList query = line.split(rx);
				QVector<int> tmp;
				for(int i = 0; i < query.size(); i++)
					tmp.append(query.at(i).toInt());
				adj.append(tmp);
			}
			emit setmax(adj.size() - 1);
		} else if(line == "i"){
			adjacency = 0;
			while(!in.atEnd()){
				QString line = in.readLine().trimmed();
				QStringList query = line.split(rx);
				QVector<QPair <int, int> >  tmp;
				for(int i = 0; i < query.size(); i+=2)
					tmp.append(qMakePair(query.at(i).toInt(), query.at(i+1).toInt() ));
				inc.append(tmp);
			}
			emit setmax(inc.size() - 1);
		}
	}
	file->close();
	drawgraph();
	setstart(0);
	/*
	if(adjacency)
		ad_dijkstra(0);
	else
		inc_dijkstra(0);
	*/
}

/*
 * Работаем с матрицей смежности.
 * Используем алгоритм Прима( ).
 * По пути записываем это дело в DOT файл и создаем картинку. Возможно, следовало
 * сделать svg, а не png.
 */

int Graph::adj_prim(int start)
{

	int n = adj.size();
	if(!n){
		qDebug()  << "Пустой граф";
		return 1;
	}

	ofstream file;
	file.open ("graf.dot");

	//cout<<"Решение: ";
	file << "graph { \n" \
	<< "graph [overlap = fasle, size =\"10, 8!\",fontsize=8] ;\n"\
	<< "node [fontsize=8,shape=circle, fixedsize = true,  margin=0, width=0.2, style=filled];\n"\
	<< "edge [fontsize=8];\n"\
	<< "splines = true;\n"\
	<< "overlap=false;\n"\
	<< "nodesep=2;\n"\
	<< "sep=\"+15,25\";\n";


	const int INF = INT_MAX; // значение "бесконечность"

	// алгоритм
	QVector<bool> used (n);
	QVector<int> min_e (n, INF), sel_e (n, -1);
	int sum = 0;
	min_e[start] = 0;
	for (int i=0; i<n; ++i) {
		int v = -1;
		for (int j=0; j<n; ++j)
			if (!used[j] && (v == -1 || min_e[j] < min_e[v]))
				v = j;
		if (min_e[v] == INF) {
			qDebug() << "No way!";
			exit(0);
		}

		used[v] = true;
		if (sel_e[v] != -1){
			file<<"\t"<< sel_e[v] <<" -- " << v << "[xlabel= " <<adj[sel_e[v]][v] << ", fontcolor=red]"<<"\n";
			qDebug() << sel_e[v] << " " << v;
			sum+= adj[sel_e[v]][v];
		}

		for (int to=0; to<n; ++to)
			if (adj[v][to] < min_e[to]) {
				min_e[to] = adj[v][to];
				sel_e[to] = v;
			}
	}

	qDebug() << "Длина: " <<  sum ;
	emit setanswer(sum);
	emit setanswer(sum);
	file << "}";
	file.close();
	system("neato -Tpng graf.dot -O ");
	emit setgraph();

	return 0;

}

/* Работаем со списком вершин и дуги.  По пути создаем DOT-файл. */

int Graph::inc_prim(int start)
{
	int n = inc.size();
	if(!n){
		qDebug()  << "Пустой граф";
		return 1;
	}
	ofstream file;
	file.open ("graf.dot");

	//cout<<"Решение: ";
	file << "graph { \n" \
	<< "graph [overlap = false,size =\"8, 8!\",fontsize=8] ;\n"\
	<< "node [fontsize=8,shape=circle,margin=0, width=0.2, style=filled];\n"\
	<< "edge [fontsize=8];\n"\
	<< "splines = true;\n"\
	<< "overlap=false;\n"\
	<< "nodesep=2;\n"\
	<< "sep=\"+15,25\";\n";


	const int INF = INT_MAX; // значение "бесконечность"

	// алгоритм
	QVector<bool> used (n);
	QVector<int> min_e (n, INF), sel_e (n, -1);
	int sum = 0;
	min_e[start] = 0;
	for (int i=0; i<n; ++i) {
		int v = -1;
		for (int j=0; j<n; ++j)
			if (!used[j] && (v == -1 || min_e[j] < min_e[v]))
				v = j;
		if (min_e[v] == INF) {
			qDebug() << "No way!";
			exit(0);
		}

		used[v] = true;
		if (sel_e[v] != -1){
			qDebug()<< sel_e[v] << " " << v;
			sum += min_e[v];
			file<<"\t"<< sel_e[v] <<" -- " << v << "[xlabel= " << min_e[v] << ", fontcolor=red]"<<"\n";
		}

		for (int j=0; j<inc[v].size(); j++){
			int to = inc[v][j].first;
			int cost = inc[v][j].second;
			if (cost < min_e[to]) {
				min_e[to] = cost;
				sel_e[to] = v;
			}
		}
	}

	qDebug() << "Длина: " <<  sum ;
	emit setanswer(sum);
	file << "}";
	file.close();
	system("neato -Tpng graf.dot -O ");
	emit setgraph();

	return 0;
}
/*Алгоритм Дейкстры для обоих видов графов :) */
/*
void Graph::ad_dijkstra(int start)
{
	const int INF = INT_MAX;
	int n = adj.size();

	vector<int> d (n, INF),  p (n);
	d[start] = 0;
	vector<char> u (n);

	for (int i=0; i<n; ++i) {
		int v = -1;
		for (int j=0; j<n; ++j)
			if (!u[j] && (v == -1 || d[j] < d[v]))
				v = j;
		if (d[v] == INF)
			break;
		u[v] = true;

		for (int j=i; j<adj[v].size(); ++j) {
			int to = j;
			int len = adj[v][j];
			if (d[v] + len < d[to]) {
				d[to] = d[v] + len;
				p[to] = v;
			}
		}
	}

	unsigned length = 0;
	for(unsigned i =0; i < n; i++)
		length += d[i];

	emit setanswer(length);

	for(unsigned i =0; i < d.size(); i++)
		cout << d[i] << "\n";
	vector<int> path;
	for (unsigned v=3; v!=start; v=p[v])
			path.push_back (v);
	path.push_back (start);
	reverse (path.begin(), path.end());
	for(unsigned i =0; i < path.size(); i++)
		cout << path[i] << "\n";

}

void Graph::inc_dijkstra(int start)
{
	const int INF = INT_MAX;
	int n = inc.size();
	vector<int> d (n, INF),  p (n);
	d[start] = 0;
	vector<char> u (n);

	for (int i=0; i<n; ++i) {
		int v = -1;
		for (int j=0; j<n; ++j)
			if (!u[j] && (v == -1 || d[j] < d[v]))
				v = j;
		if (d[v] == INF)
			break;
		u[v] = true;


		for (int j=0; j<inc[v].size(); ++j) {
			int to = inc[v][j].first,
				len = inc[v][j].second;
			if (d[v] + len < d[to]) {
				d[to] = d[v] + len;
				p[to] = v;
			}
		}
	}

	unsigned length = 0;
	for(unsigned i =0; i < n; i++)
		length += d[i];

	emit setanswer(length);

	vector<int> path;
	for (unsigned v=3; v!=start; v=p[v])
			path.push_back (v);
	path.push_back (start);
	reverse (path.begin(), path.end());
	for(unsigned i =0; i < path.size(); i++)
		cout << path[i] << "\n";
}
*/


/* Это вспомогательные функции, связанные с вызовом той или иной функции и
 * заполнением контейнера
 */

void Graph::setstart(int s)
{
	start = s;
	prim();
}
void Graph::prim(){
	if(adjacency)
		adj_prim(start);
	else
		inc_prim(start);

}

void Graph::drawgraph()
{
	if(adjacency)
		adj_graph();
	else
		inc_graph();

}

void Graph::adj_graph()
{

	ofstream file;
	file.open ("graph.dot");

	//cout<<"Решение: ";
	file << "graph { \n" \
	<< "graph [size =\"8, 8!\" overlap = false,fontsize=8] ;\n"\
	<< "node [fontsize=8,shape=circle,margin=0, width=0.2, style=filled];\n"\
	<< "edge [fontsize=8];\n"\
	<< "splines = true;\n"\
	<< "overlap=false;\n"\
	<< "nodesep=2;\n"\
	<< "sep=\"+15,25\";\n";
	for(int i = 0; i < adj.size(); i++)
		for(int j = i+1; j < adj.size();j++){
			if(adj[i][j] < 100)
			file<<"\t"<< i <<" -- " << j << "[xlabel= " << adj[i][j] << ", fontcolor=red]"<<"\n";
		}
	file << "}";
	file.close();
	system("neato -Tpng graph.dot -O ");
	emit setpicture();
	emit setgraph();
}

void Graph::inc_graph()
{

	ofstream file;
	file.open ("graph.dot");

	//cout<<"Решение: ";
	file << "graph { \n" \
	<< "graph [overlap = false, size =\"8, 8!\",  fontsize=8] ;\n"\
	<< "node [fontsize=8,shape=circle,margin=0, width=0.2,style=filled ];\n"\
	<< "edge [fontsize=8];\n"\
	<< "splines = true;\n"\
	<< "overlap=false;\n"\
	<< "nodesep=2;\n"\
	<< "sep=\"+15,25\";\n";
	bool visited[inc.size()];
	for(int i = 0; i < inc.size(); i++)
		visited[i] = -1;
	visited[0]=0;
	for(int i = 0; i < inc.size(); i++){
		for(int j = 0; j < inc[i].size();j++){
			if(visited [inc[i][j].first] != 1 )
				file<<"\t"<< i <<" -- " << inc[i][j].first << "[xlabel= " << inc[i][j].second << ",fontcolor=red]"<<"\n";
		}
		visited[i] = 0 ;
	}
	file << "}";
	file.close();
	system("neato -Tpng graph.dot -O ");
	emit setpicture();
	emit setgraph();
}
