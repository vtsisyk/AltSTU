#ifndef GRAPH_H
#define GRAPH_H

#include <QObject>
#include <QString>
#include <QDebug>
#include <QFileDialog>
#include <QVector>
#include <QTextStream>
#include <limits.h>
#include <QPair>
#include <fstream>

using namespace std;
class Graph : public QObject
{
	Q_OBJECT
public:
	explicit Graph(QObject *parent = 0);
	void readdata();
	void drawgraph();
	void adj_graph();
	void inc_graph();

private:
	bool adjacency; /* что даем на вход */
	unsigned start = 0;
	QFile *file; /* файл, из которого читаем */
	QVector<QVector<int> > adj; /* матрица смежности */
	QVector<QVector<QPair <int, int > > > inc; /* вершины-ребра*/
signals:
	void setanswer(unsigned);
	void setmax(unsigned);
	void setpicture();
	void setgraph();
public slots:
	void getname(QString);
	void setstart(int);
	void prim();

	int adj_prim(int);
	int inc_prim(int start);
};

#endif // GRAPH_H
