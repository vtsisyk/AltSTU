
#include "graph.h"
#include <QDebug>
Graph::Graph(int _V):QObject()
{
	V = D =_V;
}
Graph::Graph(int _V, int _D):QObject()
{
	V = _V;
	D = _D;
}

/* Проверка цикла на эйлеровость
 * 0 --> граф не эйлеров
 * 1 --> В графе есть эйлеров путь
 * 2 --> граф эйлеров
 */
int Graph::isEulerian()
{
	
	/* количество нечетных вершин */
	int odd = 0;
	int tmp;
	
	for (int i = 0; i < V; i++){
		tmp = 0;
		for(int j = 0; j < V; j++)
			if(adjacency[i][j] != 0)
				tmp++;
		if (tmp & 1)
			odd++;
	}
	/* если их больше двух, то граф не эйлеров */
	if (odd > 2)
		return 0;
	else if(odd)
		return 1;
	else
		return 2;
	//return (odd)? 1 : 2;
}

/*
 * Проверка графа
 */
void Graph::test()
{
	int res = this->isEulerian();
	if (res == 0)
		emit mesg("Граф не является эйлеровым(нет)");
	else if (res == 1)
		emit mesg("В графе есть эйлеров путь(да)");
	else
		emit mesg("В графе есть эйлеров цикл(да)");
}

bool Graph::isSafe(int v, int path[], int pos)
{
	if (adjacency [path[pos-1]][v] == 0)
		return false;
	for (int i = 0; i < pos; i++)
		if (path[i] == v)
			return false;
	return true;
}


bool Graph::hamCycleUtil( int path[], int pos)
{
	if (pos == V){
		if (adjacency[path[pos-1]][ path[0] ] == 1){
			return true;
		}else
			return false;
	}

	for (int v = 1; v < V; v++){
		if (isSafe(v, path, pos)){
			path[pos] = v;
			if (hamCycleUtil (path, pos+1) == true){
				return true;
			}
			path[pos] = -1;
		}
	}
	return false;
}

bool Graph::hamCycle()
{
	int *path = new int[V+1];
	for (int i = 0; i < V; i++)
		path[i] = -1;

	path[0] = 0;

	if (hamCycleUtil(path, 1) == false){
		cout<<"\nПутей нет"<<endl;
		return false;
	}
	path[V] = 0;
	printSolution(path);
	return true;
}
bool Graph::inc_hamCycle()
{
	int *path = new int[V+1];
	bool *versh_used = new bool[12];
	for(int i = 0; i < 12; i++)
		versh_used[i] = 0;
	for (int i = 0; i < V; i++)
		path[i] = -1;

	path[0] = 0;
	if (inc_hamCycleUtil(path, versh_used, 1) == false){
		cout<<"\nПутей нет"<<endl;
		return false;
	}
	path[V] = 0;
	haspath(0, V-1, versh_used);

	printSolution(path, versh_used);
	return true;
}
bool Graph::haspath(int from, int where, bool versh_used[])
{
	for(int i = 0; i < D; i++)
		for(int j = 0; j < D; j++)
			if(incidence[from][i] == incidence[where][j]){
				versh_used[incidence[from][i] ] = 1;
				return true;
			}
	return false;
}
bool Graph::inc_hamCycleUtil(int path[], bool versh_used[], int pos)
{

	if(pos == V)
		if(haspath(pos,path[0], versh_used))
			return true;
	if(haspath(pos-1, pos, versh_used )){
		path[pos] = pos;
		inc_hamCycleUtil(path,versh_used, pos+1);
		return true;
	}

	return false;
}
bool Graph::all_visited(bool versh_used[])
{
	for(int i = 0; i < 12; i++)
		if(versh_used[i] == false)
			return false;
	return true;
}
int Graph::findpath(int from, int vertex){

	for(int i = from + 1; i < V; i++)
		for(int j = 0; j < D; j++)
			if(incidence[i][j] == vertex)
				return i+1;
	return -1;
}
void Graph::printSolution(int path[], bool versh_used[])
{
	int vertexes = 1;
	int vert = -1;
	ofstream file;
	file.open ("graph.dot");

	//cout<<"Решение: ";
	file << "digraph { \n" \
	     << "graph [dimen=3, size=\"30.7,8.3!\", resolution=100, fontsize=8] ;\n"\
	     << "node [fontsize=8,shape=circle,margin=0, width=0.2, ];\n"\
	     << "edge [fontsize=8, arrowsize=.4];\n"\
	 << "splines = true;\n"\
	     << "overlap=false;\n"\
	     << "nodesep=1.8;\n"\
	     << "sep=\"+15,15\";\n";

	bool hvatit = 0;
	for (int i = 0; i < V; i++){
		file<<"\t"<<path[i]<<" -> " << path[i+1] << "[label= " << vertexes << ",fontcolor=red]"<<"\n";
		vertexes++;
	}
		for(int i = 0; i < V; i++){
			for(int j = 0; j < 3; j++){
				if(versh_used[incidence[i][j]]  == false){
					file<<"\t"<<i<<" -> " << findpath(i, incidence[i][j]) - 1\
					<< "[label= " << vertexes << ", fontcolor=blue]" <<"\n";

					versh_used[incidence[i][j]] = true;

					vert++;
					vertexes++;

				}
				if(all_visited(versh_used)){
					hvatit = 1;
					break;
				}
			}
			if(hvatit)
				break;
		}


	file << "}";
	file.close();
	system("fdp -Tpng graph.dot -O");
	emit lom(vert);


}
void Graph::printSolution(int path[])
{
	int vertexes = 1;
	int vert = -1;
	ofstream file;
	file.open ("graph.dot");

    for(int i = 1; i < V  ; i++){
        for(int j = 0; j < i; j++){
            if(adjacency[path[i-1]][path[i]] ==1 ){
                adjacency[path[i-1]][path[i]] = 0;
                adjacency[path[i]][path[i-1]] = 0;
			}
		}
    }
    adjacency[0][V-1] = 0;
    adjacency[V-1][0] = 0;

	//cout<<"Решение: ";
	file << "digraph { \n" \
	     << "graph [dimen=3, size=\"30.7,8.3!\", resolution=100, fontsize=8] ;\n"\
	     << "node [fontsize=8,shape=circle,margin=0, width=0.2, ];\n"\
	     << "edge [fontsize=8, arrowsize=.4];\n"\
         << "splines = true;\n"\
	     << "overlap=false;\n"\
	     << "nodesep=1.8;\n"\
	     << "sep=\"+15,15\";\n";

	for (int i = 1; i < V+1; i++){
		file<<"\t"<<path[i-1]<<" -> " << path[i] << "[label= " << vertexes << ",fontcolor=red]"<<"\n";
		vertexes++;
	}

	for(int i = 0; i < V  ; i++){
		for(int j = 0; j < i; j++){
			if(adjacency[i][j] == 1 ){
				file<<"\t"<<j<<" -> " << i  << "[label= " << vertexes << ", fontcolor=blue]" <<"\n";
				vert++;
				vertexes++;
			}
		}
	}
	file << "}";
	file.close();
	system("fdp -Tpng graph.dot -O");
	emit lom(vert);


}
