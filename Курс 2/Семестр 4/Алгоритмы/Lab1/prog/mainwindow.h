#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <graph.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
	void showpic();
    void addmsg(QString);
    void lomer(int lom);


private:
    Graph *tet;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
