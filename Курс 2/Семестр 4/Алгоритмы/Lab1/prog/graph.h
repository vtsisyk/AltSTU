#ifndef GRAPH_H
#define GRAPH_H

#include<iostream>
#include <list>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <QtCore>
#include <vector>
using namespace std;
class Graph: public QObject
{
    Q_OBJECT
	/* матрица смежности для квадрата */


    bool adjacency [8][8] {\
	{0,1,0,1,0,0,0,1},\
	{1,0,1,0,0,0,1,0},\
	{0,1,0,1,0,0,1,0},\
	{1,0,1,0,1,0,0,0},\
	{0,0,0,1,0,1,0,1},\
	{0,0,1,0,1,0,1,0},\
	{0,1,0,0,0,1,0,1},\
	{1,0,0,0,1,0,1,0}
	};


	int incidence[8][3]{
		{1,8,9},\
		{1,2,11},\
		{2,3,12},\
		{3,4,9},\
		{4,5,10},\
		{5,6,12},\
		{6,7,11},\
		{7,8,10}
	};


    
     int V; /* число вершин */
     int D;
signals:
     void mesg(QString);
     void lom(int);
public:
	Graph(int _V);
	Graph(int _V, int _D);
	/* проверка графа на эйлеровость */
	int isEulerian();
	void test();
	bool isSafe(int v, int path[], int pos);
	bool hamCycleUtil(int path[], int pos);
	bool hamCycle();
	void printSolution(int path[]);
	void printSolution(int path[], bool versh_used[]);
	bool inc_hamCycleUtil( int path[], bool versh_used[], int pos);
	bool inc_hamCycle();
	bool haspath(int from, int where, bool versh_used[]);
	bool all_visited(bool versh_used[]);
	int findpath(int from, int vertex);
};

#endif // GRAPH_H
