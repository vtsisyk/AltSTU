#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <openglwgt.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),

    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label->setHidden(true);
    ui->label_3->setHidden(true);
    ui->label_2->setHidden(true);

    tet = new Graph(8,3);
    connect(tet, SIGNAL(mesg(QString)), this, SLOT(addmsg(QString)));
    connect(tet, SIGNAL(lom(int)), this, SLOT(lomer(int)));

    update();


}

void MainWindow::showpic()
{
	ui->label->setPixmap(QString("graph.dot.png"));
	ui->label->show();
	tet->test();
	tet->inc_hamCycle();
	update();
}

void MainWindow::addmsg(QString msg)
{
    ui->label_3->setText(ui->label_3->text() + msg);
    ui->label_3->setVisible(true);
    update();
}
void MainWindow::lomer(int lom)
{
    ui->label_2->setText(ui->label_2->text() + QString::number(lom));
    ui->label_2->setVisible(true);
    update();
}
MainWindow::~MainWindow()
{
    delete ui;
}


