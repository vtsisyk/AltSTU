#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#define STR_SIZE 1024 
#define INPUT "input"
#define OUTPUT "output"
#define TIMES_COMPARE 3
#define RAND_STR_LEN 1
void rstring(char *s, const int len);
int generate_data(int size);
struct data{
	char *s1;
	char *s2;
	int i1;
	int i2;
	int i3;
};

struct data* new_data(int size);
void shell_sort(struct data *data, int size, int f1, int f2, int f3);
void print_data(struct data *data, int size);
int field_compare(struct data *a, struct data *b, int field);
int main(int argc, char *argv[])
{
	//FILE *input;
	//FILE *output;
	int size;
	int field1;
	int field2;
	int field3;
	
	printf("Введите размер: ");
	if(fscanf(stdin, "%d", &size) != 1 || size < 2){
		fprintf(stderr, "Неправильный размер!\n");
		return 1;
	}
	
	/* генерация данных */
	generate_data(size);
	
	/* загрузка данных из файла */	
	struct data *data = new_data(size);
	if(data == NULL){
		fprintf(stderr, "Недостаточно памяти!\n");
		return 1;
	}
	printf("Введите номера полей, по которым необходимо сортировать: ");
	if(fscanf(stdin, "%d %d %d", &field1, &field2, &field3) != 3){
		fprintf(stderr, "Неправильные номера полей!\n");
		return 1;
	}
	shell_sort(data,size, field1, field2, field3);
	
	print_data(data, size);
	for(int i =0 ; i < size; i++){
		free(data[i].s1);
		free(data[i].s2);
	}
	free(data);

	return 0;
}

void print_data(struct data *data, int size)
{
	FILE *fp;
	if((fp=fopen(OUTPUT, "w")) == NULL){
		fprintf(stderr, "Не могу открыть(создать) файл!\n");
		return 1;
	}

	for(int i = 0; i < size; i++)
	{	
		fprintf(fp,"%s %s %d %d %d\n", 	data[i].s1,\
						data[i].s2,\
						data[i].i1,\
						data[i].i2,\
						data[i].i3);
	}
	
	fclose(fp);
}

/* Cортировка уеньшающимися растояниями(Сортировка Шелла) */
void shell_sort(struct data *data, int size, int f1, int f2, int f3)
{
	struct data buf;
	int i, j, k;
	int a = 0;
	int array[] = {f1,f2,f3};
	int flag = 0;
	for(k = size/2; k > 0; k /=2 ){
		for(i = k; i < size; i++){
			buf = data[i];
			for(j = i; j >= k; j -= k){
				flag = 0;	
				for(int h = 0 ; h < TIMES_COMPARE; h++){
					
					a = field_compare(&buf, &data[j-k], array[h]);
					if(a > 0){
						flag = 1;
						break;
					}
					if(a){
						data[j] = data[j-k];
						break;
					}
					if(h == TIMES_COMPARE - 1){
						flag = 1;
						break;
					}
				}

				if(flag)
					break;
			}
			data[j] = buf;
		}
	}
}

int field_compare(struct data *a, struct data *b, int field)
{
	switch(field)
	{
		case 0: return strcmp(a->s1, b->s1);
		case 1: return strcmp(a->s2, b->s2);
		case 2: return a->i1 - b->i1;
		case 3: return a->i2 - b->i2;
		case 4: return a->i3 - b->i3;
		default: return 0;
	};
	return 0;
}
/* загружаем данные из файла */
struct data* new_data(size)
{
	FILE *fp;
	if((fp=fopen(INPUT, "r")) == NULL){
		fprintf(stderr, "Не могу открыть(создать) файл!\n");
		return 1;
	}
	struct data *data = (struct data *) malloc(sizeof(struct data) * size);

	char s[STR_SIZE];

	for(int i = 0; i < size; i++)
	{	
		fscanf(fp, "%s", s);
		data[i].s1 = (char *) malloc(sizeof(char) * strlen(s) + 1);
		strcpy(data[i].s1, s);
		fscanf(fp, "%s", s);
		data[i].s2 = (char *) malloc(sizeof(char) * strlen(s) + 1);
		strcpy(data[i].s2, s);
		fscanf(fp,"%d %d %d", &data[i].i1, &data[i].i2, &data[i].i3);
		
	}
	fclose(fp);

	return data;
}

/* генерация случайных значений и запись их в файл */
int generate_data(int size)
{
	FILE *fp;
	if((fp=fopen(INPUT, "w")) == NULL){
		fprintf(stderr, "Не могу открыть(создать) файл!\n");
		return 1;
	}
	
	srand(time(NULL));
	char s1[STR_SIZE];
	char s2[STR_SIZE];

	for(int i = 0 ; i < size; i++){ 
		rstring(s1,RAND_STR_LEN);
		rstring(s2,RAND_STR_LEN);
		fprintf(fp,"%s %s %d %d %d\n", s1, \
					       s2, \
					       rand() % 100, \
					       rand() % 100, \
					       rand() % 100);
	}

	fclose(fp);
	return 0;
}

/* генерация случайных строк */
void rstring(char *s,const int len) {
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";
	
	for (int i = 0; i < len; ++i) {
		s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}
	s[len] = 0;
}
