#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#define STR_SIZE 1024 
#define INPUT "input"
#define OUTPUT "output"
#define TIMES_COMPARE 3
#define RAND_STR_LEN 1
void rstring(char *s, const int len);
int generate_data(int size);
struct data{
	char *s1;
	char *s2;
	int i1;
	int i2;
	int i3;
};
struct tree{
	FILE *file;
	struct data record;
	int empty;
	//	struct tree *left;
//	struct tree *right;
};

struct data* new_data(int size);
int shell_sort(struct data *data, int size, int f1, int f2, int f3);
int print_data(struct data *data, int size);
int field_compare(struct data *a, struct data *b, int field);
int place_to_files(int series_length, int f1, int f2, int f3);
struct tree *create_tree(struct tree *tr, struct tree *files, int start, int total_files);
struct tree *find_min(struct tree *a, struct tree *b, int f1, int f2, int f3);
void read_field(struct tree *toread);
int main(int argc, char *argv[])
{
	//FILE *input;
	//FILE *output;
	int size;
	int field1;
	int field2;
	int field3;
	int series_length;
	int total_files = 0;
	char s[STR_SIZE];
	char filename[STR_SIZE];
	char number[STR_SIZE];
	FILE *fp;
	printf("Введите число записей: ");
	if(fscanf(stdin, "%d", &size) != 1 || size < 2){
		fprintf(stderr, "Неправильный размер!\n");
		return 1;
	}



	/* генерация данных */
	generate_data(size);
 	/* загрузка данных из файла */	
/*	struct data *data = new_data(size);
	if(data == NULL){
		fprintf(stderr, "Недостаточно памяти!\n");
		return 1;
	}
 */
	printf("Введите номера полей, по которым необходимо сортировать: ");
	if(fscanf(stdin, "%d %d %d", &field1, &field2, &field3) != 3){
		fprintf(stderr, "Неправильные номера полей!\n");
		return 1;
	}
	printf("Введите длину серии: ");
	if(fscanf(stdin, "%d", &series_length) != 1){
		fprintf(stderr, "Неправильный номер!\n");
		return 1;
	}

	total_files = place_to_files(series_length, field1, field2, field3);
//	shell_sort(data,size, field1, field2, field3);


	struct tree files[total_files];
	for(int i = 0; i < total_files; i++){
		strcpy(filename,"outputs/");
		strcat(filename, OUTPUT);
		sprintf(number, "%d", i);
		strcat(filename, number);
		if((files[i].file=fopen(filename, "r")) == NULL){
			fprintf(stderr, "Не могу открыть(создать) файл!\n");
			return 1;
		}
		
		fscanf(files[i].file, "%s", s);
		files[i].record.s1 = (char *) malloc(sizeof(char) * strlen(s) + 1);
		strcpy(files[i].record.s1, s);
		fscanf(files[i].file, "%s", s);
		files[i].record.s2 = (char *) malloc(sizeof(char) * strlen(s) + 1);
		strcpy(files[i].record.s2, s);
		fscanf(files[i].file,"%d %d %d\n", &files[i].record.i1, &files[i].record.i2, &files[i].record.i3);
		files[i].empty = 0;
	/*	files[i].left = NULL;
		files[i].right = NULL;
*/
	}
	
//	struct tree *trna = NULL;
//	trna = malloc(sizeof(struct tree *));
//       trna = create_tree(trna, files,1, total_files );

	if((fp=fopen(OUTPUT, "w")) == NULL){
		fprintf(stderr, "Не могу открыть(создать) файл!\n");
		return 1;
	}

 	clock_t tic = clock();
	struct tree *tmp;	
	tmp = &files[0];
	int i = 1;
	while(i){
		i = 0;
		int j;
		for(j = 0; j < total_files; j++){
		
			if(!files[j].empty){
				tmp = &files[j];
				break;
			}
		}
		for(j  ; j < total_files; j++){
			if(!files[j].empty){
				i = 1;
				tmp = find_min(tmp, &files[j], field1, field2, field3);
			} 
		}

		if(i){

			fprintf(fp,"%s %s %d %d %d\n", 	
				tmp->record.s1,\
				tmp->record.s2,\
				tmp->record.i1,\
				tmp->record.i2,\
				tmp->record.i3);

			if(feof(tmp->file)){
				tmp->empty = 1;
			}else{
				read_field(tmp);
			}
		}
	}

	for(int i = 0; i < total_files; i++)
		fclose(files[i].file);

	fclose(fp);
	clock_t toc = clock();
	
	printf("Elapsed: %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);
	
/*
	print_data(data, size);
	for(int i =0 ; i < size; i++){
		free(data[i].s1);
		free(data[i].s2);
	}
	free(data);

*/
	return 0;
}

void read_field(struct tree *toread)
{
	
	char s[STR_SIZE];
	
	fscanf(toread->file, "%s", s);
	strcpy(toread->record.s1, s);
	fscanf(toread->file, "%s", s);
	strcpy(toread->record.s2, s);
	fscanf(toread->file,"%d %d %d\n", &toread->record.i1, &toread->record.i2, &toread->record.i3);
}
struct tree *find_min(struct tree *a, struct tree *b, int f1, int f2, int f3)
{
	int flag;
	int fields[] = {f1,f2,f3};
	for(int i = 0; i < TIMES_COMPARE; i++ ){
		flag = field_compare(&a->record, &b->record, fields[i]);
		if( flag > 0){
			return b;				
		} else if( flag < 0){
			return a;
		} if(i == TIMES_COMPARE - 1){
			return b;
		} 
	}


}
/*
struct tree *create_tree(struct tree *tr, struct tree *files, int start, int total_files){
	
	tr->left = malloc(sizeof(struct tree *));
	tr->right = malloc(sizeof(struct tree *));
	if(total_files - start == 0){
		tr = &files[start - 1];
		tr->record = files[start - 1].record;
		return tr;
	}else if(total_files - start  != 1){
		tr->left = create_tree(tr->left,files, start, (float)(total_files )/2 + 0.5);
		tr->right = create_tree(tr->right, files, (float)(total_files)/2 + 1.5, total_files);
	} else {


		tr->record = files[start - 1].record;
		tr->left = &files[start - 1];
		tr->right = &files[start];
		return tr;
	}
	

}
*/
int place_to_files(int series_length, int f1, int f2, int f3)
{

	FILE *from;
	FILE *to;
	if((from=fopen(INPUT, "r")) == NULL){
		fprintf(stderr, "Не могу открыть(создать) файл!\n");
		return 1;
	}
	struct data *data = (struct data *) malloc(sizeof(struct data) * series_length);

	for(int i = 0 ; i < series_length; i++){
	
		data[i].s1 = (char *) malloc(sizeof(char) * STR_SIZE + 1);
		data[i].s2 = (char *) malloc(sizeof(char) * STR_SIZE + 1);
	}

	struct stat st = {0};
	if (stat("outputs", &st) == -1) {
		mkdir("outputs", 0700);
	}

	char s[STR_SIZE];
	char filename[STR_SIZE];
	char number[STR_SIZE];
	int i;
	int j;
	for(j = 0; !feof(from); j++)
	{
		strcpy(filename,"outputs/");
		strcat(filename, OUTPUT);
		sprintf(number, "%d", j);
		strcat(filename, number);
		if((to=fopen(filename, "w")) == NULL){
			fprintf(stderr, "Не могу открыть(создать) файл!\n");
			return 1;
		}
		for(i = 0; i < series_length && !feof(from); i++){
			fscanf(from, "%s", s);
			strcpy(data[i].s1, s);
			fscanf(from, "%s", s);
			strcpy(data[i].s2, s);
			fscanf(from,"%d %d %d\n", &data[i].i1, &data[i].i2, &data[i].i3);
			
			}
		shell_sort(data, i, f1,f2,f3);
		
		for(int k = 0; k < i; k++)
		{
			fprintf(to,"%s %s %d %d %d\n", 	data[k].s1,\
					data[k].s2,\
					data[k].i1,\
					data[k].i2,\
					data[k].i3);
		}
		fclose(to);
		number[0] = '\0';
		filename[0] =  '\0';
	}
	fclose(from);
	for(int i =0 ; i < series_length; i++){
		free(data[i].s1);
		free(data[i].s2);
	}
	free(data);

	return j;

}
int print_data(struct data *data, int size)
{
	FILE *fp;
	if((fp=fopen(OUTPUT, "w")) == NULL){
		fprintf(stderr, "Не могу открыть(создать) файл!\n");
		return 1;
	}

	for(int i = 0; i < size; i++)
	{	
		fprintf(fp,"%s %s %d %d %d\n", 	data[i].s1,\
						data[i].s2,\
						data[i].i1,\
						data[i].i2,\
						data[i].i3);
	}
	
	fclose(fp);
	return 0;
}

/* Cортировка уеньшающимися растояниями(Сортировка Шелла) */
int shell_sort(struct data *data, int size, int f1, int f2, int f3)
{
	struct data buf;
	int i, j, k;
	int a = 0;
	int array[] = {f1,f2,f3};
	int flag = 0;
	for(k = size/2; k > 0; k /=2 ){
		for(i = k; i < size; i++){
			buf = data[i];
			for(j = i; j >= k; j -= k){
				flag = 0;	
				for(int h = 0 ; h < TIMES_COMPARE; h++){
					
					a = field_compare(&buf, &data[j-k], array[h]);
					if(a > 0){
						flag = 1;
						break;
					}
					if(a){
						data[j] = data[j-k];
						break;
					}
					if(h == TIMES_COMPARE - 1){
						flag = 1;
						break;
					}
				}

				if(flag)
					break;
			}
			data[j] = buf;
		}
	}
	return 0;
}

int field_compare(struct data *a, struct data *b, int field)
{
	switch(field)
	{
		case 0: return strcmp(a->s1, b->s1);
		case 1: return strcmp(a->s2, b->s2);
		case 2: return a->i1 - b->i1;
		case 3: return a->i2 - b->i2;
		case 4: return a->i3 - b->i3;
		default: return 0;
	};
	return 0;
}
/* загружаем данные из файла */
struct data* new_data(int size)
{
	FILE *fp;
	if((fp=fopen(INPUT, "r")) == NULL){
		fprintf(stderr, "Не могу открыть(создать) файл!\n");
		exit(1);
	}
	struct data *data = (struct data *) malloc(sizeof(struct data) * size);

	char s[STR_SIZE];

	for(int i = 0; i < size; i++)
	{	
		fscanf(fp, "%s", s);
		data[i].s1 = (char *) malloc(sizeof(char) * strlen(s) + 1);
		strcpy(data[i].s1, s);
		fscanf(fp, "%s", s);
		data[i].s2 = (char *) malloc(sizeof(char) * strlen(s) + 1);
		strcpy(data[i].s2, s);
		fscanf(fp,"%d %d %d", &data[i].i1, &data[i].i2, &data[i].i3);
		
	}
	fclose(fp);

	return data;
}

/* генерация случайных значений и запись их в файл */
int generate_data(int size)
{
	FILE *fp;
	if((fp=fopen(INPUT, "w")) == NULL){
		fprintf(stderr, "Не могу открыть(создать) файл!\n");
		return 1;
	}
	
	srand(time(NULL));
	char s1[STR_SIZE];
	char s2[STR_SIZE];

	for(int i = 0 ; i < size; i++){ 
		rstring(s1,RAND_STR_LEN);
		rstring(s2,RAND_STR_LEN);
		fprintf(fp,"%s %s %d %d %d\n", s1, \
					       s2, \
					       rand() % 100, \
					       rand() % 100, \
					       rand() % 100);
	}

	fclose(fp);
	return 0;
}

/* генерация случайных строк */
inline void rstring(char *s,const int len) {
	static const char alphanum[] =
//		"0123456789"
//		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";
	
	for (int i = 0; i < len; ++i) {
		s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}
	s[len] = 0;
}
