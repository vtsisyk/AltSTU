#include "graph.h"

Graph::Graph(QWidget *parent) : QWidget(parent)
{

}

QVector<QVector<int> > Graph::get_adjacency()
{
	return adjacency;
}

QVector<QVector<QPair <int, edge_properties > > > Graph::get_edges()
{
	return edges;
}

void Graph::set_adjacency(QVector<QVector<int> > _adjacency)
{
	adjacency = _adjacency;
}

void Graph::set_edges (QVector<QVector<QPair <int, edge_properties > > > _edges)
{
	edges = _edges;	
}

void Graph::getname(QString name)
{
	file = new QFile(name);
	readdata();
}


bool Graph::get_type()
{
	return type;
}
/* эта функция должна считывать данные из файлика в inc или adj.
 * Результат считывания может отличаться от ожидаемого, если входной файл
 * будет не совсем "правильный".
 */

void Graph::readdata()
{

	QRegExp rx("[\\s]");
	if(!file->open(QIODevice::ReadOnly))
		return;
	QTextStream in(file);
	QString line = in.readLine().trimmed();
	if(line == "a"){
		type = 1;
		while(!in.atEnd()){
			QString line = in.readLine().trimmed();
			QStringList query = line.split(rx);
			QVector<int> tmp;
			for(int i = 0; i < query.size(); i++)
				tmp.append(query.at(i).toInt());
			adjacency.append(tmp);
		}
	} else if(line == "i"){
		type = 0;
		QString line = in.readLine().trimmed();
		edges.resize(line.toInt());
		obratka.resize(line.toInt());
		for(int i = 0 ; i < line.toInt(); i++ )
		vertexes.append(vertex_properties());
		while(!in.atEnd()){
			line = in.readLine().trimmed();
			QStringList query = line.split(rx);
			QVector<QPair <int, edge_properties > >  tmp;
			QVector<QPair <int, edge_properties > >  obrat;
			edge_properties temp = {0, query.at(2).toInt()};
			tmp.append(qMakePair(query.at(1).toInt(), temp));
			obrat.append(qMakePair(query.at(0).toInt(), temp));


			obratka[query.at(1).toInt()].append(obrat);
			edges[query.at(0).toInt()].append(tmp);
		}
	}

	file->close();
	// пока первая веришна будет истоком
	initialize_preflow(0);

//	for(int i = 0 ; i< adjacency.size(); i++)
//		visited.push_back(0);
}

void Graph::generic_push_relabel()
{
	bool push_was_done ;
	bool do_relabel;
	int minimal_height;
	for(int i = 0; i < vertexes.size() -1; i++){
		minimal_height = INT_MAX;
		push_was_done  = 0;
		if(vertexes[i].overflow > 0 && i !=vertexes.size() - 1){
			do_relabel = 0;
			/* протолкнуть по направлению */
			for(int j = 0; j < edges[i].size(); j++){
				if(vertexes[i].height == vertexes[edges[i][j].first].height + 1 \
				&& edges[i][j].second.flow <  edges[i][j].second.capacity ){
					push_was_done = 1;
					push(i, j, 1);
					i = 0;
					break;
				}

			}
			/* протолкнуть против направления */
			if(!push_was_done){
				for(int j = 0; j < obratka[i].size(); j++){
					if(vertexes[i].height == vertexes[obratka[i][j].first].height + 1 \
					&& obratka[i][j].second.flow <  obratka[i][j].second.capacity ){
						push_was_done = push(i, j, 0);
						if(push_was_done){
						i =0;
						break;
						}
					}
				}
			}
			/* Понимаем вершину */
			if(!push_was_done && i != vertexes.size() -1){
				for(int j = 0; j < edges[i].size(); j++){
					if( vertexes[i].height <= vertexes[edges[i][j].first].height && edges[i][j].second.flow != edges[i][j].second.capacity ){
						if(minimal_height > vertexes[edges[i][j].first].height){
							minimal_height = vertexes[edges[i][j].first].height;
							do_relabel = 1;
						}
					}
				}
				if(do_relabel && i != vertexes.size() - 1){
					relabel(i, minimal_height);
					i = 0;
				} else if(!do_relabel){
					for(int k = 0; k < obratka[i].size(); k++){
						if( vertexes[i].height <= vertexes[obratka[i][k].first].height){
							if(minimal_height > vertexes[obratka[i][k].first].height){
								minimal_height = vertexes[obratka[i][k].first].height;
								do_relabel = 1;
							}
						}
					}
					if(do_relabel && i != vertexes.size() - 1){
						relabel(i, minimal_height);
						i = 0;
					}
				}

			}
		}
	}
	/* рисуем все это */
	emit draw();
}

/* Направляем поток в смежые с истоком вершины */
void Graph::initialize_preflow(int s)
{
	// нет операции обнуления(сделано при вводе)
	vertexes[s].height = vertexes.size();
	for(int i = 0 ; i < edges[s].size(); i++){
		vertexes[edges[s][i].first].overflow = edges[s][i].second.capacity;
		edges[s][i].second.flow = edges[s][i].second.capacity;
		vertexes[s].overflow -=  edges[s][i].second.capacity;
	}
	generic_push_relabel();
}

int Graph::push(int u, int v, int push_was_done)
{

	int delta_f;
	int i = 0;
	/* по направлению или в обратную сторону? */
	if(push_was_done){
		delta_f = vertexes[u].overflow < edges[u][v].second.capacity - edges[u][v].second.flow \
	? vertexes[u].overflow : edges[u][v].second.capacity - edges[u][v].second.flow ;
	}else{
		for(i = 0 ; i < edges[obratka[u][v].first].size(); i++)
			if(edges[obratka[u][v].first][i].first == u)
				break;
		if(edges[obratka[u][v].first][i].second.flow >0){
			delta_f = vertexes[u].overflow < obratka[u][v].second.capacity - obratka[u][v].second.flow \
			? vertexes[u].overflow : obratka[u][v].second.capacity - obratka[u][v].second.flow ;
		} else
			return 0;
	}
	if(push_was_done)
	{
		edges[u][v].second.flow += delta_f;
		vertexes[edges[u][v].first].overflow += delta_f;
	}else{
		edges[obratka[u][v].first][i].second.flow -= delta_f;
		obratka[u][v].second.flow += delta_f;
		vertexes[obratka[u][v].first].overflow += delta_f;
	}
	vertexes[u].overflow -= delta_f;
	pushes++;
	return 1;
}

/* Поднятие вершины */
void Graph::relabel(int u, int minimal_height)
{

	vertexes[u].height = 1 + minimal_height;
	relabeled++;
}
