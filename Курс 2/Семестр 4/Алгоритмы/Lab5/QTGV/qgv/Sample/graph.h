#ifndef GRAPH_H
#define GRAPH_H

#include <QWidget>
#include <QVector>
#include <QPair>
#include <QTextStream>
#include <QFile>
#include <QString>
#include <QDebug>

class Graph : public QWidget
{
	Q_OBJECT
private:
	QFile *file;

	/* определяем, матрица смежности или список вершин и ребер */
	bool type;

	/* матрица смежности */
	QVector<QVector<int> > adjacency;

	/* вершины-ребра*/
	QVector<QVector<QPair <int, int > > > edges;

	/* вес вершины */
	QVector<int> visited;

	int color = 0;
	QString palette[8] = {"", "red", "green", "blue", "cyan", "magenta", "black", "yellow"};
	int colored = 1;


public:
	QVector<QVector<int> > get_adjacency();
	QVector<QVector<QPair <int, int > > > get_edges();
	void set_adjacency(QVector<QVector<int> > _adjacency);
	void set_edges (QVector<QVector<QPair <int, int > > > _edges);
	bool get_type();


	void edges_coloring(int vertex);

	int findcolor(int vertex, int to);
	
	bool is_visited(int i);
	void readdata();
	explicit Graph(QWidget *parent = 0);
	int getmax(int *pos);

signals: 
	void color_vertex(int, QString);
	void colorthis(int, int, QString);
public slots:
	void getname(QString);
};

#endif // GRAPH_H
