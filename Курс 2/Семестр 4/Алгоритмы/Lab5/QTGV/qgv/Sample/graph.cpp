#include "graph.h"

Graph::Graph(QWidget *parent) : QWidget(parent)
{

}

QVector<QVector<int> > Graph::get_adjacency()
{
	return adjacency;
}

QVector<QVector<QPair <int, int > > > Graph::get_edges()
{
	return edges;
}

void Graph::set_adjacency(QVector<QVector<int> > _adjacency)
{
	adjacency = _adjacency;
}

void Graph::set_edges (QVector<QVector<QPair <int, int > > > _edges)
{
	edges = _edges;	
}

void Graph::getname(QString name)
{
	file = new QFile(name);
	readdata();
}


bool Graph::get_type()
{
	return type;
}
/* эта функция должна считывать данные из файлика в inc или adj.
 * Результат считывания может отличаться от ожидаемого, если входной файл
 * будет не совсем "правильный".
 */

void Graph::readdata()
{

	QRegExp rx("[\\s]");
	if(!file->open(QIODevice::ReadOnly))
		return;
	QTextStream in(file);
	QString line = in.readLine().trimmed();
	if(line == "a"){
		type = 1;
		while(!in.atEnd()){
			QString line = in.readLine().trimmed();
			QStringList query = line.split(rx);
			QVector<int> tmp;
			for(int i = 0; i < query.size(); i++)
				tmp.append(query.at(i).toInt());
			adjacency.append(tmp);
		}
	} else if(line == "i"){
		type = 0;
		while(!in.atEnd()){
			QString line = in.readLine().trimmed();
			QStringList query = line.split(rx);
			QVector<QPair <int, int> >  tmp;
			for(int i = 0; i < query.size(); i+=2)
				tmp.append(qMakePair(query.at(i).toInt(), query.at(i+1).toInt() ));
				edges.append(tmp);
			}
	}

	file->close();
	for(int i = 0 ; i< adjacency.size(); i++)
		visited.push_back(0);
}


/* реберная раскраска для матрицы смежности. Полный перебор. */
void Graph::edges_coloring(int vertex)
{
	for(int i = 0; i < adjacency.size(); i++ ){
		if(adjacency [vertex][i] == 1 && !visited[i]){
			int color = findcolor(vertex, i) ;

			colorthis(vertex, i, palette[color -1 ]);
			adjacency[vertex][i] = color;
			adjacency[i][vertex] = color;
			visited[vertex] = 1;


			if(colored == 1 ){
				emit color_vertex(vertex, palette[colored]);
				colored = 3;

			}

			if(colored == 2 ){
				emit color_vertex(i, palette[colored - 1]);
				colored = 3;
			}
			else if(colored == 3){
				emit color_vertex(i, palette[colored - 1]);
				colored = 2;
			}

			edges_coloring(i);


			colored = 0;
			visited[vertex] = 0;
		}
	}

}

/* Поиск цвета для ребра. Можно было попробовать сделать отдельный
 * массив, типа
 * 1 2 3 4 5 6
 * к с к с к с
 * ж к с к с к
 * ....
 * где буквы - это первые буквы названий цветов.
 * Это может сократить время работы, но за это придется платить памятью.
 * Поэтому оставлю.
 */
int Graph::findcolor(int vertex, int to)
{
	int color = 0;
	bool found = 0;
	for(int i = 2; i < 6; i++){
		for(int j = 0; j < adjacency.size(); j++ ){
			if(adjacency[vertex][j] == i || adjacency[to][j] == i){
				found = 1;
				break;
			}
		}
		if(!found){
			color = i;
			break;

		}
		found = 0;
	}

	return color;
}
