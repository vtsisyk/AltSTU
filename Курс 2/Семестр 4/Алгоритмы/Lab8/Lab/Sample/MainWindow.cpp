/***************************************************************
QGVCore Sample
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/
#include "MainWindow.h"
#include "moc_MainWindow.cpp"
#include "ui_MainWindow.h"
#include "QGVScene.h"
#include "QGVNode.h"
#include "QGVEdge.h"
#include "QGVSubGraph.h"
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	_scene = new QGVScene("DEMO", this);
	ui->graphicsView->setScene(_scene);

	te = new Graph();
	connect(_scene, SIGNAL(nodeContextMenu(QGVNode*)), SLOT(nodeContextMenu(QGVNode*)));
	connect(_scene, SIGNAL(nodeDoubleClick(QGVNode*)), SLOT(nodeDoubleClick(QGVNode*)));

	connect(this, SIGNAL(setname(QString)), te, SLOT(getname(QString)));
	connect(ui->action, SIGNAL(triggered()),this, SLOT(read()));
//	connect(te, SIGNAL(colorthis(int,int, QString)),this, SLOT(newColor(int, int, QString)));
//	connect(te, SIGNAL(color_vertex(int, QString)),this, SLOT(color_vertex(int, QString)));
	connect(te, SIGNAL(draw()), this, SLOT(drawGraph()));
	
	//Configure scene attributes
	_scene->setGraphAttribute("label", "Лабораторная работа №8");
	/* стиль соединительных линий */

	_scene->setGraphAttribute("splines", "splines");
	/* слева направо*/
	_scene->setGraphAttribute("rankdir", "LR");

	/* расстояние между узлами */
	_scene->setGraphAttribute("nodesep", "0.6");

	/* стиль узлов */
	_scene->setNodeAttribute("shape", "circle");
	_scene->setNodeAttribute("style", "filled");
	_scene->setNodeAttribute("fillcolor", "grey");
	_scene->setNodeAttribute("height", "0.7");
	_scene->setEdgeAttribute("minlen", "3");
	_scene->setEdgeAttribute("fontsize", "10");

	//Add some nodes

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::read()
{
	QString filename = QFileDialog::getOpenFileName(
	this,
	tr("Открыть файл"),
	tr("/home/vlad/Empty"), /* путь в файловой системе. "" для корня */
	tr("Файлы (*.txt)")
	);
	emit setname(filename);

}


void MainWindow::drawGraph()
{


	QVector<QVector<QPair <int, int > > > edges = te->get_edges();
	bool type = te->get_type();
	QVector <QGVNode *> gr;

	for(int i = 0; i < edges.size(); i++){
		gr.push_back(_scene->addNode(QString::number(i +1)));
	}


	for(int i = 0; i < edges.size(); i++){
		for(int j = 0; j < edges[i].size(); j++){
			QGVEdge *a = _scene->addEdge(gr[i],gr[edges[i][j].first],\
			QString::number(edges[i][j].second));
			a->setAttribute("color","white");
			//a->setAttribute("dir","none");
		}
	}

	_scene->applyLayout();

	//Fit in view
	ui->graphicsView->fitInView(_scene->sceneRect(), Qt::KeepAspectRatio);
}

void MainWindow::nodeContextMenu(QGVNode *node)
{
    //Context menu exemple
    QMenu menu(node->label());

    menu.addSeparator();
    menu.addAction(tr("Informations"));
    menu.addAction(tr("Options"));

    QAction *action = menu.exec(QCursor::pos());
    if(action == 0)
        return;
}

void MainWindow::nodeDoubleClick(QGVNode *node)
{
    QMessageBox::information(this, tr("Node double clicked"), tr("Node %1").arg(node->label()));
}
