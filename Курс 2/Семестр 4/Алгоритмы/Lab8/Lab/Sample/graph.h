#ifndef GRAPH_H
#define GRAPH_H

#include <QWidget>
#include <QVector>
#include <QPair>
#include <QTextStream>
#include <QFile>
#include <QString>
#include <QDebug>
#include <limits.h>
#include <iostream>
#include <vector>



class Graph : public QWidget
{
	Q_OBJECT
private:
	QFile *file;

	/* определяем, матрица смежности или список вершин и ребер */
	bool type;

	/* матрица смежности */
	QVector<QVector<int> > adjacency;

	/* вершины-ребра*/
	QVector<QVector<QPair <int, int > > > edges;

	//QVector<vertex_properties> vertexes;



public:

	QVector<QVector<int> > get_adjacency();
	QVector<QVector<QPair <int, int > > > get_edges();
	void set_adjacency(QVector<QVector<int> > _adjacency);
	void set_edges (QVector<QVector<QPair <int, int> > > _edges);

	int **mediana;
	bool get_type();
	void work();
	int find_path(int start );


	void readdata();
	explicit Graph(QWidget *parent = 0);
	int getmax(int *pos);

signals:
	void draw();
	//void color_vertex(int, QString);
	//void colorthis(int, int, QString);
public slots:
	void getname(QString);
};

#endif // GRAPH_H
