#include "graph.h"

Graph::Graph(QWidget *parent) : QWidget(parent)
{

}

QVector<QVector<int> > Graph::get_adjacency()
{
	return adjacency;
}

QVector<QVector<QPair <int, int> > > Graph::get_edges()
{
	return edges;
}

void Graph::set_adjacency(QVector<QVector<int> > _adjacency)
{
	adjacency = _adjacency;
}

void Graph::set_edges (QVector<QVector<QPair<int, int> > > _edges)
{
	edges = _edges;	
}

void Graph::getname(QString name)
{
	file = new QFile(name);
	readdata();
}


bool Graph::get_type()
{
	return type;
}
/* эта функция должна считывать данные из файлика в inc или adj.
 * Результат считывания может отличаться от ожидаемого, если входной файл
 * будет не совсем "правильный".
 */

void Graph::readdata()
{

	QRegExp rx("[\\s]");
	if(!file->open(QIODevice::ReadOnly))
		return;
	QTextStream in(file);
	QString line = in.readLine().trimmed();
	if(line == "a"){
		type = 1;
		while(!in.atEnd()){
			QString line = in.readLine().trimmed();
			QStringList query = line.split(rx);
			QVector<int> tmp;
			for(int i = 0; i < query.size(); i++)
				tmp.append(query.at(i).toInt());
			adjacency.append(tmp);
		}
	} else if(line == "i"){
		type = 0;
		QString line = in.readLine().trimmed();
		edges.resize(line.toInt());
		while(!in.atEnd()){
			line = in.readLine().trimmed();
			QStringList query = line.split(rx);
			QVector<QPair <int, int > >  tmp;

			tmp.append(qMakePair(query.at(1).toInt(), query.at(2).toInt()));

			edges[query.at(0).toInt()].append(tmp);
		}

	}

	file->close();

	work();



}

void Graph::work()
{
	mediana = new int *[edges.size() + 1];
	for(int i = 0; i < edges.size() + 1; i++){
		mediana[i] = new int[edges.size() + 1];
	}

	for(int i = 0 ; i < edges.size() ; i++){
		find_path(i);
	}

	int inside = INT_MAX;
	int out = INT_MAX;

	std::cout <<"     ";
	for(int i = 0; i < edges.size(); i++)
		std::cout << "x"<< i+1<<"  ";
	std::cout <<"\n------------------------------\n";
	for(int i = 0 ; i < edges.size(); i++){
		int horiz = 0;
		for(int j = 0; j < edges.size() ; j++){
			//std::cout << mediana[i][j] << "   ";
			horiz += mediana[i][j];
		}
		if(horiz < out){
			out = horiz;
		}
		mediana[i][edges.size() ]= horiz;
	}
	for(int i = 0 ; i < edges.size() ; i++){
		int vert= 0;
		for(int j = 0; j < edges.size() ; j++){
			//std::cout << mediana[j][i] << "   ";
			vert += mediana[j][i];
		}
		if(vert < inside){
			inside =  vert;
		}

		mediana[edges.size() ][i]= vert;

	}

	for(int i =0; i < edges.size()  ; i++ ){
		std::cout << "x"<< i+1 <<" | ";
		for(int j = 0 ; j < edges.size() ; j++){
			std::cout << mediana[i][j] << "   ";
		}
		std::cout<<" | ";
		std::cout << mediana[i][edges.size() ] ;
		if(mediana[i][edges.size() ] == out)
			std::cout << "*";
		std::cout<<"\n";

	}
	std::cout <<"------------------------------\n";
	std::cout <<"     ";
	for(int i =0; i < edges.size(); i++ ){
		std::cout << mediana[edges.size() ][i] ;
		if(mediana[edges.size() ][i] == inside)
			std::cout << "*";
		std::cout << "  ";

	}
	std::cout << "\n";


		draw();
}
/* алгоритм дейкстры для поиска самого дешевого пути */
int Graph::find_path(int start ){
	const int INF = INT_MAX;
	bool exit = 0;
	int n = edges.size()  ;
	QVector<int> d (n, INF),  p (n);
	d[start] = 0;
	QVector<char> u (n);

	for (int i=0; i<n; ++i) {
		int v = -1;
		for (int j=0; j<n; ++j)
			if (!u[j] && (v == -1 || d[j] < d[v]))
				v = j;
		if (d[v] == INF)
			break;
		u[v] = true;


		for (int j=0; j<edges[v].size(); ++j) {
			int to = edges[v][j].first,
				len = edges[v][j].second;
			if (d[v] + len < d[to] ) {
				d[to] = d[v] + len;
				p[to] = v;
			}
		}
	}

	unsigned length = 0;
	for(int i = 0; i < n; i++){
		exit = 0;
		for(unsigned j =0; j < n; j++)
			length += d[j];

	std::vector<int> path;

	if(start == i){
		mediana[start][i] = 0;
		continue;
	}

	for (int v=i; v!=start;v=p[v]){
		path.push_back (v);
		if(v==p[v]){
			exit = 1;
			mediana[start][i] = INT_MAX/50;
			break;
		}

	}
	if(exit)
		continue;


	path.push_back (start);
	reverse (path.begin(), path.end());
	int flow = INT_MAX;

	/*
	if(path[path.size() - 1] != 7)
		return 1;
	*/
	int length = 0;
		for(unsigned k =0; k < path.size() - 1; k++){
			for(int j = 0; j < edges[path[k]].size() ; j++){
				if(edges[path[k]][j].first == path[k + 1] ){
					length += edges[path[k]][j].second;
					break;
				}
			}
		}
		mediana[start][i] = length;

	}

	return 0;
}
