#include "graph.h"

Graph::Graph(QWidget *parent) : QWidget(parent)
{

}

QVector<QVector<int> > Graph::get_adjacency()
{
	return adjacency;
}

QVector<QVector<QPair <int, edge_properties > > > Graph::get_edges()
{
	return edges;
}

void Graph::set_adjacency(QVector<QVector<int> > _adjacency)
{
	adjacency = _adjacency;
}

void Graph::set_edges (QVector<QVector<QPair <int, edge_properties > > > _edges)
{
	edges = _edges;	
}

void Graph::getname(QString name)
{
	file = new QFile(name);
	readdata();
}


bool Graph::get_type()
{
	return type;
}
/* эта функция должна считывать данные из файлика в inc или adj.
 * Результат считывания может отличаться от ожидаемого, если входной файл
 * будет не совсем "правильный".
 */

void Graph::readdata()
{

	QRegExp rx("[\\s]");
	if(!file->open(QIODevice::ReadOnly))
		return;
	QTextStream in(file);
	QString line = in.readLine().trimmed();
	if(line == "a"){
		type = 1;
		while(!in.atEnd()){
			QString line = in.readLine().trimmed();
			QStringList query = line.split(rx);
			QVector<int> tmp;
			for(int i = 0; i < query.size(); i++)
				tmp.append(query.at(i).toInt());
			adjacency.append(tmp);
		}
	} else if(line == "i"){
		type = 0;
		QString line = in.readLine().trimmed();
		edges.resize(line.toInt());

		for(int i = 0 ; i < line.toInt(); i++ )
		vertexes.append(vertex_properties());
		while(!in.atEnd()){
			line = in.readLine().trimmed();
			QStringList query = line.split(rx);
			QVector<QPair <int, edge_properties > >  tmp;

			edge_properties temp = {0, query.at(2).toInt(), query.at(3).toInt()};
			tmp.append(qMakePair(query.at(1).toInt(), temp));

			edges[query.at(0).toInt()].append(tmp);
		}
	}

	file->close();

	int ret;
	while(1){
		ret = find_path(0);
		if(ret)
			break;

	}
	draw();

}
/* алгоритм дейкстры для поиска самого дешевого пути */
int Graph::find_path(int start ){
	const int INF = INT_MAX;
	int n = edges.size();
	QVector<int> d (n, INF),  p (n);
	d[start] = 0;
	QVector<char> u (n);

	for (int i=0; i<n; ++i) {
		int v = -1;
		for (int j=0; j<n; ++j)
			if (!u[j] && (v == -1 || d[j] < d[v]))
				v = j;
		if (d[v] == INF)
			break;
		u[v] = true;


		for (int j=0; j<edges[v].size(); ++j) {
			int to = edges[v][j].first,
				len = edges[v][j].second.cost;
			if (d[v] + len < d[to] && edges[v][j].second.capacity != edges[v][j].second.flow) {
				d[to] = d[v] + len;
				p[to] = v;
			}
		}
	}

	unsigned length = 0;
	for(unsigned i =0; i < n; i++)
		length += d[i];



	std::vector<int> path;
	for (int v=7; v!=start;v=p[v]){
			path.push_back (v);
			qDebug() << v + 1;
	}
	qDebug() << "-------";

	if(path.size() == 1)
		return 1;

	path.push_back (start);
	reverse (path.begin(), path.end());
	int flow = INT_MAX;
	if(path[path.size() - 1] != 7)
		return 1;
	std::vector<int> way;
	for(unsigned i =0; i < path.size() - 1; i++){
		for(int j = 0; j < edges[path[i]].size(); j++){
			if(edges[path[i]][j].first == path[i + 1] ){
				way.push_back(j);
				if(edges[path[i]][j].second.capacity - edges[path[i]][j].second.flow < flow){
					flow = edges[path[i]][j].second.capacity - edges[path[i]][j].second.flow;
					break;
				}
			}
		}
	}

	for(int i = 0; i < path.size() - 1; i++){	
		edges[path[i]][way[i]].second.flow += flow;
	}
	return 0;
}
