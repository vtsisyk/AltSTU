#ifndef GRAPH_H
#define GRAPH_H

#include <QWidget>
#include <QVector>
#include <QPair>
#include <QTextStream>
#include <QFile>
#include <QString>
#include <QDebug>
#include <limits.h>
#include <iostream>
#include <vector>

struct edge_properties{
	int flow;
	int capacity;
	int cost;

	edge_properties()
	{
		cost = 0;
		flow = 0;
		capacity = 0;
	}
	edge_properties(int _flow, int _capacity, int _cost)
	{
		cost = _cost;
		flow = _flow;
		capacity = _capacity;
	}
};

struct vertex_properties{
	int overflow;
	int height;

	vertex_properties()
	{
		overflow = 0;
		height = 0;
	}
	vertex_properties(int _overflow, int _height)
	{
		overflow = _overflow;
		height = _height;
	}
};


class Graph : public QWidget
{
	Q_OBJECT
private:
	QFile *file;

	int pushes = 0;
	int relabeled = 0;
	/* определяем, матрица смежности или список вершин и ребер */
	bool type;

	// Количество операций подъема.
	unsigned times_relabeled = 0;

	// Количество операций проталкивания.
	unsigned times_preflow_pushed = 0 ;

	/* матрица смежности */
	QVector<QVector<int> > adjacency;

	/* вершины-ребра*/
	QVector<QVector<QPair <int, edge_properties > > > edges;

	QVector<vertex_properties> vertexes;



public:

	QVector<QVector<int> > get_adjacency();
	QVector<QVector<QPair <int, edge_properties > > > get_edges();
	void set_adjacency(QVector<QVector<int> > _adjacency);
	void set_edges (QVector<QVector<QPair <int, edge_properties> > > _edges);

	bool get_type();
	int find_path(int start );


	void readdata();
	explicit Graph(QWidget *parent = 0);
	int getmax(int *pos);

signals:
	void draw();
	//void color_vertex(int, QString);
	//void colorthis(int, int, QString);
public slots:
	void getname(QString);
};

#endif // GRAPH_H
