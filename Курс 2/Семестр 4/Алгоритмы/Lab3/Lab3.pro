#-------------------------------------------------
#
# Project created by QtCreator 2016-02-28T18:38:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab3
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
	mainwindow.cpp\
	graph.cpp

HEADERS  += mainwindow.h graph.h

FORMS    += mainwindow.ui
