#ifndef GRAPH_H
#define GRAPH_H

#include <QObject>
#include <QString>
#include <QDebug>
#include <QFileDialog>
#include <QVector>
#include <QTextStream>
#include <limits.h>
#include <QPair>
#include <fstream>

using namespace std;
class Graph : public QObject
{
	Q_OBJECT
public:
	const int not_reachable = 1000;
	explicit Graph(QObject *parent = 0);
	void readdata();
	void longestcycle();
	void adj_DFS(int v);
	void inc_DFS(int v);
	int fromto(int i, int j);
	void drawgraph();
	void adj_graph();
	void inc_graph();
	void answergraph();

private:
	int size = 0;
	int longest = 0;
	bool adjacency; /* что даем на вход */
	unsigned start = 0;
	QFile *file; /* файл, из которого читаем */
	QVector<QVector<int> > adj; /* матрица смежности */
	QVector<QVector<QPair <int, int > > > inc; /* вершины-ребра*/
	QVector<bool> visited;
	QVector<int> vertexes;
	QVector<int> answer;
signals:
	int setmax (unsigned);
	void setpicture();
	void setgraph();
	void setanswer(int);
public slots:
	void getname(QString);
};

#endif // GRAPH_H
