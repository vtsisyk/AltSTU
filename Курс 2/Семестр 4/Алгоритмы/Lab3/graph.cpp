#include "graph.h"

Graph::Graph(QObject *parent) : QObject(parent)
{

}
/*
void Graph::getname(QString name)
{
	file = new QFile(name);
	readdata();
}
*/

/*
 * Читаем данные из файла. Проверяем каким образом задан список("a" - для
 * матрицы смежности, "i" - для списка вершин и дуг).
 * Соотвествующие данные пихаем в разные виды контейнеров.
 * Запускаем алгоритм поиска минимального каркаса из первой(нулевой) вершины.
 */

void Graph::getname(QString name)
{
	file = new QFile(name);
	readdata();
}
void Graph::readdata()
{
	adj.clear();
	inc.clear();
	QRegExp rx("[\\s]");
	if(file->open(QIODevice::ReadOnly)){
		QTextStream in(file);
		QString line = in.readLine().trimmed();
		if(line == "a"){
			adjacency = 1;
			while(!in.atEnd()){
				QString line = in.readLine().trimmed();
				QStringList query = line.split(rx);
				QVector<int> tmp;
				for(int i = 0; i < query.size(); i++)
					tmp.append(query.at(i).toInt());
				adj.append(tmp);
			}
			emit setmax(adj.size() - 1);
		} else if(line == "i"){
			adjacency = 0;
			while(!in.atEnd()){
				QString line = in.readLine().trimmed();
				QStringList query = line.split(rx);
				QVector<QPair <int, int> >  tmp;
				for(int i = 0; i < query.size(); i+=2)
					tmp.append(qMakePair(query.at(i).toInt(), query.at(i+1).toInt() ));
				inc.append(tmp);
			}
			emit setmax(inc.size() - 1);
		}
	}
	file->close();
	drawgraph();
	longestcycle();
}

void Graph::longestcycle()
{
	/* будем читать, что граф связный.
	 * так мы переберм меньше путей.
	 * Для того, чтобы сделать алгоритм для несвязного графа
	 * нужно весго лишь сделать цикл, который будет вызывать
	 * функцию DFS() для всех вершин
	 */
	

	if(adjacency){
		visited.resize(adj.size());
		for(int i = 0; i < adj.size(); i++)
			visited[i] = false;
	adj_DFS(0);

	}else{
		visited.resize(inc.size());
		for(int i = 0; i < adj.size(); i++)
			visited[i] = false;
	inc_DFS(0);

	}

	answergraph();
	emit setgraph();
	emit setanswer(size);
	qDebug()<< "------ "<< "answer" <<" ------ " ;
	for(int i =0; i < answer.size(); i++)
		qDebug() << answer[i];
	qDebug()<< "size: " << size;

}

void Graph::adj_DFS(int v)
{
	vertexes.push_back(v);
	visited[v] = true;
	for(int i = 0; i < adj.size() ; i++){
		if(!visited[i] && adj[v][i] > 0 && adj[v][i] < not_reachable){
			adj_DFS(i);
		} else if(visited[i] && i!= v && vertexes[vertexes.size()-2] != i
			&& adj[v][i] > 0 && adj[v][i] < not_reachable){
			int j;
			int sum = 0;
			vertexes.push_back(i);
			for( j = 0; j < vertexes.size(); j++)
				if(vertexes[j] == i)
					break;

			for(int k = j; k < vertexes.size() - 1; k++){
				qDebug() << vertexes[k ];
				sum += adj[vertexes[k]][vertexes[k+1]] ;
			}
			qDebug() << vertexes[j];

			if(sum > longest) {
				answer.clear();
				for(int k = j; k < vertexes.size() ; k++)
					answer.push_back(vertexes[k]);
				size = sum;
			}

			qDebug()<< "sum "<< sum<< "\n--------";
			vertexes.pop_back();
		}
	}
	vertexes.pop_back();
	visited[v] = false;
}

void Graph::inc_DFS(int v)
{
	vertexes.push_back(v);
	visited[v] = true;
	for(int i = 0; i < inc[v].size() ; i++){
		if(!visited[inc[v][i].first] && inc[v][i].second > 0 && inc[v][i].second < not_reachable){
			inc_DFS(inc[v][i].first);
		} else if(visited[inc[v][i].first] && vertexes[vertexes.size()-2] != inc[v][i].first
			&& inc[v][i].second > 0 && inc[v][i].second < not_reachable){
			int j;
			int sum = 0;
			vertexes.push_back(inc[v][i].first);
			for( j = 0; j < vertexes.size(); j++)
				if(vertexes[j] == inc[v][i].first)
					break;

			for(int k = j; k < vertexes.size() - 1; k++){
				qDebug() << vertexes[k ];
				sum += fromto(vertexes[k], vertexes[k+1]);
			}
			qDebug() << vertexes[j];

			if(sum > size) {
				answer.clear();
				for(int k = j; k < vertexes.size() ; k++)
					answer.push_back(vertexes[k]);
				size = sum;
			}

			qDebug()<< "sum "<< sum<< "\n--------";
			vertexes.pop_back();
		}
	}
	vertexes.pop_back();
	visited[v] = false;

}

int Graph::fromto(int i, int j){

	for(int k = 0; k < inc[i].size(); k++)
		if(inc[i][k].first == j)
			return inc[i][k].second;
	return 0;
}



void Graph::drawgraph()
{
	if(adjacency)
		adj_graph();
	else
		inc_graph();

}

void Graph::adj_graph()
{

	ofstream file;
	file.open ("graph.dot");

	//cout<<"Решение: ";
	file << "graph { \n" \
	<< "graph [size =\"8, 8!\" overlap = false,fontsize=8] ;\n"\
	<< "node [fontsize=8,shape=circle,margin=0, width=0.2, style=filled];\n"\
	<< "edge [fontsize=8];\n"\
	<< "splines = true;\n"\
	<< "overlap=false;\n"\
	<< "nodesep=2;\n"\
	<< "sep=\"+15,25\";\n";
	for(int i = 0; i < adj.size(); i++)
		for(int j = i+1; j < adj.size();j++){
			if(adj[i][j] < 100)
			file<<"\t"<< i <<" -- " << j << "[xlabel= " << adj[i][j] << ", fontcolor=red]"<<"\n";
		}
	file << "}";
	file.close();
	system("neato -Tpng graph.dot -O ");
	emit setpicture();
	//emit setgraph();
}

void Graph::inc_graph()
{

	ofstream file;
	file.open ("graph.dot");

	//cout<<"Решение: ";
	file << "graph { \n" \
	<< "graph [overlap = false, size =\"8, 8!\",  fontsize=8] ;\n"\
	<< "node [fontsize=8,shape=circle,margin=0, width=0.2,style=filled ];\n"\
	<< "edge [fontsize=8];\n"\
	<< "splines = true;\n"\
	<< "overlap=false;\n"\
	<< "nodesep=2;\n"\
	<< "sep=\"+15,25\";\n";
	bool visited[inc.size()];
	for(int i = 0; i < inc.size(); i++)
		visited[i] = -1;
	visited[0]=0;
	for(int i = 0; i < inc.size(); i++){
		for(int j = 0; j < inc[i].size();j++){
			if(visited [inc[i][j].first] != 1 )
				file<<"\t"<< i <<" -- " << inc[i][j].first << "[xlabel= " << inc[i][j].second << ",fontcolor=red]"<<"\n";
		}
		visited[i] = 0 ;
	}
	file << "}";
	file.close();
	system("neato -Tpng graph.dot -O ");
	emit setpicture();
}

void Graph::answergraph(){

	ofstream file;
	file.open ("graf.dot");

	//cout<<"Решение: ";
	file << "graph { \n" \
	<< "graph [size =\"5, 5!\" overlap = false,fontsize=8] ;\n"\
	<< "node [fontsize=8,shape=circle,margin=0, width=0.2, style=filled];\n"\
	<< "edge [fontsize=8];\n"\
	<< "splines = true;\n"\
	<< "overlap=false;\n"\
	<< "nodesep=2;\n"\
	<< "sep=\"+15,15\";\n";
	if(adjacency){
		int i;
		for( i =0; i < answer.size() -2; i++)
			file<<"\t"<< answer[i] <<" -- " << answer[i+1] << "[xlabel= " << adj[answer[i]][answer[i+1]] << ", fontcolor=red]"<<"\n";
		file<<"\t"<< answer[i] <<" -- " << answer[0] << "[xlabel= " << adj[answer[i]][answer[0]] << ", fontcolor=red]"<<"\n";

	} else {
		for(int i =0; i < answer.size() - 1; i++)
			file<<"\t"<< answer[i] <<" -- " << answer[i+1] << "[xlabel= " << fromto(answer[i], answer[i+1]) << ", fontcolor=red]"<<"\n";

	}
	file << "}";
	file.close();
	system("neato -Tpng graf.dot -O ");

}
