#include "Pest.h"

Pest::Pest(QString n, int ac, int kick, int dmg, QVector<QString> prod)
{
	name = n;
	appear_chance = ac;
	kick_away_chance = kick;
	damage = dmg;
	d_products = prod;
}

Pest::Pest(){}
QString Pest::GetRandomProduct()
{
	if(ProductsCount() != 0)
		return d_products[qrand() % ProductsCount()];
	else
		return 0;
}
