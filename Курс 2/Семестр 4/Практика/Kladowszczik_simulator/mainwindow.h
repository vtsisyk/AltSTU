#ifndef MAINWINDOW_H
#define MAINWINDOW_H
/* сделать киллер-фичу в виде нажатия кнопки стопицот раз*/
#include <QMainWindow>
#include <Kladowszczik.h>
#include <Storehouse.h>
#include <Pest.h>
#include <QString>
#include <QStringList>
#include <QGraphicsScene>
#include <QDialog>

#include <QTableWidgetItem>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QStandardItem>
#include "Game.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:



	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:

	void StartGame();
	void FillEquip(QVector<Game::equip_price_t> equip);
	void FillMoney(int);
	void FillExp(int);
	void FillRespect(int);
	void FillStrength(int);
	void FillPerception(int);
	void FillCharisma(int charisma);
	void FillLuck(int);
	void FillSize(int );
	void FillFreespace(int);
	void BuffButtonsDisable();
	void BuffButtonsEnable();
	void BuffButtonPressed();
	void UpdatePrice(QStandardItem *item);
	void SellProductSelect(QStandardItem *item);
	void SellProducts();
	void UpdateEquip();
	void Blank();
	void ChangeSignalState(bool);
	void FillStorehouseTable(QLinkedList<Storehouse::product_t>);
	void FillSecurity(int lvl);
	void UnexpectedVictory();
	void GameOver();




signals:
	void SetBuffString(QString);
	void BuffStorehouse(int);
	void DecMoney(int);
private:
	Ui::MainWindow *ui;
	Game* game;
	QGraphicsScene *scene;
	QGraphicsEllipseItem *ellipse;
	QGraphicsRectItem *rectangle;
	QGraphicsTextItem *text;
	QStandardItemModel* tableModel;
	QStandardItemModel* storehouse_model;

	QVector<bool>products_count ;
	QVector<bool> equip_installed;
	QVector<bool> equip_tp_install;
	int magic= 0;
	int to_inst_equip = 0;
	int to_buff= 0;
	bool block_signals = false;
	QMessageBox messageBox;

};

#endif // MAINWINDOW_H
