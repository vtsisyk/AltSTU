#ifndef STOREHOUSE_H
#define STOREHOUSE_H

#include <QVector>
#include <QString>
#include <QPair>
#include <QLinkedList>
#include <iterator>
#include <QDebug>



class Storehouse
{
public:
	struct item{
		QPair<int, int> pos;
		int space_taken;
	};

	struct product_t{
		QString name; /* наименование товара */
		int size; /* количество на складе */
		int price; /* стоимость каждой единицы товара */
		int was_sold = 0;
		QVector<struct item > place; /* местоположение каждого товра */
	};


private:
	struct shelf{
		int space;
		int freespace;
	};

	int size; /* размер склада, сколько в нем есть места */
	int freespace; /* свободного места на складе */
	double security_lvl; /* уровень защищенности склада. от 1 до 100 */
	QStringList equipment; /* установленное оборудование, пока так */

	Storehouse();
	Storehouse(Storehouse const&) = delete;
	Storehouse& operator=(Storehouse const&) = delete;



	 QLinkedList<product_t> store; /* сам склад */

	struct shelf **space_matrix; /* матрица свободного места */
public:

	const int storage = 1000; /* размер самого склада */
	const int capacity = 100; /* размер ячейки в складе */

	double UsedSpace();

	inline void AdjustSecurityLvl(int lvl) {security_lvl = lvl;}
	inline int GetSecurityLvl(){return security_lvl;}

	inline void SetSize(int s){size = s; }
	inline int GetSize(){return size;}
	inline int HaveProducts(){return store.size();}
	inline void SetFreespace(int f){freespace = f; }
	inline int GetFreeSpace(){return freespace;}



	int InstQuip(QString name);

	QLinkedList<product_t> WhatWeHave(){return store;}


	bool IsEmpty(){return !(size-freespace);}
	int HaveEnoughSpace(int to_place){return (freespace - to_place);}
	bool HaveProduct(QString name);

	int HowMuch(QString name);
	QString ProductAt(int pos);


	int  FindPlace(int *q, int *w);
	int AddItem(QString name, int size, int price);
	int RemoveItem(QString name, int size );


	/* реализация singleton */
	static Storehouse& Instance(){
		static Storehouse instance;
		return instance;
	}

};

#endif // STOREHOUSE_H
