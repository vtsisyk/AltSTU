#-------------------------------------------------
#
# Project created by QtCreator 2016-06-27T15:59:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kladowszczik_simulator
TEMPLATE = app

QMAKE_CXXFLAGS += -Wall -pedantic
CONFIG += c++11
SOURCES += main.cpp\
        mainwindow.cpp \
    Kladowszczik.cpp \
    Pest.cpp \
    Storehouse.cpp \
    Game.cpp

HEADERS  += mainwindow.h \
    Kladowszczik.h \
    Pest.h \
    Storehouse.h \
    Game.h

FORMS    += mainwindow.ui
