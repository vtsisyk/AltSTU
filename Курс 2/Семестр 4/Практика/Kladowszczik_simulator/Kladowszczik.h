#ifndef KLADOWSZCZIK_H
#define KLADOWSZCZIK_H

#include <QString>

class Kladowszczik
{


private:
	Kladowszczik();
	Kladowszczik(Kladowszczik const&) = delete;
	Kladowszczik& operator=(Kladowszczik const&) = delete;

	/*
	 * опыт - от 0 до 30
	 * уважение от 0 до 100
	 * сила от 1 до 10
	 * восприятие от 1 до 10
	 * обаяние от 1 до 10
	 * удача от 1 до 10
	 */

	int has_money; /* денег всего */
	int respect; /* уважение начальства */
	short exp; /* стаж работы */
	int strength; /* сила. Нужна в борьбе с вором */
	int perception; /* восприятие. Нужна для обнаружения
вредителя до порчи имущества*/
	int charisma; /* обаяние. Нужна в общении с начальством */
	int luck; /* удача. Учитывается во всех действиях */


public:
	static Kladowszczik& Instance(){
		static Kladowszczik instance;
		return instance;
	}

	inline int MoneyStatus(){ return has_money; }
	 void Salary(int money) {has_money += money;}

	inline int GetRespLvl() { return respect;}
	inline void ChgRespLvl(int resp) {respect += resp;}

	inline void IncExpLvl() { exp++; }
	inline short GetExpLvl(){return exp ;}

	inline void ChgStrengthLvl(int s){if(strength < 10) strength += s;}
	inline void ChgPerceptionLvl(int p){if(perception < 10) perception += p;}
	inline void ChgCharismaLvl(int c){if(charisma < 10) charisma+= c;}
	inline void ChgLuckLvl(int lk){if(luck < 10) luck += lk;}

	inline int GetStrengthLvl(){return strength;}
	inline int GetPerceptionLvl(){return perception;}
	inline int GetCharismaLvl(){return charisma;}
	inline int GetLuckLvl(){ return luck;}

};

#endif // KLADOWSZCZIK_H
