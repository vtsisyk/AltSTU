/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *startGame;
    QAction *close;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QTableView *tableView_2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *storehouse_title;
    QLabel *storehouse_label;
    QSpacerItem *horizontalSpacer_14;
    QLabel *freespace_title;
    QLabel *freespace_label;
    QSpacerItem *horizontalSpacer_13;
    QPushButton *sell_button;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_3;
    QTableView *tableView;
    QHBoxLayout *horizontalLayout_13;
    QLabel *defence_title;
    QLabel *defence_label;
    QSpacerItem *horizontalSpacer_15;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_2;
    QLabel *money_equip_label;
    QSpacerItem *horizontalSpacer_11;
    QLabel *label;
    QLabel *equip_inst_label;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *BuyButton;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_6;
    QLabel *exp_title;
    QLabel *exp_label;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_7;
    QLabel *reso_title;
    QLabel *resp_label;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_8;
    QLabel *strength_title;
    QLabel *strength_label;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_2;
    QLabel *charisma_title;
    QLabel *charisma_label;
    QSpacerItem *horizontalSpacer_12;
    QHBoxLayout *horizontalLayout_9;
    QLabel *luck_title;
    QLabel *luck_label;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_10;
    QLabel *perception_title;
    QLabel *perception_label;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_11;
    QLabel *money_title;
    QLabel *money_label;
    QSpacerItem *horizontalSpacer_9;
    QSpacerItem *verticalSpacer;
    QPushButton *charismabutton;
    QPushButton *strengthbutton;
    QPushButton *luckbutton;
    QPushButton *perceptionbutton;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *unexwin_button;
    QSpacerItem *horizontalSpacer;
    QPushButton *TurnButton;
    QMenuBar *menuBar;
    QMenu *menu;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(659, 616);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../../../../\320\230\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\321\217/harold.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setAutoFillBackground(false);
        startGame = new QAction(MainWindow);
        startGame->setObjectName(QStringLiteral("startGame"));
        close = new QAction(MainWindow);
        close->setObjectName(QStringLiteral("close"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setEnabled(true);
        tabWidget->setFocusPolicy(Qt::NoFocus);
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setElideMode(Qt::ElideNone);
        tabWidget->setUsesScrollButtons(true);
        tabWidget->setTabsClosable(false);
        tabWidget->setMovable(true);
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        tableView_2 = new QTableView(tab);
        tableView_2->setObjectName(QStringLiteral("tableView_2"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tableView_2->sizePolicy().hasHeightForWidth());
        tableView_2->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(tableView_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        storehouse_title = new QLabel(tab);
        storehouse_title->setObjectName(QStringLiteral("storehouse_title"));

        horizontalLayout_4->addWidget(storehouse_title);

        storehouse_label = new QLabel(tab);
        storehouse_label->setObjectName(QStringLiteral("storehouse_label"));

        horizontalLayout_4->addWidget(storehouse_label);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_14);

        freespace_title = new QLabel(tab);
        freespace_title->setObjectName(QStringLiteral("freespace_title"));

        horizontalLayout_4->addWidget(freespace_title);

        freespace_label = new QLabel(tab);
        freespace_label->setObjectName(QStringLiteral("freespace_label"));

        horizontalLayout_4->addWidget(freespace_label);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_13);

        sell_button = new QPushButton(tab);
        sell_button->setObjectName(QStringLiteral("sell_button"));

        horizontalLayout_4->addWidget(sell_button);


        verticalLayout_2->addLayout(horizontalLayout_4);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_3 = new QVBoxLayout(tab_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        tableView = new QTableView(tab_2);
        tableView->setObjectName(QStringLiteral("tableView"));

        verticalLayout_3->addWidget(tableView);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        defence_title = new QLabel(tab_2);
        defence_title->setObjectName(QStringLiteral("defence_title"));

        horizontalLayout_13->addWidget(defence_title);

        defence_label = new QLabel(tab_2);
        defence_label->setObjectName(QStringLiteral("defence_label"));

        horizontalLayout_13->addWidget(defence_label);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_15);


        verticalLayout_3->addLayout(horizontalLayout_13);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        money_equip_label = new QLabel(tab_2);
        money_equip_label->setObjectName(QStringLiteral("money_equip_label"));

        horizontalLayout_3->addWidget(money_equip_label);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_11);

        label = new QLabel(tab_2);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_3->addWidget(label);

        equip_inst_label = new QLabel(tab_2);
        equip_inst_label->setObjectName(QStringLiteral("equip_inst_label"));

        horizontalLayout_3->addWidget(equip_inst_label);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        BuyButton = new QPushButton(tab_2);
        BuyButton->setObjectName(QStringLiteral("BuyButton"));

        horizontalLayout_3->addWidget(BuyButton);


        verticalLayout_3->addLayout(horizontalLayout_3);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_4 = new QVBoxLayout(tab_3);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        exp_title = new QLabel(tab_3);
        exp_title->setObjectName(QStringLiteral("exp_title"));

        horizontalLayout_6->addWidget(exp_title);

        exp_label = new QLabel(tab_3);
        exp_label->setObjectName(QStringLiteral("exp_label"));

        horizontalLayout_6->addWidget(exp_label);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);


        verticalLayout_4->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        reso_title = new QLabel(tab_3);
        reso_title->setObjectName(QStringLiteral("reso_title"));

        horizontalLayout_7->addWidget(reso_title);

        resp_label = new QLabel(tab_3);
        resp_label->setObjectName(QStringLiteral("resp_label"));

        horizontalLayout_7->addWidget(resp_label);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_5);


        verticalLayout_4->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        strength_title = new QLabel(tab_3);
        strength_title->setObjectName(QStringLiteral("strength_title"));

        horizontalLayout_8->addWidget(strength_title);

        strength_label = new QLabel(tab_3);
        strength_label->setObjectName(QStringLiteral("strength_label"));

        horizontalLayout_8->addWidget(strength_label);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_6);


        verticalLayout_4->addLayout(horizontalLayout_8);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        charisma_title = new QLabel(tab_3);
        charisma_title->setObjectName(QStringLiteral("charisma_title"));

        horizontalLayout_2->addWidget(charisma_title);

        charisma_label = new QLabel(tab_3);
        charisma_label->setObjectName(QStringLiteral("charisma_label"));

        horizontalLayout_2->addWidget(charisma_label);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_12);


        verticalLayout_4->addLayout(horizontalLayout_2);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        luck_title = new QLabel(tab_3);
        luck_title->setObjectName(QStringLiteral("luck_title"));

        horizontalLayout_9->addWidget(luck_title);

        luck_label = new QLabel(tab_3);
        luck_label->setObjectName(QStringLiteral("luck_label"));

        horizontalLayout_9->addWidget(luck_label);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_7);


        verticalLayout_4->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        perception_title = new QLabel(tab_3);
        perception_title->setObjectName(QStringLiteral("perception_title"));

        horizontalLayout_10->addWidget(perception_title);

        perception_label = new QLabel(tab_3);
        perception_label->setObjectName(QStringLiteral("perception_label"));

        horizontalLayout_10->addWidget(perception_label);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_8);


        verticalLayout_4->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        money_title = new QLabel(tab_3);
        money_title->setObjectName(QStringLiteral("money_title"));

        horizontalLayout_11->addWidget(money_title);

        money_label = new QLabel(tab_3);
        money_label->setObjectName(QStringLiteral("money_label"));

        horizontalLayout_11->addWidget(money_label);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_9);


        verticalLayout_4->addLayout(horizontalLayout_11);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer);

        charismabutton = new QPushButton(tab_3);
        charismabutton->setObjectName(QStringLiteral("charismabutton"));

        verticalLayout_4->addWidget(charismabutton);

        strengthbutton = new QPushButton(tab_3);
        strengthbutton->setObjectName(QStringLiteral("strengthbutton"));

        verticalLayout_4->addWidget(strengthbutton);

        luckbutton = new QPushButton(tab_3);
        luckbutton->setObjectName(QStringLiteral("luckbutton"));

        verticalLayout_4->addWidget(luckbutton);

        perceptionbutton = new QPushButton(tab_3);
        perceptionbutton->setObjectName(QStringLiteral("perceptionbutton"));

        verticalLayout_4->addWidget(perceptionbutton);

        tabWidget->addTab(tab_3, QString());

        verticalLayout->addWidget(tabWidget);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        unexwin_button = new QPushButton(centralWidget);
        unexwin_button->setObjectName(QStringLiteral("unexwin_button"));
        unexwin_button->setEnabled(true);
        unexwin_button->setAutoFillBackground(false);
        unexwin_button->setAutoDefault(true);

        horizontalLayout->addWidget(unexwin_button);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        TurnButton = new QPushButton(centralWidget);
        TurnButton->setObjectName(QStringLiteral("TurnButton"));
        TurnButton->setEnabled(true);

        horizontalLayout->addWidget(TurnButton);


        verticalLayout->addLayout(horizontalLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 659, 25));
        menu = new QMenu(menuBar);
        menu->setObjectName(QStringLiteral("menu"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        menuBar->addAction(menu->menuAction());
        menu->addAction(startGame);
        menu->addAction(close);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "C\320\270\320\274\321\203\320\273\321\217\321\202\320\276\321\200 \320\272\320\273\320\260\320\264\320\276\320\262\321\211\320\270\320\272\320\260", 0));
        startGame->setText(QApplication::translate("MainWindow", "\320\235\320\276\320\262\320\260\321\217 \320\270\320\263\321\200\320\260", 0));
        close->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0));
        storehouse_title->setText(QApplication::translate("MainWindow", "\320\240\320\260\320\267\320\274\320\265\321\200 \321\201\320\272\320\273\320\260\320\264\320\260: ", 0));
        storehouse_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        freespace_title->setText(QApplication::translate("MainWindow", "\320\241\320\262\320\276\320\261\320\276\320\264\320\275\320\276 \320\274\320\265\321\201\321\202\320\276: ", 0));
        freespace_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        sell_button->setText(QApplication::translate("MainWindow", "\320\237\321\200\320\276\320\264\320\260\321\202\321\214", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "\320\241\320\272\320\273\320\260\320\264", 0));
        defence_title->setText(QApplication::translate("MainWindow", "\320\243\321\200\320\276\320\262\320\265\320\275\321\214 \321\201\320\272\320\273\320\260\320\264\320\260: ", 0));
        defence_label->setText(QApplication::translate("MainWindow", "1", 0));
        label_2->setText(QApplication::translate("MainWindow", "\320\225\321\201\321\202\321\214 \320\264\320\265\320\275\320\265\320\263", 0));
        money_equip_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label->setText(QApplication::translate("MainWindow", "\320\241\321\202\320\276\320\270\320\274\320\276\321\201\321\202\321\214 \320\277\320\276\320\272\321\203\320\277\320\272\320\270", 0));
        equip_inst_label->setText(QApplication::translate("MainWindow", "0", 0));
        BuyButton->setText(QApplication::translate("MainWindow", "\320\232\321\203\320\277\320\270\321\202\321\214", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "\320\236\320\261\320\276\321\200\321\203\320\264\320\276\320\262\320\260\320\275\320\270\320\265", 0));
        exp_title->setText(QApplication::translate("MainWindow", "\320\241\321\202\320\260\320\266:", 0));
        exp_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        reso_title->setText(QApplication::translate("MainWindow", "\320\243\321\200\320\276\320\262\320\265\320\275\321\214 \321\203\320\262\320\260\320\266\320\265\320\275\320\270\321\217: ", 0));
        resp_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        strength_title->setText(QApplication::translate("MainWindow", "\320\241\320\270\320\273\320\260: ", 0));
        strength_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        charisma_title->setText(QApplication::translate("MainWindow", "\320\236\320\261\320\260\321\217\320\275\320\270\320\265:", 0));
        charisma_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        luck_title->setText(QApplication::translate("MainWindow", "\320\243\320\264\320\260\321\207\320\260: ", 0));
        luck_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        perception_title->setText(QApplication::translate("MainWindow", "\320\222\320\276\321\201\320\277\321\200\320\270\321\217\321\202\320\270\320\265: ", 0));
        perception_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        money_title->setText(QApplication::translate("MainWindow", "\320\224\320\265\320\275\320\265\320\263 \320\265\321\201\321\202\321\214: ", 0));
        money_label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        charismabutton->setText(QApplication::translate("MainWindow", "\320\247\320\270\321\202\320\260\321\202\321\214 \320\272\320\275\320\270\320\266\320\272\320\270(\320\276\320\261\320\260\321\217\320\275\320\270\320\265)", 0));
        strengthbutton->setText(QApplication::translate("MainWindow", "\320\245\320\276\320\264\320\270\321\202\321\214 \320\262 \320\272\320\260\321\207\320\260\320\273\320\272\321\203(\321\201\320\270\320\273\320\260)", 0));
        luckbutton->setText(QApplication::translate("MainWindow", "\320\241\320\277\320\276\321\200\320\270\321\202\321\214 \320\262 \320\270\320\275\321\202\320\265\321\200\320\275\320\265\321\202\320\260\321\205(\321\203\320\264\320\260\321\207\320\260)", 0));
        perceptionbutton->setText(QApplication::translate("MainWindow", "\320\234\320\275\320\276\320\263\320\276 \321\201\320\277\320\260\321\202\321\214(\320\262\320\276\321\201\320\277\321\200\320\270\321\217\321\202\320\270\320\265)", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "\320\230\320\263\321\200\320\276\320\272", 0));
        unexwin_button->setText(QApplication::translate("MainWindow", "\320\227\320\262\320\276\320\275\320\276\320\272 ", 0));
        TurnButton->setText(QApplication::translate("MainWindow", "\320\241\320\264\320\265\320\273\320\260\321\202\321\214 \321\205\320\276\320\264", 0));
        menu->setTitle(QApplication::translate("MainWindow", "\320\234\320\265\320\275\321\216", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
