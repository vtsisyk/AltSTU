/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Kladowszczik_simulator/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QLinkedList>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[36];
    char stringdata0[470];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 13), // "SetBuffString"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 14), // "BuffStorehouse"
QT_MOC_LITERAL(4, 41, 8), // "DecMoney"
QT_MOC_LITERAL(5, 50, 9), // "StartGame"
QT_MOC_LITERAL(6, 60, 9), // "FillEquip"
QT_MOC_LITERAL(7, 70, 28), // "QVector<Game::equip_price_t>"
QT_MOC_LITERAL(8, 99, 5), // "equip"
QT_MOC_LITERAL(9, 105, 9), // "FillMoney"
QT_MOC_LITERAL(10, 115, 7), // "FillExp"
QT_MOC_LITERAL(11, 123, 11), // "FillRespect"
QT_MOC_LITERAL(12, 135, 12), // "FillStrength"
QT_MOC_LITERAL(13, 148, 14), // "FillPerception"
QT_MOC_LITERAL(14, 163, 12), // "FillCharisma"
QT_MOC_LITERAL(15, 176, 8), // "charisma"
QT_MOC_LITERAL(16, 185, 8), // "FillLuck"
QT_MOC_LITERAL(17, 194, 8), // "FillSize"
QT_MOC_LITERAL(18, 203, 13), // "FillFreespace"
QT_MOC_LITERAL(19, 217, 18), // "BuffButtonsDisable"
QT_MOC_LITERAL(20, 236, 17), // "BuffButtonsEnable"
QT_MOC_LITERAL(21, 254, 17), // "BuffButtonPressed"
QT_MOC_LITERAL(22, 272, 11), // "UpdatePrice"
QT_MOC_LITERAL(23, 284, 14), // "QStandardItem*"
QT_MOC_LITERAL(24, 299, 4), // "item"
QT_MOC_LITERAL(25, 304, 17), // "SellProductSelect"
QT_MOC_LITERAL(26, 322, 12), // "SellProducts"
QT_MOC_LITERAL(27, 335, 11), // "UpdateEquip"
QT_MOC_LITERAL(28, 347, 5), // "Blank"
QT_MOC_LITERAL(29, 353, 17), // "ChangeSignalState"
QT_MOC_LITERAL(30, 371, 19), // "FillStorehouseTable"
QT_MOC_LITERAL(31, 391, 34), // "QLinkedList<Storehouse::produ..."
QT_MOC_LITERAL(32, 426, 12), // "FillSecurity"
QT_MOC_LITERAL(33, 439, 3), // "lvl"
QT_MOC_LITERAL(34, 443, 17), // "UnexpectedVictory"
QT_MOC_LITERAL(35, 461, 8) // "GameOver"

    },
    "MainWindow\0SetBuffString\0\0BuffStorehouse\0"
    "DecMoney\0StartGame\0FillEquip\0"
    "QVector<Game::equip_price_t>\0equip\0"
    "FillMoney\0FillExp\0FillRespect\0"
    "FillStrength\0FillPerception\0FillCharisma\0"
    "charisma\0FillLuck\0FillSize\0FillFreespace\0"
    "BuffButtonsDisable\0BuffButtonsEnable\0"
    "BuffButtonPressed\0UpdatePrice\0"
    "QStandardItem*\0item\0SellProductSelect\0"
    "SellProducts\0UpdateEquip\0Blank\0"
    "ChangeSignalState\0FillStorehouseTable\0"
    "QLinkedList<Storehouse::product_t>\0"
    "FillSecurity\0lvl\0UnexpectedVictory\0"
    "GameOver"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      27,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  149,    2, 0x06 /* Public */,
       3,    1,  152,    2, 0x06 /* Public */,
       4,    1,  155,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,  158,    2, 0x0a /* Public */,
       6,    1,  159,    2, 0x0a /* Public */,
       9,    1,  162,    2, 0x0a /* Public */,
      10,    1,  165,    2, 0x0a /* Public */,
      11,    1,  168,    2, 0x0a /* Public */,
      12,    1,  171,    2, 0x0a /* Public */,
      13,    1,  174,    2, 0x0a /* Public */,
      14,    1,  177,    2, 0x0a /* Public */,
      16,    1,  180,    2, 0x0a /* Public */,
      17,    1,  183,    2, 0x0a /* Public */,
      18,    1,  186,    2, 0x0a /* Public */,
      19,    0,  189,    2, 0x0a /* Public */,
      20,    0,  190,    2, 0x0a /* Public */,
      21,    0,  191,    2, 0x0a /* Public */,
      22,    1,  192,    2, 0x0a /* Public */,
      25,    1,  195,    2, 0x0a /* Public */,
      26,    0,  198,    2, 0x0a /* Public */,
      27,    0,  199,    2, 0x0a /* Public */,
      28,    0,  200,    2, 0x0a /* Public */,
      29,    1,  201,    2, 0x0a /* Public */,
      30,    1,  204,    2, 0x0a /* Public */,
      32,    1,  207,    2, 0x0a /* Public */,
      34,    0,  210,    2, 0x0a /* Public */,
      35,    0,  211,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, 0x80000000 | 31,    2,
    QMetaType::Void, QMetaType::Int,   33,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SetBuffString((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->BuffStorehouse((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->DecMoney((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->StartGame(); break;
        case 4: _t->FillEquip((*reinterpret_cast< QVector<Game::equip_price_t>(*)>(_a[1]))); break;
        case 5: _t->FillMoney((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->FillExp((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->FillRespect((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->FillStrength((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->FillPerception((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->FillCharisma((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->FillLuck((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->FillSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->FillFreespace((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->BuffButtonsDisable(); break;
        case 15: _t->BuffButtonsEnable(); break;
        case 16: _t->BuffButtonPressed(); break;
        case 17: _t->UpdatePrice((*reinterpret_cast< QStandardItem*(*)>(_a[1]))); break;
        case 18: _t->SellProductSelect((*reinterpret_cast< QStandardItem*(*)>(_a[1]))); break;
        case 19: _t->SellProducts(); break;
        case 20: _t->UpdateEquip(); break;
        case 21: _t->Blank(); break;
        case 22: _t->ChangeSignalState((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->FillStorehouseTable((*reinterpret_cast< QLinkedList<Storehouse::product_t>(*)>(_a[1]))); break;
        case 24: _t->FillSecurity((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 25: _t->UnexpectedVictory(); break;
        case 26: _t->GameOver(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::SetBuffString)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::BuffStorehouse)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::DecMoney)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 27)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 27;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::SetBuffString(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::BuffStorehouse(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::DecMoney(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
