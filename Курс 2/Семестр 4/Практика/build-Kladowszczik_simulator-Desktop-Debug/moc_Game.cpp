/****************************************************************************
** Meta object code from reading C++ file 'Game.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Kladowszczik_simulator/Game.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QLinkedList>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Game.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Game_t {
    QByteArrayData data[31];
    char stringdata0[358];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Game_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Game_t qt_meta_stringdata_Game = {
    {
QT_MOC_LITERAL(0, 0, 4), // "Game"
QT_MOC_LITERAL(1, 5, 9), // "FillEquip"
QT_MOC_LITERAL(2, 15, 0), // ""
QT_MOC_LITERAL(3, 16, 28), // "QVector<Game::equip_price_t>"
QT_MOC_LITERAL(4, 45, 9), // "FillMoney"
QT_MOC_LITERAL(5, 55, 5), // "money"
QT_MOC_LITERAL(6, 61, 7), // "FillExp"
QT_MOC_LITERAL(7, 69, 3), // "exp"
QT_MOC_LITERAL(8, 73, 11), // "FillRespect"
QT_MOC_LITERAL(9, 85, 7), // "respect"
QT_MOC_LITERAL(10, 93, 12), // "FillStrength"
QT_MOC_LITERAL(11, 106, 8), // "strength"
QT_MOC_LITERAL(12, 115, 14), // "FillPerception"
QT_MOC_LITERAL(13, 130, 10), // "perception"
QT_MOC_LITERAL(14, 141, 8), // "FillLuck"
QT_MOC_LITERAL(15, 150, 4), // "luck"
QT_MOC_LITERAL(16, 155, 12), // "FillCharisma"
QT_MOC_LITERAL(17, 168, 8), // "charisma"
QT_MOC_LITERAL(18, 177, 19), // "ActivateBuffButtons"
QT_MOC_LITERAL(19, 197, 14), // "FillStoreTable"
QT_MOC_LITERAL(20, 212, 34), // "QLinkedList<Storehouse::produ..."
QT_MOC_LITERAL(21, 247, 12), // "FillFreepace"
QT_MOC_LITERAL(22, 260, 8), // "FillSize"
QT_MOC_LITERAL(23, 269, 17), // "ChangeSignalState"
QT_MOC_LITERAL(24, 287, 12), // "FillSecurity"
QT_MOC_LITERAL(25, 300, 8), // "gameover"
QT_MOC_LITERAL(26, 309, 4), // "Turn"
QT_MOC_LITERAL(27, 314, 13), // "GetBuffString"
QT_MOC_LITERAL(28, 328, 8), // "DecMoney"
QT_MOC_LITERAL(29, 337, 14), // "BuffStorehouse"
QT_MOC_LITERAL(30, 352, 5) // "level"

    },
    "Game\0FillEquip\0\0QVector<Game::equip_price_t>\0"
    "FillMoney\0money\0FillExp\0exp\0FillRespect\0"
    "respect\0FillStrength\0strength\0"
    "FillPerception\0perception\0FillLuck\0"
    "luck\0FillCharisma\0charisma\0"
    "ActivateBuffButtons\0FillStoreTable\0"
    "QLinkedList<Storehouse::product_t>\0"
    "FillFreepace\0FillSize\0ChangeSignalState\0"
    "FillSecurity\0gameover\0Turn\0GetBuffString\0"
    "DecMoney\0BuffStorehouse\0level"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Game[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      15,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x06 /* Public */,
       4,    1,  112,    2, 0x06 /* Public */,
       6,    1,  115,    2, 0x06 /* Public */,
       8,    1,  118,    2, 0x06 /* Public */,
      10,    1,  121,    2, 0x06 /* Public */,
      12,    1,  124,    2, 0x06 /* Public */,
      14,    1,  127,    2, 0x06 /* Public */,
      16,    1,  130,    2, 0x06 /* Public */,
      18,    0,  133,    2, 0x06 /* Public */,
      19,    1,  134,    2, 0x06 /* Public */,
      21,    1,  137,    2, 0x06 /* Public */,
      22,    1,  140,    2, 0x06 /* Public */,
      23,    1,  143,    2, 0x06 /* Public */,
      24,    1,  146,    2, 0x06 /* Public */,
      25,    0,  149,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      26,    0,  150,    2, 0x0a /* Public */,
      27,    1,  151,    2, 0x0a /* Public */,
      28,    1,  154,    2, 0x0a /* Public */,
      29,    1,  157,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 20,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,   30,

       0        // eod
};

void Game::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Game *_t = static_cast<Game *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->FillEquip((*reinterpret_cast< QVector<Game::equip_price_t>(*)>(_a[1]))); break;
        case 1: _t->FillMoney((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->FillExp((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->FillRespect((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->FillStrength((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->FillPerception((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->FillLuck((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->FillCharisma((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->ActivateBuffButtons(); break;
        case 9: _t->FillStoreTable((*reinterpret_cast< QLinkedList<Storehouse::product_t>(*)>(_a[1]))); break;
        case 10: _t->FillFreepace((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->FillSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->ChangeSignalState((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->FillSecurity((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->gameover(); break;
        case 15: _t->Turn(); break;
        case 16: _t->GetBuffString((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 17: _t->DecMoney((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->BuffStorehouse((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Game::*_t)(QVector<Game::equip_price_t> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillEquip)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillMoney)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillExp)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillRespect)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillStrength)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillPerception)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillLuck)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillCharisma)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (Game::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::ActivateBuffButtons)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (Game::*_t)(QLinkedList<Storehouse::product_t> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillStoreTable)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillFreepace)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillSize)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (Game::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::ChangeSignalState)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (Game::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::FillSecurity)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (Game::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Game::gameover)) {
                *result = 14;
                return;
            }
        }
    }
}

const QMetaObject Game::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Game.data,
      qt_meta_data_Game,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Game::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Game::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Game.stringdata0))
        return static_cast<void*>(const_cast< Game*>(this));
    return QObject::qt_metacast(_clname);
}

int Game::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void Game::FillEquip(QVector<Game::equip_price_t> _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Game::FillMoney(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Game::FillExp(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Game::FillRespect(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Game::FillStrength(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Game::FillPerception(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Game::FillLuck(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Game::FillCharisma(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Game::ActivateBuffButtons()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void Game::FillStoreTable(QLinkedList<Storehouse::product_t> _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Game::FillFreepace(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Game::FillSize(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Game::ChangeSignalState(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void Game::FillSecurity(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void Game::gameover()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
