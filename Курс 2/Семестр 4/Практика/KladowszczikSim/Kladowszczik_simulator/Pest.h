#ifndef PEST_H
#define PEST_H

#include <QString>
#include <QStringList>
#include <QVector>
class Pest
{
private:
	
	/* шанс появление от 1 до 100
	 * шанс прогнать от 1 до 100
	 * урон от 1 до 100
	 */

	QString name; /* вид вредителя*/
	int appear_chance; /* вероятность появления */
	int kick_away_chance; /* вероятность прогнать */
	int damage; /* сколько урона наносит */
	QVector<QString> d_products; /* список уничтожаемых продуктов */
public:
	Pest();
	Pest(QString name, int ac, int kick, \
	     int damage, QVector<QString> prod );

	int GetAppearChance() {return appear_chance;}
	int GetDamage(){ return damage;}
	int GetKickChance(){return kick_away_chance;}\
	int ProductsCount() {return d_products.size();}
	QString GetRandomProduct();
	QString GetName(){return name;}
};

#endif // PEST_H
