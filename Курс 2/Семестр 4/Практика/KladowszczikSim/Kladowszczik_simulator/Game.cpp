#include "Game.h"

Game::Game(QObject *parent) :
	QObject(parent)
{

	month = 0;
	qsrand (QDateTime::currentMSecsSinceEpoch());
	kladowszczik =  & Kladowszczik::Instance();
	storehouse = & Storehouse::Instance();

	QDir dir("../Pests");
	dir.setFilter(QDir::Files | QDir::NoSymLinks);


	QStringList list = dir.entryList();
	for(int i = 0; i < list.size(); i++ ){
		QFile file(dir.absoluteFilePath(list.at(i)));
		QVector<QString> pest_products;


		if(!file.open(QIODevice::ReadOnly)) {
			QMessageBox::information(0, "error", file.errorString());
		}

		QTextStream in(&file);
		/* Вредители и что они портят/уносят */

		/* сделать считывание из файла */
		QString name = in.readLine();
		QString line = in.readLine();
		while(!in.atEnd()){
			line = in.readLine();
			if(line == "")
				break;
			pest_products.push_back(line);

		}
		line = in.readLine();
		int appear_chance = line.toInt();
		line = in.readLine();
		int kick_away = line.toInt();
		line = in.readLine();
		int damage = line.toInt();
		Pest pst(name, appear_chance, kick_away, damage, pest_products);
		pests.push_back(name);
		pests_ar.push_back(pst);



		file.close();
	}

	QDir products("../Products");

	products.setFilter(QDir::Files | QDir::NoSymLinks);
	list = products.entryList();
	for(int i = 0; i < list.size(); i++ ){
		QFile file(products.absoluteFilePath(list.at(i)));
		item_price_t temp;
		if(!file.open(QIODevice::ReadOnly)) {
			QMessageBox::information(0, "error", file.errorString());
		}
		QTextStream in(&file);
		QString line = in.readLine();
		temp.name = line;
		line = in.readLine();
		temp.price = line.toInt();
		produkty.push_back(temp);
		file.close();
	}



	struct equip_price_t tmp;

	QDir eq("../Equipment");

	eq.setFilter(QDir::Files | QDir::NoSymLinks);
	list = eq.entryList();
	for(int i = 0; i < list.size(); i++ ){
		QFile file(eq.absoluteFilePath(list.at(i)));
		if(!file.open(QIODevice::ReadOnly)) {
			QMessageBox::information(0, "error", file.errorString());
		}

		QTextStream in(&file);
		QString line = in.readLine();
		tmp.name = line;
		line = in.readLine();
		tmp.price = line.toInt();
		line = in.readLine();
		tmp.buff = line.toInt();

		equip.push_back(tmp);
		equipment.push_back(tmp.name);
		file.close();
	}


	buff = "";


}
void Game::MoneyShow()
{
	emit FillMoney(kladowszczik->MoneyStatus());

}
void Game::EquipShow()
{
	emit FillEquip(equip);

}

void Game::ExpShow()
{
	emit FillExp(kladowszczik->GetExpLvl());
}
void Game::RespectShow()
{
	emit FillRespect(kladowszczik->GetRespLvl());
}
void Game::StrengthShow()
{
	emit FillStrength(kladowszczik->GetStrengthLvl());
}
void Game::PerceptionShow()
{
	emit FillPerception(kladowszczik->GetPerceptionLvl());
}

void Game::CharismaShow()
{
	emit FillCharisma(kladowszczik->GetCharismaLvl());

}
void Game::LuckShow()
{
	emit FillLuck(kladowszczik->GetLuckLvl());
}
void Game::DecMoney(int money)
{
	kladowszczik->Salary( money );

}

void Game::BuffStorehouse(int level)
{
	storehouse->AdjustSecurityLvl(level);

}
void Game::StorehouseSecurityShow()
{
	emit FillSecurity(storehouse->GetSecurityLvl());

}

void Game::SizeShow(){

	emit FillSize(storehouse->GetSize());
}

void Game::FreespaceShow()
{
	emit FillFreepace(storehouse->GetFreeSpace());
}

void Game::SignalSwitch(bool state)
{
	emit ChangeSignalState(state);
}
void  Game::GetBuffString(QString buf)
{
	buff = buf;
	whenbuff = month;
	if(buff == "Strength"){
		kladowszczik->ChgStrengthLvl(-month - 4);
		StrengthShow();
	} else if(buff == "Perception"){
		kladowszczik->ChgPerceptionLvl(-month - 4);
		PerceptionShow();
	} else if(buff == "Charisma"){
		kladowszczik->ChgCharismaLvl(-month - 4);
		CharismaShow();
	} else if(buff == "Luck"){
		kladowszczik->ChgLuckLvl(-month - 4);
		LuckShow();
	}

}
void Game::Turn(){

//	int i = 0;
//while(1){
	AddToMonthlyString("ход сделан");
	AddToMonthlyString("Прошел месяц " + months[month] );
	/* прикрутить риалтайм??*/

	/* ходят вредители */
	Raid();
	/* со склада забирается часть товара */
	ProductsTaken();
	/* привозится новый товар */
	NewProduct();

	/* выдается зарплата */
	SetSalary();

	/* выкидывается отчет за месяц */
	MonthyResult();

	QMessageBox messageBox;

	month++;
	kladowszczik->ChgRespLvl(1);
	if(month == 12){
		month = 0;
		kladowszczik->IncExpLvl();
		kladowszczik->ChgRespLvl(1);



		if(buff == "Strength"){
			kladowszczik->ChgStrengthLvl(whenbuff + 5);
			StrengthShow();
		} else if(buff == "Perception"){
			kladowszczik->ChgPerceptionLvl(whenbuff + 5);
			PerceptionShow();
		} else if(buff == "Charisma"){
			kladowszczik->ChgCharismaLvl(whenbuff + 5);
			CharismaShow();
		} else if(buff == "Luck"){
			kladowszczik->ChgLuckLvl(whenbuff + 5);
			LuckShow();
		}


		emit ActivateBuffButtons();
		buff ="";
		whenbuff = 0;
		messageBox.critical(0,"","Прошел год!\n");
		messageBox.setFixedSize(500,200);
		messageBox.show();
		ExpShow();

	}




	ProductsShow();
	FreespaceShow();
	RespectShow();
	MoneyShow();

	if(kladowszczik->GetRespLvl() < -15){
		QMessageBox msgBox;
		msgBox.setWindowTitle("Проигрыш");
		msgBox.setText("Вы проиграли");
		msgBox.setStandardButtons(QMessageBox::Yes);
		if(msgBox.exec() == QMessageBox::Yes){
			emit gameover();
		}
	}

	if(kladowszczik->GetExpLvl() == 25 && lets_continue == false){
		QMessageBox msgBox;
		msgBox.setWindowTitle("Победа!");
		msgBox.setText("Поздравлю! Вы достойно работали на складе"
			       "и теперь можете уйти на покой. Продолжить игру?");
		msgBox.setStandardButtons(QMessageBox::Yes);
		msgBox.addButton(QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::No);
		if(msgBox.exec() == QMessageBox::Yes){
			lets_continue = true;
		} else {
			emit gameover();
		}
	}
//}
}
void Game::ProductsTaken()
{
	qsrand (QDateTime::currentMSecsSinceEpoch());
	int randomdelivery = qrand() % 100;
	if(randomdelivery < 50)
		return;
	if(storehouse->HaveProducts() == 0)
		return;
	int prod_to_dest = qrand() % storehouse->HaveProducts();
	if(prod_to_dest == 0)
		return;

	for(int i = 0; i < prod_to_dest; i++){
		prod_to_dest = prod_to_dest -1;
		int rproduct ;
		if(prod_to_dest == 0)
			rproduct = 0;
		else
		 rproduct  = qrand() % (prod_to_dest);
		int amount;
		QString name = storehouse->ProductAt(rproduct);
		amount = storehouse->HowMuch(name);
		if(amount/2 == 0 )
			return;
		amount = qrand() % (amount / 2);
		if(amount == 0){
			amount = storehouse->HowMuch(name);
		}

		storehouse->RemoveItem(name,amount);
		AddToMonthlyString("Со склада забрали " + name + " в количестве " +QString::number(amount) + " единиц"  );

		}


}

void Game::MonthyResult()
{



	if(report.pest != ""){
		AddToMonthlyString( "В этом месяце на склад напали " + report.pest);

		if(report.items_destroyed.size() == 0)
			AddToMonthlyString("но они ужаснулись от вашего склада и решили его не трогать");
		else{
			AddToMonthlyString("От них пострадало(товара - единиц):");
			for(int i = 0; i < report.items_destroyed.size(); i++){
				AddToMonthlyString(report.items_destroyed[i].name + " -- " + QString::number(report.items_destroyed[i].size));
			}
		}
	} else {
		AddToMonthlyString("Склад охранялся хорошо!");

	}

	AddToMonthlyString("Верный работник склада получил " + QString::number(report.salary) + " рублей зарплаты ");

	//qDebug("А всего имеет %d рублей ", kladowszczik->MoneyStatus());


	report.items_destroyed.clear();
	QMessageBox messageBox;
	messageBox.critical(0,"", MonthyResultString);
	MonthyResultString="";
	report.pest= "";

}

/*
 * Продаем что-либо со склада
 */
int Game::SellSomething(QString name, int amount)
{
	int money;

	money = storehouse->RemoveItem(name, amount);
	if(money == 0 )
		return 1;
	/* настучит ли начальство по шапке за такое */
	if(kladowszczik->GetLuckLvl() * 10 + qrand() % 100 < 100)
		kladowszczik->ChgRespLvl(-1);
	kladowszczik->Salary(money);

	MoneyShow();
	FreespaceShow();
	return 0;
}

/* Покупаем что-нибудь */
int Game::BuySomething(QString name)
{
	int i;
	for(i = 0 ; i < equip.size() - 1; i++)
		if(equip[i].name == name)
			return 0 ; // уже есть


	/* если есть деньги на установку
	 * Если нет, то говорим, что все плохо
	 */
	if(kladowszczik->MoneyStatus() < equip[i].price ){
		qDebug()<< "Денег нет!";
		return 0;
	}


//	if(storehouse->InstalledEquip(name))
//		return 0;

	storehouse->InstQuip(name);
	kladowszczik->Salary((-1)* equip[i].price);
	/* вычесть количество денег со счета */
	/* Баффнуть склад*/
	return 0;
}
int Game::NewProduct()
{
	/* Определить новый продукт к поступлению (ВБР)(может быть ничего)
	 * Если это не ничего, то определяем количество(стоимость прописана)
	 * пхаем
	 */

	int size = produkty.size() - 1;
	if(size == 0)
		return 0;
	int product_id = qrand() % size;
	if(product_id == produkty.size() - 1 )
		return 0;
	 size = storehouse->GetSize()/3;
	if(size == 0)
		return 0;
	int amount = qrand() % size;
	if(storehouse->HaveEnoughSpace(amount) < 0){
		kladowszczik->ChgRespLvl(-1);
		AddToMonthlyString("На склад хотели завести товар, но для партии не нашлось места. Начальство думает о расширении склада");
		return 0;
	}
	storehouse->AddItem(produkty[product_id ].name,\
			    amount,produkty[product_id ].price);
	AddToMonthlyString("На склад завезли " + produkty[product_id ].name + " в размере " + QString::number(amount) + " единиц.");


	return 0;

}
int Game::Raid()
{
	qsrand (QDateTime::currentMSecsSinceEpoch());

	/* Выбираем кто идет воровать/пожирать */
	int pest_id = qrand() % (pests.size() );

	/* то есть никто не идет */
	if(pest_id == (pests.size() - 1))
		return 0;

	int will_go;
	/* Определяем, пойдут ли они вообще (ВБР) */
	/* случайное число от 0 до 1*/
	will_go = pests_ar[pest_id].GetAppearChance() + qrand() / 100;

	/* то есть  не идет */
	if(will_go < 100)
		return 0;


	/* Определяем, отгонит ли их кладовщик(ВБР) */
	int klad_chce = (kladowszczik->GetExpLvl() * 2 + kladowszczik->GetLuckLvl() + \
	kladowszczik->GetStrengthLvl() + kladowszczik->GetPerceptionLvl() + \
	storehouse->GetSecurityLvl()) / 2;
	double kick = klad_chce/(double)pests_ar[pest_id].GetKickChance() *\
	((double) rand() / (RAND_MAX));

	if(kick > 1)
		return 0;

	/* https://youtu.be/SrwlxcEZC4Q  (все уже украдено до нас)*/
	if(storehouse->IsEmpty())
		return 0;
	int prod_to_dest = qrand() % (pests_ar[pest_id].ProductsCount() - 1) +1 ;
	report.pest = pests_ar[pest_id].GetName();

	/* из списка товаров, которые есть на складе*/
	/* выбрать продукт */
	/* убрать определенное количество со склада */

	for(int i = 0; i < prod_to_dest; i++){
		QString to_eat = pests_ar[pest_id].GetRandomProduct();
		if(storehouse->HaveProduct(to_eat)){
			int how_much = (storehouse->HowMuch(to_eat) / 4);

			if(how_much == 0)
				how_much = storehouse->HowMuch(to_eat);
			else{
				how_much = qrand() % how_much;
			}
			/* проверить на то, не мало ли осталось.*/
			storehouse->RemoveItem(to_eat,how_much);
			struct was_destroyed_t tmp;
			tmp.name = to_eat;
			tmp.size = how_much;
			report.items_destroyed.push_back(tmp);

		}


	}

	/* Если нет, то определяем уровень повреждений:
	 * всего поврежденных предметов(ВБР), количество
	 * поврежденного товара определенного вида(0 - всего товара(ВБР))
	 */

/*Выкидываем со склада поврежденное
	 * Определяем, заметило ли начальство рейд(ВБР) и выпишет ли
	 * наставлений кладовщику.
	 */

	int diplomatics = (kladowszczik->GetCharismaLvl() * 5 +  kladowszczik->GetRespLvl() * 5) / 2;
	if(diplomatics  > 100)
		/*повезло и не заметили */
		return 0;
	else
		kladowszczik->ChgRespLvl(-10);
	return 0;

}
/*
 * Опредляем зарплату за месяц
 */

int Game::SetSalary(){

	/* Определить уровень уважения
	 * Размер склада
	 * Это умножаем на базовую зарплату и дать ее кладовщику
	 */

	double bonus =   (kladowszczik->GetLuckLvl() * 2 +\
	kladowszczik->GetCharismaLvl() * 2  + kladowszczik->GetExpLvl() * 2 + kladowszczik->GetRespLvl() * 2)/100.0  ;


	int pieniadze = base_salary + base_salary*(bonus);
	kladowszczik->Salary(pieniadze);
	report.salary = pieniadze;
	return 0;


}
