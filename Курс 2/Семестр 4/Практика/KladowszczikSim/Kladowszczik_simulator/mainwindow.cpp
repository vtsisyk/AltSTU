﻿#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	ui->tabWidget->setVisible(false);
	update();

	connect(ui->startGame,SIGNAL(triggered()), this, SLOT(StartGame()));
	connect(ui->close, SIGNAL(triggered()), this, SLOT(close()));

	tableModel = new QStandardItemModel();
	storehouse_model = new QStandardItemModel();


}
void MainWindow::UnexpectedVictory()
{


	if(magic == 100)
	{
		QMessageBox msgBox;
		msgBox.setWindowTitle("Победа?");
		msgBox.setText("Победа?");
		msgBox.setStandardButtons(QMessageBox::Ok);

		if(msgBox.exec() == QMessageBox::Ok){
			GameOver();
		}

	}
	magic++;

}
void MainWindow::FillMoney(int money)
{
	ui->money_label->setText(QString::number(money));
	ui->money_equip_label->setText(QString::number(money));
	update();
}

void MainWindow::FillExp(int exp)
{
	ui->exp_label->setText(QString::number(exp));

}
void MainWindow::FillRespect(int respect)
{
	ui->resp_label->setText(QString::number(respect));
}
void MainWindow::FillStrength(int strength)
{

	ui->strength_label->setText(QString::number(strength));
}
void MainWindow::FillPerception(int perception)
{
	ui->perception_label->setText(QString::number(perception));

}

void MainWindow::FillCharisma(int charisma)
{
	ui->charisma_label->setText(QString::number(charisma));

}
void MainWindow::FillLuck(int luck)
{
	ui->luck_label->setText(QString::number(luck));

}

void MainWindow::FillSize(int size)
{
	ui->storehouse_label->setText( QString::number(size));
}


void MainWindow::FillFreespace(int freespace)
{
	ui->freespace_label->setText( QString::number(freespace));
}

void MainWindow::ChangeSignalState(bool state)
{
	block_signals = state;
}

void MainWindow::FillSecurity(int lvl)
{
	ui->defence_label ->setText(QString::number(lvl));
}
void MainWindow::BuffButtonsDisable()
{
	ui->luckbutton->setEnabled(false);
	ui->strengthbutton->setEnabled(false);
	ui->charismabutton->setEnabled(false);
	ui->perceptionbutton->setEnabled(false);
}

void MainWindow::BuffButtonsEnable()
{
	ui->luckbutton->setEnabled(true);
	ui->strengthbutton->setEnabled(true);
	ui->charismabutton->setEnabled(true);
	ui->perceptionbutton->setEnabled(true);
}

void MainWindow::BuffButtonPressed()
{
	QObject* button = QObject::sender();
	BuffButtonsDisable();
	if(button == ui->strengthbutton){
		emit SetBuffString("Strength");
	//	emit DecStrength(-4);
	}else if(button == ui->charismabutton){
		emit SetBuffString("Charisma");
	//	emit DecCharisma(-4);
	} else if(button == ui->perceptionbutton){
		emit SetBuffString("Perception");
	//	emit DecPerception(-4);
	} else if(button == ui->luckbutton){
		emit SetBuffString("Luck");
	//	emit DecLuck(-4);
	}


}
void MainWindow::FillStorehouseTable(QLinkedList<Storehouse::product_t> products)
{

	game->SignalSwitch(true);

	storehouse_model->clear();
	products_count.resize(products.size());
	QStandardItem* item0 = new QStandardItem(true);
	item0->setText("Продукт"); // вставляем текст
	storehouse_model->setHorizontalHeaderItem(0, item0);


	QStandardItem* item1 = new QStandardItem(true);
	item1->setText("Стоимость"); // вставляем текст
	storehouse_model->setHorizontalHeaderItem(1,item1);

	QStandardItem* item2 = new QStandardItem(true);
	item2->setText("Есть на складе"); // вставляем текст
	storehouse_model->setHorizontalHeaderItem(2,item2);

	QStandardItem* item3 = new QStandardItem(true);
	item3->setText("Продать"); // вставляем текст
	storehouse_model->setHorizontalHeaderItem(3,item3);

	QLinkedList<Storehouse::product_t>::iterator k;
	int i = 0;
	for (k = products.begin(); k != products.end(); ++k){

		QString test= k->name;
		storehouse_model->setItem(i, 0, new QStandardItem(test));
		storehouse_model->setItem(i, 1, new QStandardItem(QString::number(k->price)));
		storehouse_model->setItem(i, 2, new QStandardItem(QString::number(k->size)));
storehouse_model->setItem(i, 3, new QStandardItem(QString::number(k->size)));


		QStandardItem* item = new QStandardItem(true);
		item->setCheckable(true);
		if(k->size < 50 ){
			item->setText(QString::number(k->size));
		} else {
			item->setText("50");
		}
		storehouse_model->setItem(i, 3, item);



		storehouse_model->item(i,0)->setFlags(storehouse_model->item(i,0)->flags() &= ~Qt::ItemIsEditable);
		storehouse_model->item(i,1)->setFlags(storehouse_model->item(i,1)->flags() &= ~Qt::ItemIsEditable);
		storehouse_model->item(i,2)->setFlags(storehouse_model->item(i,2)->flags() &= ~Qt::ItemIsEditable);
storehouse_model->item(i,3)->setFlags(storehouse_model->item(i,3)->flags() &= ~Qt::ItemIsEditable);

		i++;
	}




	// set model
	ui->tableView_2->setModel(storehouse_model);
	ui->tableView_2->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
	ui->tableView_2->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
	ui->tableView_2->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);

game->SignalSwitch(false);

}


void MainWindow::FillEquip(QVector<Game::equip_price_t> equip)
{

	QStandardItem* item0 = new QStandardItem(true);
	item0->setText("оборудование"); // вставляем текст
	tableModel->setHorizontalHeaderItem(0,item0);

	QStandardItem* item1 = new QStandardItem(true);
	item1->setText("Добавляет к складу"); // вставляем текст
	tableModel->setHorizontalHeaderItem(1,item1);

	QStandardItem* item2 = new QStandardItem(true);
	item2->setText("Покупка"); // вставляем текст
	tableModel->setHorizontalHeaderItem(2,item2);


	equip_installed.resize(equip.size() -1);
	equip_tp_install.resize(equip.size() -1);



	for(int i = 0; i < equip.size() -1 ; i++){
		equip_installed[i] = false;
		equip_tp_install[i] = false;
		tableModel->setItem(i, 0, new QStandardItem(equip[i].name));
		tableModel->setItem(i, 1, new QStandardItem(QString::number(equip[i].buff)));
		tableModel->item(i,0)->setFlags(tableModel->item(i,0)->flags() &= ~Qt::ItemIsEditable);
		tableModel->item(i,1)->setFlags(tableModel->item(i,1)->flags() &= ~Qt::ItemIsEditable);


		QStandardItem* item = new QStandardItem(true);
		item->setCheckable(true);
		item->setText(QString::number(equip[i].price));
		tableModel->setItem(i, 2, item);
			tableModel->item(i,2)->setFlags(tableModel->item(i,2)->flags() &= ~Qt::ItemIsEditable);


	}



	// set model
	ui->tableView->setModel(tableModel);
	ui->tableView->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
	ui->tableView->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
	ui->tableView->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);



}
MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::SellProductSelect(QStandardItem *item)
{
	if(block_signals == true)
		return;


	int tmp =  item->text().toInt();
	int tmp2 = storehouse_model->item(item->row(), 1)->text().toInt();
	if(item->checkState() == false){
		tmp = -1 * tmp;
		tmp2 = -1 * tmp2;
		products_count[item->row()] = false;

	} else {
		products_count[item->row()] = true;
	}
	update();

}

void MainWindow::UpdatePrice(QStandardItem *item)
{

	int tmp =  item->text().toInt();
	int tmp2 = tableModel->item(item->row(), 1)->text().toInt();
	if(item->checkState() == false){
		tmp = -1 * tmp;
		tmp2 = -1 * tmp2;
		equip_tp_install[item->row()] = false;

	} else {
		if(equip_installed[item->row()] == false)
			equip_tp_install[item->row()] = true;
	}


	qDebug() << item->row();
	to_buff += tmp2;
	to_inst_equip += tmp;
	ui->equip_inst_label->setText(QString::number(to_inst_equip));
	int money = ui->money_label->text().toInt();
	if(to_inst_equip > money )
		ui->BuyButton->setEnabled(false);
	else{
		ui->BuyButton->setEnabled(true);
	}
	ui->equip_inst_label->setText(QString::number(to_inst_equip));
	update();

}
void MainWindow::SellProducts()
{

	for(int i = 0; i < products_count.size(); i++)
	{
		if(products_count[i] == true)
		{
			QString name = storehouse_model->item(i, 0)->text();
			int amount = storehouse_model->item(i, 3)->text().toInt();

			game->SellSomething(name, amount);

		}
		products_count[i] = false;
	}
	game->SignalSwitch(true);
	game->ProductsShow();
	game->SignalSwitch(false);
	update();
}


void MainWindow::Blank()
{
	for(int i = 0; i < equip_installed.size(); i++ ){
		if(equip_installed[i] == true)
			tableModel->item(i,2)->setCheckState(Qt::Checked);
		else
			tableModel->item(i, 2)->setCheckState(Qt::Unchecked);
		equip_tp_install[i] == false;
	}

	for(int i = 0; i < products_count.size(); i++){
		storehouse_model->item(i,3)->setCheckState(Qt::Unchecked);
		products_count[i] = false;
	}


	ui->equip_inst_label->setText("0");
	to_inst_equip = 0;
	to_buff = 0;


}
void MainWindow::UpdateEquip()
{

	emit BuffStorehouse (0);
	for(int i = 0; i < equip_tp_install.size(); i++ ){
		if(equip_tp_install[i] == true){
			emit BuffStorehouse (tableModel->item(i,1)->text().toInt());
			tableModel->item(i,2)->setCheckState(Qt::Checked);
		}else{
			tableModel->item(i, 2)->setCheckState(Qt::Unchecked);
		}
	}
	equip_installed = equip_tp_install;
	emit DecMoney(-1 * to_inst_equip);
	ui->equip_inst_label->setText("0");
	to_inst_equip = 0;
	to_buff = 0;
	game->MoneyShow();
	game->StorehouseSecurityShow();

}
void MainWindow::StartGame()
{
	game = new Game();
	connect(ui->TurnButton, SIGNAL(pressed()), game, SLOT(Turn()));
	connect(game, SIGNAL(FillEquip(QVector<Game::equip_price_t>)), this, SLOT(FillEquip(QVector<Game::equip_price_t>)));

	connect(game, SIGNAL(FillMoney(int)), this, SLOT(FillMoney(int)));
	connect(game, SIGNAL(FillExp(int)), this, SLOT(FillExp(int)));
	connect(game, SIGNAL(FillRespect(int)), this, SLOT(FillRespect(int)));
	connect(game, SIGNAL(FillStrength(int)), this, SLOT(FillStrength(int)));
	connect(game, SIGNAL(FillLuck(int)), this, SLOT(FillLuck(int)));
	connect(game, SIGNAL(FillCharisma(int)), this, SLOT(FillCharisma(int)));
	connect(game, SIGNAL(FillPerception(int)), this, SLOT(FillPerception(int)));
	connect(game, SIGNAL(ActivateBuffButtons()), this, SLOT(BuffButtonsEnable()));
	connect(ui->strengthbutton, SIGNAL(pressed()), this, SLOT(BuffButtonPressed()));
	connect(ui->perceptionbutton, SIGNAL(pressed()), this, SLOT(BuffButtonPressed()));
	connect(ui->charismabutton, SIGNAL(pressed()), this, SLOT(BuffButtonPressed()));
	connect(ui->luckbutton, SIGNAL(pressed()), this, SLOT(BuffButtonPressed()));
	connect(ui->BuyButton, SIGNAL(pressed()), this, SLOT(UpdateEquip()));
	connect(ui->TurnButton, SIGNAL(pressed()), this, SLOT(Blank()));
	connect(this, SIGNAL(DecMoney(int)), game, SLOT(DecMoney(int)));
	connect(this, SIGNAL(BuffStorehouse(int)), game, SLOT(BuffStorehouse(int)));
	connect(game, SIGNAL(FillStoreTable(QLinkedList<Storehouse::product_t>)), this, SLOT(FillStorehouseTable(QLinkedList<Storehouse::product_t>)));
	connect(game, SIGNAL(FillFreepace(int)), this, SLOT(FillFreespace(int)));
	connect(game, SIGNAL(FillSize(int)), this, SLOT(FillSize(int)));
	connect(this,SIGNAL(SetBuffString(QString)), game, SLOT(GetBuffString(QString)) );
	connect(game, SIGNAL(ChangeSignalState(bool)), this, SLOT(ChangeSignalState(bool)) );
	connect(game, SIGNAL(FillSecurity(int)), this, SLOT(FillSecurity(int)));
	connect(ui->sell_button, SIGNAL(pressed()), this, SLOT(SellProducts()));
	connect(ui->unexwin_button, SIGNAL(pressed()), this, SLOT(UnexpectedVictory()));

	game->EquipShow();
	game->ProductsShow();
	game->FreespaceShow();
	game->ExpShow();
	game->StrengthShow();
	game->LuckShow();
	game->CharismaShow();
	game->PerceptionShow();
	game->RespectShow();
	game->MoneyShow();
	game->SizeShow();



	ui->tabWidget->setVisible(true);
	connect(game, SIGNAL(gameover()), this, SLOT(GameOver()));
	connect(tableModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(UpdatePrice(QStandardItem *)));
	connect(storehouse_model, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(SellProductSelect(QStandardItem *)));
	game->SignalSwitch(false);



	update();
}

void MainWindow::GameOver()
{
	this->close();
}
