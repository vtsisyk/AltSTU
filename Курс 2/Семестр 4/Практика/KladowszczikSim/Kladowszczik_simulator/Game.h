#ifndef GAME_H
#define GAME_H
#include <Kladowszczik.h>
#include <Storehouse.h>
#include <Pest.h>
#include <QStringList>
#include <QDateTime>
#include <QObject>
#include <QMessageBox>
#include <QTableWidgetItem>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QStandardItem>
#include <QFileInfoList>
#include <QDir>
class Game : public QObject
{
	 Q_OBJECT
public:
	explicit Game(QObject *parent = 0);
	struct equip_price_t{
		QString name;
		int price;
		int buff;
	};

signals:
	void FillEquip(QVector<Game::equip_price_t>);
	void FillMoney(int money);
	void FillExp(int exp);
	void FillRespect(int respect);
	void FillStrength(int strength);
	void FillPerception(int perception);
	void FillLuck(int luck);
	void FillCharisma(int charisma);
	void ActivateBuffButtons();
	void FillStoreTable(QLinkedList<Storehouse::product_t>);
	void FillFreepace(int);
	void FillSize(int);
	void ChangeSignalState(bool);
	void FillSecurity(int);
	void gameover();




private:

 QString months[12] = { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };
	struct item_price_t{
		QString name;
		int price;
	};

	struct was_destroyed_t
	{
		QString name;
		int size;
	};

	struct monthy{
		QString pest;
		QVector<was_destroyed_t> items_destroyed;
		int salary;
	};

	struct monthy report;
	struct monthy was_taken;

	QVector<QString> pests;
	QVector<item_price_t> produkty;
	QStringList equipment;
	QVector<Pest> pests_ar;

	QVector<equip_price_t> equip;



	int month;
	bool lets_continue = false;
	int base_salary = 3000;
	Kladowszczik* kladowszczik;
	Storehouse* storehouse;
	QString buff;
	QString MonthyResultString = "";
	int whenbuff;
public:

	int SellSomething(QString name, int amount);
	int BuySomething(QString);
	int Raid();
	int NewProduct();
	int SetSalary();
	void ProductsTaken();
	void MonthyResult();
	void EquipShow();
	void MoneyShow();
	void ExpShow();
	void RespectShow();
	void StrengthShow();
	void PerceptionShow();
	void CharismaShow();
	void LuckShow();
	void SizeShow();
	void FreespaceShow();
	void StorehouseSecurityShow();
	void SignalSwitch(bool);
	void ProductsShow(){emit FillStoreTable(storehouse->WhatWeHave());}
	void CheckEquipPrice();
	void AddToMonthlyString(QString str){MonthyResultString += str + "\n";}

public slots:
	void Turn();
	void GetBuffString(QString);
	void DecMoney(int money);
	void BuffStorehouse(int level);
};

#endif // GAME_H
