#include "Storehouse.h"

Storehouse::Storehouse()
{
	size = storage;
	freespace = storage;
	security_lvl = 1;

	/* пусть матрица будет квадратной */
	space_matrix =  new struct shelf* [size];
	for(int i = 0; i < size; i++)
		space_matrix[i] = new struct shelf[size];
	for(int i = 0; i < size; i++)
		for(int j = 0; j < size; j++){
			space_matrix[i][j].freespace = space_matrix[i][j].space = capacity;

		}
}

/* Устанавливаем оборудование */
int Storehouse::InstQuip(QString name)
{
	int i = equipment.indexOf(name);
	if (i != -1)
		return 1;
	else{
		equipment.push_back(name);
		return 0;
	}
	return 0;

}
/*
 * Добавление нового товара на склад
 */
int Storehouse::AddItem(QString name, int size,  \
			int price)
{

	/*
	 * найти похожий элемент, если есть - то плюсать к нему
	 * если нет - то сделать новый
	 */
	static int i,j;

	struct item tmp2;
	tmp2.space_taken = 0;
	QLinkedList<product_t>::iterator k;
	for (k = store.begin(); k != store.end(); ++k){
		if(name ==  k->name){
			if(this->freespace > size){
				int freespace;
				k->size += size;
				this->freespace -= size;
				while(size > 0){
					/* найти место под товар. Принимаем только
					 * полную партию, как в жизни
					 */
					freespace = FindPlace(&i,&j);

					tmp2.pos = qMakePair(i,j);
					if(size < freespace){
						space_matrix[i][j].freespace -= size;
						tmp2.space_taken +=size;
						size = 0;


					} else{
						space_matrix[i][j].freespace = 0;
						size -= freespace;
						tmp2.space_taken +=freespace ;
					}
					k->place.append(tmp2);
					/* Нашли, распихиваем по ячейкам */
				}
				return 0;
			} else {
				/* не нашли, посылаем сообщение, что нельзя */
				return 1;
			}

		}
	}

	/* добавляем абсолютно новый товар */
	if(this->freespace > size){
		struct product_t tmp;

		tmp.name = name;
		tmp.size = size;
		tmp.price = price;
		this->freespace -=size;
		while(size > 0){
			int freespace;
			freespace = FindPlace(&i,&j);
			tmp2.pos = qMakePair(i,j);
			if(size < freespace){
				space_matrix[i][j].freespace -= size;
				tmp2.space_taken = size;
				size = 0;

			} else{
				space_matrix[i][j].freespace = 0;
				size -= freespace;
				tmp2.space_taken =freespace ;
			}

			tmp.place.append(tmp2);
			/* Нашли, распихиваем по ячейкам */
		}

		store.append(tmp);

	} else {
		return 1;
	}
	return 0;
}

/*
 * Удаление элемента
 */
int Storehouse::RemoveItem(QString name, int size )
{
	bool found = 0;
	int money = 0;
	int h = 0;
	/* ищем товар */
	QLinkedList<product_t>::iterator k;
	for (k = store.begin(); k != store.end(); ++k, h++){
		if(k->name == name){
			found = 1;
			break;
		}
	}
	/* если нет, то плохо */
	if(!found)
		return 0;
	/* если сколько надо и больше, то хорошо */
	else if(k->size >= size){
		money = k->price * size;
		k->size -= size;
		this->freespace +=size;
		while(size >0){
			int i = k->place[0].pos.first;
			int j = k->place[0].pos.second;

			if(capacity > size){

				space_matrix[i][j].freespace += size;
				k->place[0].space_taken -= size;
				size = 0;

			} else {
				space_matrix[i][j].freespace = capacity;
				size -= k->place[0].space_taken;
				k->place.remove(0);

			}

		}
		if(k->size == 0)
			store.erase(k);
	/* если мало, то плохо */
	} else {
		return 0;
	}
	/* возвращаем количество денег, которые можно
	 * получить за этот товар
	 */
	return money;

}

bool Storehouse::HaveProduct(QString name)
{
	QLinkedList<product_t>::iterator k;
	for (k = store.begin(); k != store.end(); ++k){
		if(name ==  k->name)
			return true;
	}
	return false;

}


QString Storehouse::ProductAt(int pos)
{
	QLinkedList<product_t>::iterator k;
	int i = 0;
	for (k = store.begin(); k != store.end(); ++k){
		if(i ==  pos){
			return k->name;
		}
		i++;
	}
	return 0;

}
int Storehouse::HowMuch(QString name)
{
	QLinkedList<product_t>::iterator k;
	for (k = store.begin(); k != store.end(); ++k){
		if(name ==  k->name){
			return k->size;
		}
	}
	return 0;

}
/*
 *  Ищем свободное место. Сначала пытаемся ложить на свободные полки
 */
int Storehouse::FindPlace(int *q, int *w)
{
	int i = 0;
	int j = 0;

	int maxfree = 0;
	for(i = 0; i < size; ++(i))
		for(j = 0; j < size; ++(j)){
			if(maxfree < space_matrix[i][j].freespace)
				maxfree = space_matrix[i][j].freespace;
			if(maxfree == capacity){
				*q = i;
				*w = j;
				return maxfree;
			}
		}
	if(maxfree == 0)
		return 0;
	return maxfree;
}
