#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>
#include <vector>

const int M = 10;
const int N = 10;
bool compare(Parabola *a, Parabola *b);
QTableWidgetItem *table; /* костыли */
QListWidgetItem *list;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::concreate(){


    std::vector<Parabola *> v;
    QString txt;
    v.clear();

    int tmp = 2;
    ui->listWidget->clear();
    ui->tableWidget->setRowCount(N);
    ui->tableWidget->setColumnCount(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
             QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(tmp));
             ui->tableWidget->setItem(i,j, newItem);

        }
        tmp = tmp * 2;
    }
    for(int i = 0; i < M; i++){
        Parabola *par = new Parabola(i, (i*2)%N ,(tmp+i)%M); /* M и N тут просто ради заполнения */
        v.push_back(par);
        txt = par->downe();
        QListWidgetItem *newItem = new QListWidgetItem(txt);
        ui->listWidget->addItem(newItem);

    }

}

void MainWindow::redaktiren(QTableWidgetItem *x){
    table = x;
    ui->lineEdit->setText( x->text());
}
void MainWindow::redaktiren(QListWidgetItem *x){
    list = x;
    ui->lineEdit_2->setText(x->text());
}
void MainWindow::redaktiren(QString x){
    ui->lineEdit_3->setText( x);
}

void MainWindow::tendredaktiren(){
    if(!table) /* без этого крашится */
        return;
    table->setText(ui->lineEdit->text());
    /*тут надо сделать изменения в массиве, но как это сделать - я не знам */
}

void MainWindow::lendredaktiren(){
    if(!list) /* без этого крашится */
        return;
    list->setText(ui->lineEdit_2->text());
    /*тут надо сделать изменения в массиве, но как это сделать - я не знам */
}

void MainWindow::cendredaktiren(){
    ui->comboBox->setItemText(ui->comboBox->currentIndex(), ui->lineEdit_3->text());
}

void MainWindow::sort(){
       QVector<Parabola *> m;
       for(int i = 0; i < M; i++)
           m.append((new Parabola(ui->listWidget->item(i)->text())));
       std::sort(m.begin(), m.end(), compare);
       for(int i = 0; i < M; i++)
           ui->comboBox->addItem(m[i]->downe());



}

bool compare( Parabola *a,  Parabola *b){
      return a->Distance() < b->Distance();
}


void MainWindow::save(){
    QString filename = QFileDialog::getSaveFileName(this,
                tr("Save as"),QDir::currentPath(),
                tr("Document files (*.txt *.bin);;All files (*.*)"));
    if(filename.isNull()){
        return;
    }
    if(filename.endsWith(".txt")){
        QFile file(filename);
        if (file.open(QIODevice::WriteOnly))
        {
            QTextStream stream(&file);
            for(int i = 0;i<N;++i){
                for(int j =0; j<N;++j){
                    stream<<ui->tableWidget->item(i,j)->text()<<" ";
                }
                stream<<"\n";
            }
            file.close();
            if (stream.status() != QTextStream::Ok)
            {
                qDebug() << "Ошибка записи файла";
            }
        }
    }
    else if(filename.endsWith(".bin")){
        QFile file(filename);
        if (file.open(QIODevice::WriteOnly)){
            QDataStream out(&file);
            out.setVersion(QDataStream::Qt_4_8);
            QVector<Parabola> m;
            for(int i = 0;i<M;++i){
                m.append(*(new Parabola(ui->listWidget->item(i)->text())));
//				out<<ui->listWidget->item(i)->text();
            }
            out<<m;
            file.flush();
            file.close();
            if (out.status() != QDataStream::Ok)
            {
                qDebug() << "Ошибка записи файла";
            }
        }
    }
}

void MainWindow::load(){
    QString filename = QFileDialog::getOpenFileName(this,
                tr("Open as"),QDir::currentPath(),
                tr("Document files (*.txt *.bin);;All files (*.*)"));
    if(filename.isNull()){
        return;
    }
    if(filename.endsWith(".txt")){
        QFile file(filename);
        if (file.open(QIODevice::ReadOnly))
        {
            QTextStream stream(&file);
            for(int i = 0;i<N;++i){
                for(int j =0; j<N;++j){
                    int buffer=0;
                    stream>>buffer;
                    ui->tableWidget->setItem(i,j,new QTableWidgetItem(QString::number(buffer)));
                }
            }
            file.close();
            if (stream.status() != QTextStream::Ok)
            {
                qDebug() << "Ошибка чтения файла";
            }
        }
    }
    else if(filename.endsWith(".bin")){
        QFile file(filename);
        if (file.open(QIODevice::ReadOnly)){
            ui->listWidget->clear();
            QDataStream out(&file);
            QVector<Parabola > m;
            out>>m;

            for(int i = 0;i<M;++i){
                ui->listWidget->addItem(m[i].downe());
//				QString a;
//				out >>a;
//				ui->listWidget->addItem(a);
            }
            file.close();
            if (out.status() != QDataStream::Ok)
            {
                qDebug() << "Ошибка чтения файла";
            }
        }
    }
}


Parabola::Parabola(double _a, double _b, double _c)
{
    a = _a;
    b = _b;
    c = _c;
}

double Parabola::Distance()
{
    double x;
    double y;
    x = -b/(2*a);
    y = (b*b - 4*a*c)/(4*a);
    return sqrt(x*x + y*y);
}

void Parabola::matrix(int **array,int x, int n)
{
    int tmp = x;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++)
            array[i][j] = tmp;
        tmp = tmp * x;
    }


}

QDataStream &operator <<(QDataStream &out, const Parabola& a)
{
    out << a.a;
    out << a.b;
    out << a.c;
    return out;
}
QDataStream &operator >>(QDataStream &out, Parabola& a){
    out >> a.a;
    out >> a.b;
    out >> a.c;
    return out;
}

Parabola::Parabola()
{
    a = 4;
    b = 2;
    c = 1;
}

Parabola::Parabola(QString str){

    QStringList list1 = str.split(" ");
    a = list1.at(0).toDouble();
    b = list1.at(1).toDouble();
    c = list1.at(2).toDouble();

}

QString Parabola::downe(){

    return  QString::number(a,'f', 2) + " "  + QString::number(b,'f',2) + " " + QString::number(c,'f',2);
}




