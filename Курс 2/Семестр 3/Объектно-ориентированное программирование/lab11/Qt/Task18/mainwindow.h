#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QTableWidgetItem>
#include <QFileDialog>
#include <QtCore>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void concreate();
    void redaktiren(QTableWidgetItem *x);
    void redaktiren(QListWidgetItem *x);
    void redaktiren(QString x);
    void tendredaktiren();
    void lendredaktiren();
    void cendredaktiren();
    void sort();
    void save();
    void load();


private:
    Ui::MainWindow *ui;
};

class Parabola{
    public:
        Parabola(double _a, double _b, double _c);
        Parabola();
        Parabola(QString);
        QString downe();
        static void matrix(int **array,int x, int n);
        double Distance();
        friend QDataStream &operator <<(QDataStream &out, const Parabola& a);
        friend QDataStream &operator >>(QDataStream &out, Parabola& a);

    private:
        double a;
        double b;
        double c;



};
#endif // MAINWINDOW_H
