#include "scene.h"
void Scene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent){
	if (mouseEvent->button() != Qt::LeftButton) return;
	begin = mouseEvent->scenePos().toPoint();
	clicked = true;
}
void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent){
	if(clicked){
		end = mouseEvent->scenePos().toPoint();
		clicked = false;
		emit Get(begin,end);
	}
}
Scene::Scene():QGraphicsScene(){
	magic = false;
	clicked = 0;
}
