#include "mainwindow.h"
#include "ui_mainwindow.h"
QImage image("../OOP_13/cat.jpg");
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	Work();
	connect(scene,SIGNAL(Get(QPoint,QPoint)),this,SLOT(Open(QPoint, QPoint)));
}
void MainWindow::Work(){
	scene_end = new Scene();
	scene = new Scene();
	scene_one = new Scene();
	scene_two = new Scene();
	scene->addPixmap(QPixmap::fromImage(image));
	ui->graphicsView->setScene(scene);
	ui->end_graph->setScene(scene_end);
	ui->lineEdit->setText("2");
}
MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::Open(QPoint a, QPoint b){
	double k = fabs(ui->lineEdit->text().toDouble());
	QPoint base;
	QImage *end_image = new QImage(abs(a.x()-b.x()),abs(a.y()-b.y()),QImage::Format_RGB16);
	QPoint z(abs(b.x()-a.x()),abs(b.y()-a.y()));
	if(a.x()<b.x()){
		if(a.y()<b.y()) base=a;
		else{
			base.setX(a.x());
			base.setY(b.y());
		}
	}
	else{
		if(a.y()<b.y()){
			base.setX(b.x());
			base.setY(a.y());
		}
		else base=b;
	}
	for(int i = 0;i<z.x();i++){
		for(int j = 0;j<z.y();j++){
			QColor alpha= image.pixel(base.x()+i,base.y()+j);
			end_image->setPixel(i,j,alpha.rgb());
		}
	}
	scene_end->clear();
	scene_end->addPixmap(QPixmap::fromImage(*end_image));
	ui->end_graph->setScene(scene_end);
	QImage *alt = new QImage(abs(a.x()-b.x())*k,abs(a.y()-b.y())*k,QImage::Format_RGB16);
	for(int i = 0;i<z.x()*k;i++){
		for(int j = 0;j<z.y()*k;j++){
			QColor alpha= image.pixel(base.x()+i*(1/k),base.y()+j*(1/k));
			alt->setPixel(i,j,alpha.rgb());
		}
	}
	scene_two->clear();
	scene_two->addPixmap(QPixmap::fromImage(*alt));
	ui->view2->setScene(scene_two);
	*alt = end_image->scaled(abs(a.x()-b.x())*k,abs(a.y()-b.y())*k,Qt::KeepAspectRatioByExpanding,Qt::SmoothTransformation);
	scene_one->clear();
	scene_one->addPixmap(QPixmap::fromImage(*alt));
	ui->view1->setScene(scene_one);
}
