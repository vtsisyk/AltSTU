#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "scene.h"
#include <QMainWindow>
#include <QImage>
#include <QGraphicsPixmapItem>

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
public slots:
	void Open(QPoint a, QPoint b);
public:
	Scene *scene,*scene_end,*scene_one,*scene_two;
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	void Work();
private:
	Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
