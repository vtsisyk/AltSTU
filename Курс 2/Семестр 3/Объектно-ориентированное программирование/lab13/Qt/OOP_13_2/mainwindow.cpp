#include "mainwindow.h"
#include "ui_mainwindow.h"

QImage *image,*image_new;

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	Work();
}
void MainWindow::Work(){
	scene_end = new Scene();
	scene = new Scene();
	ui->graphicsView->setScene(scene);
	ui->end_graph->setScene(scene_end);
}
MainWindow::~MainWindow()
{
	delete ui;
}
void MainWindow::one(){
    image =  new QImage("../OOP_13_2/cat.jpg");
	image_new =  new QImage(image->size(),QImage::Format_RGB16);
	scene->clear();
	scene->addPixmap(QPixmap::fromImage(*image));
}
void MainWindow::two(){
    image =  new QImage("../OOP_13_2/cat2.jpg");
	image_new =  new QImage(image->size(),QImage::Format_Grayscale8);
	scene->clear();
	scene->addPixmap(QPixmap::fromImage(*image));
}
void MainWindow::comunism(){
  int sx = 0;
  int sy = 0;
  int ex = image->size().width();
  int ey = image->size().height();
  int rh[256],gh[256],bh[256],rl[256],gl[256],bl[256],rr[256],gr[256],br[256];
  for(int i = 0;i<256;i++){
	  rh[i]=0;
	  gh[i]=0;
	  bh[i]=0;
	  rl[i]=0;
	  gl[i]=0;
	  bl[i]=0;
	  rr[i]=0;
	  gr[i]=0;
	  br[i]=0;
  }

  for(int i = sx; i < ex; ++i){
		for(int j = sy; j < ey; ++j) {
			 QColor *c = new QColor(image->pixel(i,j));
			 rh[c->red()] += 1;
			 gh[c->green()]+=1;
			 bh[c->blue()]+=1;
		}
  }
  int Hm = (ex*ey)/255;

  int Hsr=0;
  int Rr=0;

  int Hsg=0;
  int Rg=0;
  int Hsb=0;
  int Rb=0;

  for (int i = 0; i < 256; i++)
  {
		rl[i] = Rr;
		Hsr = Hsr+ rh[i];
		while (Hsr > Hm)
		{
			 Hsr = Hsr-Hm;
			 Rr++;
		}
		rr[i] = Rr;
		gl[i] = Rg;
		Hsg = Hsg+ gh[i];
		while (Hsg > Hm)
		{
			 Hsg = Hsg-Hm;
			 Rg++;
		}
		gr[i] = Rg;
		bl[i] = Rb;
		Hsb = Hsb+ bh[i];
		while (Hsb > Hm)
		{
			 Hsb = Hsb-Hm;
			 Rb++;
		}
		br[i] = Rb;
  }
  for(int i = sx; i < ex; ++i){
	  for(int j = sy; j < ey; ++j) {
			QColor *c = new QColor(image->pixel(i,j));
			int r = (rl[c->red()]+rr[c->red()])/2;
			int g = (gl[c->green()]+gr[c->green()])/2;
			int b = (bl[c->blue()]+br[c->blue()])/2;
			if(r > 255){
				r = 255;
			}
			if(g > 255){
				g = 255;
			}
			if(b > 255){
				b = 255;
			}
			image_new->setPixel(i,j, (new QColor(r,g,b))->rgb());
			delete c;
		}
  }
  scene_end->addPixmap(QPixmap::fromImage(*image_new));
}
