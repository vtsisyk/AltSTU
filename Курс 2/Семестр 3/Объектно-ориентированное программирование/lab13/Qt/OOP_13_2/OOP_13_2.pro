#-------------------------------------------------
#
# Project created by QtCreator 2016-01-01T20:49:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OOP_13_2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    scene.cpp

HEADERS  += mainwindow.h \
    scene.h

FORMS    += mainwindow.ui
