#ifndef SCENE_H
#define SCENE_H
#include <QMouseEvent>
#include <QtCore>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>

class Scene : public QGraphicsScene{
	Q_OBJECT
protected:
	bool magic;
	QPoint begin,end;
	bool clicked;
public:
	void set(){	magic = true; }
	explicit Scene();
	void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);
signals:
	void Get(QPoint a, QPoint b);
};

#endif // SCENE_H
