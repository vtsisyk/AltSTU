#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QColorDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    pos1 = 60;
    pos2 = 240;
    pos3= 50;
    end = 0;
    timer = new QTimer();
    ui->setupUi(this);
    scene = new QGraphicsScene();
    color_1 = QColorDialog::getColor(Qt::white, this );
    color_2 = QColorDialog::getColor(Qt::white, this );
    color_3 = QColorDialog::getColor(Qt::white, this ) ;

    paint();
    ui->graphicsView->setScene(scene);
    connect(timer, SIGNAL(timeout()), this, SLOT(start()));

}


void MainWindow::paint()
{
   scene->clear();
   QPixmap m("../OOP_13_3/dniwe.jpg");
   scene->addPixmap(m);
   QBrush *def = new QBrush(Qt::white);
   QBrush *tail = new QBrush(color_2);
   QBrush *glaz = new QBrush(color_1);
   QPen *pen = new QPen(color_3 ) ;

   QPolygon plawnikwerch;
   plawnikwerch << QPoint(pos3+30,60);
   plawnikwerch << QPoint(pos3+100,110);
   plawnikwerch << QPoint(pos3+160,60);


   QPolygon plawnikniz;
   plawnikniz << QPoint(pos3+40,230);
   plawnikniz << QPoint(pos3+110,180);
   plawnikniz << QPoint(pos3+180,230);

   QPolygon hwost;
   hwost << QPoint(pos3+280,pos1);
   hwost << QPoint(pos3+180,150);
   hwost << QPoint(pos3+280,pos2);

   pen->setWidth(6);
   scene->addPolygon(plawnikniz,*pen,*def);
   scene->addPolygon(plawnikwerch,*pen,*def);
   scene->addPolygon(hwost,*pen, *tail);
   scene->addEllipse(pos3,100,200,100, *pen,*def);
   scene->addEllipse(pos3+30,120,20,20, *pen,*glaz );



}


void MainWindow::start(){
    if(end == 8)
        timer->stop();
    else{
            if(up){
                pos3--;
                pos2++;
                pos1--;
                paint();
            }else{
                pos2--;
                pos1++;
                paint();
            }
            if(pos1 == 80 || pos1 ==50){
                end++;
                up = !up;

            }

    }

}
void MainWindow::push(){

        timer->start(ui->lineEdit->text().toInt());
        end = 0;
        up = false;

}
MainWindow::~MainWindow()
{
    delete ui;
}
