#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QtGui>
#include <QTimer>

//#include <QtCore>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QGraphicsScene *scene;
    QColor color_1, color_2,color_3;
    int pos1,pos2,end;
    int pos3;
    QTimer *timer;
    bool up;
    void paint();
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Work();

public slots:
    void start();
    void push();
private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
