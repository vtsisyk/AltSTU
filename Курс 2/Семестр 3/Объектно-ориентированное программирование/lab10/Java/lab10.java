import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
public class lab10 
{
	int Dollars;
	double Kurs;
	double Rubles;
	JFrame form; 
	JLabel lb; 
	JLabel lb2; 
	JLabel lb3; 
	JLabel lb4; 
	JTextField tf1;
	JTextField tf2;
	JButton bt;
	JButton bt2;
	JRadioButton option1; 
	JRadioButton option2; 
	ButtonGroup group; 
	lab10()
	{
	/* главное окно */
	form = new JFrame();
	form.setBounds (10,20,370,210);
	form.setTitle ("Лабораторная 10");  
	form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	form.setLayout(null);
	
	/* */
	lb = new JLabel();
	lb.setBounds(10,10,400,40); 
	lb.setText("Введите ");
	form.add(lb);
	
	/* ввод числа */	  
	tf1 = new JTextField();
	tf1.setBounds(10,50,150,20);
	tf1.setText("");
	form.add(tf1);
/*	tf1.addActionListener(new ActionListener() {
		public void focusLost(ActionEvent e){
			Calculate();
		}
	});
*/
	/* */
	lb2 = new JLabel();
	lb2.setBounds(200,10,400,40); 
	lb2.setText("Точность");
	form.add(lb2);
	
	/* ввод точности */
	tf2= new JTextField();
	tf2.setBounds(200,50,150,20);
	tf2.setText("");
	form.add(tf2);
	
	group = new ButtonGroup();
	
	option1 = new JRadioButton("По точности e");
	option1.setBounds(10,70,150,40); 
	group.add(option1);
	form.add(option1);	
	option1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e){
			Calculate();
		}
	});

	option2 = new JRadioButton("По слагаемым");
	option2.setBounds(200,70,140,40); 
	group.add(option2);
	form.add(option2);	
	option2.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e){
			Calculate();
		}
	});
	lb3 = new JLabel();
	lb3.setBounds(10,100,400,40); 
	lb3.setText("Вывод:");
	form.add(lb3);
	
	lb4 = new JLabel();
	lb4.setBounds(80,100,400,40); 
	lb4.setText("");
	form.add(lb4);

	/* вычисление */
	bt=new JButton();
	bt.setBounds(10,150,140,20);
	bt.setLabel("Вычислить");
	form.add(bt);
	bt.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e){
			Calculate();
		}
		});
	
	form.show();

	bt2=new JButton();
	bt2.setBounds(200,150,140,20);
	bt2.setLabel("Выход");
	form.add(bt2);
	bt2.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e){
			System.exit(0);
		}
		});
	}
	public void Calculate(){
	
	 double num;
        double precise;
        double sum = 0;
        double tmp;
        boolean prec = option1.isSelected();
        boolean su = option2.isSelected();
        String s1;
        if(tf1.getText().isEmpty()) {
         //   Toast.makeText(this, "Нет числа", Toast.LENGTH_SHORT).show();
            return;
        }
        s1 =  tf1.getText().toString();
        num = Double.parseDouble(s1);
        if(tf2.getText().isEmpty()) {
            //Toast.makeText(this, "Нет точности или количества слагаемых", Toast.LENGTH_SHORT).show();
            return;
        }
        s1 = tf2.getText().toString();
        precise = Double.parseDouble(s1);
        if(prec) {
            for (double i = 2; ; i++) {
                tmp = Math.pow(-1.0, i) * (Math.pow(num, 2.0 * (i - 1.0)) / (Math.pow(2.0, 2.0 * (i - 1.0)) * factorial(i) * factorial(i - 2.0)));
                if (Math.abs(tmp) < precise)
                    break;
                else
                    sum += tmp;
            }
        }
        if(su) {
            for(double i = 2; i < precise; i++){
                tmp = Math.pow(-1.0, i) * (Math.pow(num, 2.0 * (i - 1.0)) / (Math.pow(2.0, 2.0 * (i - 1.0)) * factorial(i) * factorial(i - 2.0)));
                sum +=tmp;
            }
        }
        s1 = Double.toString(sum);
		System.out.println(s1);
        lb4.setText(s1);
/*String s1;  
	int k;
	s1=tf1.getText(); // введенная строка курса
	Kurs = Double.parseDouble(s1); // перевод в вещественное число
	s1=tf2.getText();    // введенная строка количества долларов
	Dollars=Integer.parseInt(s1);  // перевод в целое число 
	  Rubles=Kurs*Dollars;
	  k=(int)Math.round(Rubles*100); // округление
	  Rubles=k/100.0;          // до копеек
	  s1=Double.toString(Rubles);  // перевод вещественного числа в строку
	  s1="Ответ: "+s1;
	  lb.setText(s1);  // вывод в label
*/
	}
  public  double factorial(double x){

        if (x < 0)
            return 0.0;
        if(x == 0.0)
            return 1.0;
        else
            return x * factorial(x-1) ;
    }
	

	 public static void main (String args[]) {
		lab10 f;
		f= new lab10();
	}
}
