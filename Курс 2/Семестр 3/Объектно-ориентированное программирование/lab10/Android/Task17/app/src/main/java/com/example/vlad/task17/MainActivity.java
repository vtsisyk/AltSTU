package com.example.vlad.task17;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.text.InputType;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;
import android.widget.RadioButton;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import java.lang.Math;

public class MainActivity extends AppCompatActivity {
    private EditText input;
    private EditText precision;
    private EditText output;
    private RadioButton precE;
    private RadioButton precSum;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input=(EditText)findViewById(R.id.input);
        precision=(EditText)findViewById(R.id.precision);
        output=(EditText)findViewById(R.id.output);
        precE=(RadioButton)findViewById(R.id.precE);
        precSum=(RadioButton)findViewById(R.id.precSum);

        input.setOnFocusChangeListener(new View.OnFocusChangeListener (){
            @Override
            public void onFocusChange(View v, boolean hasFocus){
                if(!hasFocus) {
                    if(precE.isChecked()) {
                        Calculate(R);
                        precSum.setChecked(true);
                    }else {
                        precE.setChecked(true);
                    }
                }
            }
        });

    }


    public void hel(View vie){
        finish();
        //System.exit(0);
    }
    private boolean isEmpty(EditText  etText){
        if(etText.getText().toString().trim().length() > 0)
            return false;
        else
            return true;

    }
    public void Calculate(View vie){
        double num;
        double precise;
        double sum = 0;
        double tmp;
        boolean prec = precE.isChecked();
        boolean su = precSum.isChecked();
        String s1;
        if(isEmpty(input)) {
            Toast.makeText(this, "Нет числа", Toast.LENGTH_SHORT).show();
            return;
        }
        s1 = input.getText().toString();
        num = Double.parseDouble(s1);
        if(isEmpty(precision)) {
            Toast.makeText(this, "Нет точности или количества слагаемых", Toast.LENGTH_SHORT).show();
            return;
        }
        s1 = precision.getText().toString();
        precise = Double.parseDouble(s1);
        if(prec) {
            for (double i = 2; ; i++) {
                tmp = Math.pow(-1.0, i) * (Math.pow(num, 2.0 * (i - 1.0)) / (Math.pow(2.0, 2.0 * (i - 1.0)) * factorial(i) * factorial(i - 2.0)));
                if (Math.abs(tmp) < precise)
                    break;
                else
                    sum += tmp;
            }
        }
        if(su) {
            for(double i = 2; i < precise; i++){
                tmp = Math.pow(-1.0, i) * (Math.pow(num, 2.0 * (i - 1.0)) / (Math.pow(2.0, 2.0 * (i - 1.0)) * factorial(i) * factorial(i - 2.0)));
                sum +=tmp;
            }
        }
        s1 = String.valueOf(sum);
        output.setText(s1);
    }
    public  double factorial(double x){

        if (x < 0)
            return 0.0;
        if(x == 0.0)
            return 1.0;
        else
            return x * factorial(x-1) ;
    }


}
