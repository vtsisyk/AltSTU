﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::Calculate(){
    QString s1;
    QString s2;
    bool precision = ui->precision->isChecked();
    bool num = ui->num->isChecked();
    long long int x;
    double sum = 0;
    double e;
    s2= ui->howbig->text();
    s1 = ui->input->text();
    e = s2.toDouble();
    x = s1.toInt();
    if(s2 == "" || s1 == "" && (precision || num)  || x > 10 || x < -10 ){
        QMessageBox messageBox;
        messageBox.critical(0, QString::fromUtf8("костыляка"), QString::fromUtf8("Не корректные данные"));
        messageBox.setFixedSize(500,200);
        return;

    }

    double tmp;
    double t;

    if(precision){

        for(int i = 2; ; i++){
            tmp = pow(-1, i)*(pow(x,2*(i-1)) / (pow(2,2*(i-1)) * factorial(i) * factorial(i-2)));
            if( std::abs(tmp) < e)
                break;
            else
                sum +=tmp;
        }
    } else if(num){
        for(int i = 2; i < e; i++){
                tmp = pow(-1, i)*(pow(x,2*(i-1)) / (pow(2,2*(i-1)) * factorial(i) * factorial(i-2)));
                sum +=tmp;
            }
    }

    s1 = QString::number(sum,'f', 6);
   ui->output->setText(s1);
}
long long int MainWindow::factorial(int x){

    if (x < 0)
        return 0;
    if(!x)
        return 1;
    else
        return x * factorial(x-1) ;
}

MainWindow::~MainWindow()
{
    delete ui;
}
