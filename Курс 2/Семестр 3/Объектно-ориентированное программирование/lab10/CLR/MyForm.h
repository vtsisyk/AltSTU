#pragma once
#include "praca.h"
namespace Task17_CLR {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
        using namespace Runtime::InteropServices;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
                praca ^wk;
		MyForm(void)
		{
			InitializeComponent();
                        wk = gcnew praca();
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
        private: System::Windows::Forms::Button^  button1;

        private: System::Windows::Forms::Label^  label1;
        private: System::Windows::Forms::Label^  label2;
        private: System::Windows::Forms::TextBox^  textBox1;
        private: System::Windows::Forms::TextBox^  textBox2;
        private: System::Windows::Forms::TextBox^  textBox3;
        private: System::Windows::Forms::Label^  label3;
        private: System::Windows::Forms::RadioButton^  radioButton1;
        private: System::Windows::Forms::RadioButton^  radioButton2;
        private: System::Windows::Forms::Button^  button2;
        protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
                        this->button1 = (gcnew System::Windows::Forms::Button());
                        this->button2 = (gcnew System::Windows::Forms::Button());
                        this->label1 = (gcnew System::Windows::Forms::Label());
                        this->label2 = (gcnew System::Windows::Forms::Label());
                        this->textBox1 = (gcnew System::Windows::Forms::TextBox());
                        this->textBox2 = (gcnew System::Windows::Forms::TextBox());
                        this->textBox3 = (gcnew System::Windows::Forms::TextBox());
                        this->label3 = (gcnew System::Windows::Forms::Label());
                        this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
                        this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
                        this->SuspendLayout();
                        // 
                        // button1
                        // 
                        this->button1->Location = System::Drawing::Point(177, 207);
                        this->button1->Name = L"button1";
                        this->button1->Size = System::Drawing::Size(75, 23);
                        this->button1->TabIndex = 0;
                        this->button1->Text = L"�������";
                        this->button1->UseVisualStyleBackColor = true;
                        this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
                        // 
                        // button2
                        // 
                        this->button2->Location = System::Drawing::Point(265, 207);
                        this->button2->Name = L"button2";
                        this->button2->Size = System::Drawing::Size(75, 23);
                        this->button2->TabIndex = 1;
                        this->button2->Text = L"�����";
                        this->button2->UseVisualStyleBackColor = true;
                        this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
                        // 
                        // label1
                        // 
                        this->label1->AutoSize = true;
                        this->label1->Location = System::Drawing::Point(12, 44);
                        this->label1->Name = L"label1";
                        this->label1->Size = System::Drawing::Size(63, 17);
                        this->label1->TabIndex = 2;
                        this->label1->Text = L"�������";
                        this->label1->Click += gcnew System::EventHandler(this, &MyForm::label1_Click);
                        // 
                        // label2
                        // 
                        this->label2->AutoSize = true;
                        this->label2->Location = System::Drawing::Point(174, 44);
                        this->label2->Name = L"label2";
                        this->label2->Size = System::Drawing::Size(70, 17);
                        this->label2->TabIndex = 3;
                        this->label2->Text = L"��������";
                        // 
                        // textBox1
                        // 
                        this->textBox1->Location = System::Drawing::Point(177, 64);
                        this->textBox1->Name = L"textBox1";
                        this->textBox1->Size = System::Drawing::Size(163, 22);
                        this->textBox1->TabIndex = 4;
                        // 
                        // textBox2
                        // 
                        this->textBox2->Location = System::Drawing::Point(15, 64);
                        this->textBox2->Name = L"textBox2";
                        this->textBox2->Size = System::Drawing::Size(156, 22);
                        this->textBox2->TabIndex = 5;
                        // 
                        // textBox3
                        // 
                        this->textBox3->Location = System::Drawing::Point(15, 160);
                        this->textBox3->Name = L"textBox3";
                        this->textBox3->Size = System::Drawing::Size(325, 22);
                        this->textBox3->TabIndex = 6;
                        // 
                        // label3
                        // 
                        this->label3->AutoSize = true;
                        this->label3->Location = System::Drawing::Point(12, 140);
                        this->label3->Name = L"label3";
                        this->label3->Size = System::Drawing::Size(50, 17);
                        this->label3->TabIndex = 7;
                        this->label3->Text = L"�����";
                        // 
                        // radioButton1
                        // 
                        this->radioButton1->AutoSize = true;
                        this->radioButton1->Checked = true;
                        this->radioButton1->Location = System::Drawing::Point(15, 106);
                        this->radioButton1->Name = L"radioButton1";
                        this->radioButton1->Size = System::Drawing::Size(124, 21);
                        this->radioButton1->TabIndex = 8;
                        this->radioButton1->TabStop = true;
                        this->radioButton1->Text = L"�� �������� e";
                        this->radioButton1->UseVisualStyleBackColor = true;
                        this->radioButton1->CheckedChanged += gcnew System::EventHandler(this, &MyForm::button1_Click);
                        // 
                        // radioButton2
                        // 
                        this->radioButton2->AutoSize = true;
                        this->radioButton2->Location = System::Drawing::Point(177, 106);
                        this->radioButton2->Name = L"radioButton2";
                        this->radioButton2->Size = System::Drawing::Size(123, 21);
                        this->radioButton2->TabIndex = 9;
                        this->radioButton2->Text = L"�� ���������";
                        this->radioButton2->UseVisualStyleBackColor = true;
                        // 
                        // MyForm
                        // 
                        this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
                        this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
                        this->ClientSize = System::Drawing::Size(352, 243);
                        this->Controls->Add(this->radioButton2);
                        this->Controls->Add(this->radioButton1);
                        this->Controls->Add(this->label3);
                        this->Controls->Add(this->textBox3);
                        this->Controls->Add(this->textBox2);
                        this->Controls->Add(this->textBox1);
                        this->Controls->Add(this->label2);
                        this->Controls->Add(this->label1);
                        this->Controls->Add(this->button2);
                        this->Controls->Add(this->button1);
                        this->Name = L"MyForm";
                        this->Text = L"MyForm";
                        this->ResumeLayout(false);
                        this->PerformLayout();

                }
#pragma endregion
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
        Application::Exit();
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

        wk->TB2 = textBox1->Text;
        wk->TB1 = textBox2->Text;
        wk->Calculate(radioButton1->Checked);  // ���������� ����� ������
        textBox3->Text = wk->L1;  // ����� � label1 ����������

}
};
}
