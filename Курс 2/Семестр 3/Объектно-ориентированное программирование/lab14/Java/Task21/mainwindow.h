#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    bool eventFilter(QObject *object, QEvent *event);
    bool norm(QString str, int n);
    ~MainWindow();
public slots:
    void changeFont();
    void arbeiten();
    void perepil();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
