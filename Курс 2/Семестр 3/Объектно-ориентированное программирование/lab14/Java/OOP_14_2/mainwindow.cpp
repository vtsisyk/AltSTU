#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	ui->lineEdit->setText(QString("лодка плетка волк заяц круасан дигидрофосфат натрия"));
}

MainWindow::~MainWindow()
{
	delete ui;
}
void MainWindow::onclick(){
    ui->listWidget->clear();
    ui->tableWidget->clear();
	QVector<QString> word = ui->lineEdit->text().split(" ").toVector();
	QVector<QPair<int,QString> > data;
     ui->tableWidget->setRowCount(word.length());
     ui->tableWidget->setColumnCount(word.length());
    int i = 0;
	foreach (QString alpha, word) {
        int j = 0;
		foreach (QString beta, word) {
			if(alpha == beta) continue;
            data.append(QPair<int,QString>(editdist(alpha,beta),alpha+" "+beta));
		}
        QTableWidgetItem *test = new QTableWidgetItem();
        test ->setText(alpha);
        ui->tableWidget->setHorizontalHeaderItem(i, test);
        ui->tableWidget->setVerticalHeaderItem(i, test);
        i++;
	}
	qSort(data.begin(),data.end(),[](QPair<int,QString> a, QPair<int,QString> b){
		return a.first<b.first;
	} );
	QPair<int,QString> alpha;
	foreach(alpha , data){
		ui->listWidget->addItem(alpha.second + " - " + QString::number(alpha.first));
	}


     for(int i = 0; i<word.length();i++){
         for(int j = 0; j<word.length(); j++){
             QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(editdist(word[i],word[j])));
             ui->tableWidget->setItem(i,j, newItem); }
     }

}



int MainWindow::editdist(QString S1, QString S2) {
	int m = S1.length(), n = S2.length();
	int *D1;
	int *D2 = new int[n + 1];

	for (int i = 0; i <= n; i++)
		D2[i] = i;

	for (int i = 1; i <= m; i++) {
		D1 = D2;
		D2 = new int[n + 1];
		for (int j = 0; j <= n; j++) {
			 if (j == 0) D2[j] = i;
			 else {
				  int cost = (S1.at(i - 1) != S2.at(j - 1)) ? 1 : 0;
				  if (D2[j - 1] < D1[j] && D2[j - 1] < D1[j - 1] + cost)
						D2[j] = D2[j - 1] + 1;
				  else if (D1[j] < D1[j - 1] + cost)
						D2[j] = D1[j] + 1;
				  else
						D2[j] = D1[j - 1] + cost;
			}
		}
	}
	return D2[n];
}
