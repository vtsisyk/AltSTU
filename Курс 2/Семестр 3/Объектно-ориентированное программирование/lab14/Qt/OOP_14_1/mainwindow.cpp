#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}
void MainWindow::click(){
	QString s1 = ui->lineEdit->text();
	QString s2 = ui->lineEdit_2->text();
	int M = s1.length();
	int N = s2.length();
	int **D = new int*[M + 1];
	for(int i =0; i<M+1;i++){
		D[i] =  new int[N+1];
	}

	D[0][0] = 0;
	for (int j = 1; j <= N; ++j)
		 D[0][j] = D[0][j - 1] + 1;
	for (int i = 1; i <= M; ++i) {
		 D[i][0] = D[i - 1][0] + 1;
		 for (int j = 1; j <= N; ++j)
			  D[i][j] = min(
						 D[i - 1][j] + 1,
						 min(
									D[i][j - 1] + 1,
									D[i - 1][j - 1] + (s1.at(i - 1) == s2.at(j - 1) ? 0 : 1)
						 )
			  );
	}
	int i = M;
	int j = N;
	QVector<QString> strs;
	strs.append(s1);
	QString sb = s1;
	if(N > M){
		 for(int k=0;k<N-M;++k)
			  sb.append(" ");
	}
	while (i != 0 || j != 0) {
		 int gg;
		 int bi;
		 if (D[i - 1][j] + 1 < D[i][j - 1] + 1) {
			  gg = D[i - 1][j] + 1;
			  bi = 0;
		 } else {
			  gg = D[i][j - 1] + 1;
			  bi = 1;
		 }
		 if (gg > D[i - 1][j - 1] + (s1.at(i - 1) == s2.at(j - 1) ? 0 : 1)) {
			  bi = 2;
		 }
		 switch (bi) {
			  case 0:
					sb.replace(i-1,1,'-');
					strs.append(sb);
					i -= 1;
					break;
			  case 1:
					sb.insert(j, s2.at(j - 1));
					strs.append(sb);
					j -= 1;
					break;
			  case 2:
					if (s1.at(i - 1) != s2.at(j - 1)) {
						 sb.replace(i - 1,1, s2.at(j - 1));
						 strs.append(sb);
					}
					i -= 1;
					j -= 1;
					break;
		 }
	}
	ui->listWidget->clear();
	foreach(QString s, strs){
		 ui->listWidget->addItem(s.replace("-","").replace(" ",""));
	}




}


void MainWindow::Latwija(){

    ui->tableWidget->clear();
    QString s = ui->lineEdit->text();
    QString t = ui->lineEdit_2->text();
    QString tmp =s;
    int m = s.length();
    int n = t.length();
    int d[m][n];
    ui->tableWidget->setRowCount(m+10);
    ui->tableWidget->setColumnCount(n+10);
    int state = 0;
    int i,j,temp,tracker;

      for(i=0;i<=m;i++)
          d[0][i] = i;
      for(j=0;j<=n;j++)
          d[j][0] = j;

      for (j=1;j<=m;j++){
          for(i=1;i<=n;i++){
              if(s[i-1] == t[j-1]){
                  tracker = 0;
              } else{
                  tracker = 1;
              }

              temp = min((d[i-1][j]+1),(d[i][j-1]+1));

              d[i][j] = min(temp,(d[i-1][j-1]+tracker));

              QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(d[i][j]));

              ui->tableWidget->setItem(i,j, newItem);
              QTableWidgetItem *ne = new QTableWidgetItem(t.at(i-1));

              ui->tableWidget->setItem(i,0, ne);
          }


          QTableWidgetItem *nee = new QTableWidgetItem(s.at(j-1));
          ui->tableWidget->setItem(0,j, nee);
      }

}

