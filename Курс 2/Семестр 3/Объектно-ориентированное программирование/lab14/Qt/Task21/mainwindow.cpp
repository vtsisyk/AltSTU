#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QKeyEvent>
int a[100];
int q=0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    for(int i =0; i < 100; i++)
        a[i] = 0;
    ui->setupUi(this);
    ui->textEdit->installEventFilter(this);
    ui->textEdit->setText("Psze, улица, фонарь, аптека,\n"
                            "Бессмысленный и тусклый свет.\n"
                            "Живи еще хоть четверть века -\n"
                            "Все будет так. Исхода нет.\n");

}


void MainWindow::perepil(){

    if(a[0] == 0)
        return;
    QString str1 = ui->textEdit->toPlainText();
    str1[a[0]] = ',';
    for(int i = a[0]; i < str1.length(); i++)
        if(str1[i].isLetter() && str1[i] == str1[i].toUpper()){
            str1[i] =str1[i].toLower();
            break;
        }



    ui->textEdit->setText(str1);
    q = 0;
    arbeiten();

}
bool MainWindow::eventFilter(QObject *object, QEvent *event){

    if(object == ui->textEdit && event->type() == QEvent::KeyPress){
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if(keyEvent->key() == Qt::Key_F2){

            perepil();
            return true;
        }else{
            return QMainWindow::eventFilter(object, event);
        }

    }
}
void MainWindow::changeFont(){

      QFont font = ui->fontComboBox->currentFont();
      QString str = ui->textEdit->toPlainText();
      font.setPixelSize(14);
      font.setBold(false);
      ui->textEdit->setText(str);
      ui->textEdit->setFont(font);
}

void MainWindow::arbeiten(){
    for(int i =0; i < 100; i++)
        a[i] = 0;
    q = 0;
    QString str1 = ui->textEdit->toPlainText();
    ui->textEdit->setText(str1);
    int j = 0;
    QString str = ui->textEdit->toPlainText();
    QStringList strList = str.split('.');
    int i = 0;
    while(i < strList.size()){

        if(norm(strList.at(i), ui->lineEdit->text().toInt())){
            for(j; j < str.length()-1; j++)
                if(str[j].toLatin1() == '.')
                    break;

            if(j != str.length()-1){
                a[q]=j;
                q++;
            }
            QFont font = ui->fontComboBox->currentFont();
            font.setBold(true);
            font.setPixelSize(20);
           //ui->textEdit->setFont(font);

            QTextCursor cr(ui->textEdit->textCursor());
            cr.setPosition(j);
            cr.setPosition(j+1, QTextCursor::KeepAnchor);

            ui->textEdit->setTextCursor(cr);
            QTextCharFormat format = cr.charFormat();
            format.setFont(font);
            cr.setCharFormat(format);
            cr.setPosition(0, QTextCursor::MoveAnchor);
            ui->textEdit->setTextCursor(cr);
            j++;
        }
    i++;
    }
    QTextCursor cr(ui->textEdit->textCursor());
    cr.setPosition(a[0]);
    cr.setPosition(a[0], QTextCursor::KeepAnchor);
    ui->textEdit->setTextCursor(cr);


}

bool MainWindow::norm(QString str, int n){

    int wordcount = str.split(QRegExp("(\\s|\\r)+"), QString::SkipEmptyParts).count();
    if(wordcount > n)
        return true;
    else
        return false;
}


MainWindow::~MainWindow()
{
    delete ui;
}
