#include "abstractsport.h"

AbstractSport::AbstractSport()
{
}
string
AbstractSport::getname()
{
    return name;
}

void
AbstractSport::setname(string _name)
{
    name = _name;
}

char
AbstractSport::gettype()
{
    return type;
}

void
AbstractSport::settype(char _type)
{
    type = _type;
}

char
AbstractSport::getmrz()
{
    return mrz;
}

void
AbstractSport::setmrz(char _mrz)
{
    mrz = _mrz;
}
