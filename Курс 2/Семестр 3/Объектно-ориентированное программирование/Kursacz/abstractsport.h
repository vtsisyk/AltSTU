#ifndef ABSTRACTSPORT_H
#define ABSTRACTSPORT_H

#include <string>
#include <vector>
#include "sportsman.h"

using namespace std;

class AbstractSport
{
public:
    AbstractSport();
    //AbstractSport(string _type, string _name);
    virtual void setrecord(Sportsman _meister) = 0;
    virtual Sportsman getmeister() = 0;
    virtual double getrecord() = 0;

    string getname();
    void setname(string _name);

    char gettype();
    void settype(char _type);

    char getmrz();
    void setmrz(char _mrz);

protected:
    string name;       /* название спорта */
    char type;         /* вид спорта */
    char mrz;          /* разделение по полу */
    Sportsman meister; /* рекордсмен */
};

#endif // ABSTRACTSPORT_H
