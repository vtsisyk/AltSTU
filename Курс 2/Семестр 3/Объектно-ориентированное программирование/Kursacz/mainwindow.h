#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma once
#include "QTime"
#include <QMainWindow>
#include "aboutdialog.h"
#include "addsportsman.h"
#include "addolympics.h"
#include "records.h"
#include "olympics.h"
#include <QObject>
#include <QListWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
public slots:
    void showabout();
    void showsport();
    void addolympics();
    void sort_by_country();
    void records_set();
    void doit();
    void sort_by_olympics();
    void country_selected(QListWidgetItem *country);
    void sportsman_selected(QListWidgetItem *sportsman);
    void savedata();
    void loaddata();


private:
    vector<Olympics> Olympiady;
    Ui::MainWindow *ui;
    AboutDialog aboutdialog;
    AddSportsman *sportdialog;
    AddOlympics *olympicsdialog;
    Records *superrecord;
};

#endif // MAINWINDOW_H
