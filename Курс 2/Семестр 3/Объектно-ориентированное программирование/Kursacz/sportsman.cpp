#include "sportsman.h"

Sportsman::Sportsman()
{
}

/*
 * Получаем ФИО
 */
struct Sportsman::FIO
Sportsman::getFIO()
{
    return fio;
}

/*
 * Устанавливаем ФИО
 */
void
Sportsman::setFIO(QString nm, QString ot, QString fam)
{
    fio.name = nm;
    fio.otczestwo = ot;
    fio.nazwisko = fam;
}

/*
 * Получаем страну участника
 */
QString
Sportsman::getcountry()
{
    return country;
}

/*
 * Устанавливаем страну участника
 */
void
Sportsman::setcountry(QString _country)
{
    country = _country;
}

/*
 *  Получаем еще одну дисциплину, в которой участвовал спортсмен
 */
QString
Sportsman::getsport(int i)
{
    return (results.at(i)).sport;
}

/*
 * Добавляем вид спорта и результат
 */
void
Sportsman::addsports(QString sport, double result)
{
    results.push_back(res());
    reverse( results.begin(), results.end() );
    results[0].sport = sport;
    results[0].result = result;
}

/*
 *  Получаем результат
 */
struct Sportsman::res
Sportsman::getresults(int i)
{
    return results.at(i);
}

/*
 *  Сбрасываем результат
 */
void
Sportsman::dropresults()
{
    results.clear();
}


/*
 * Получить значение пола
 */
QString Sportsman::getsex()
{
    return sex;
}

/*
 * Получить значение пола
 */
void
Sportsman::setsex(QString sx)
{
     sex = sx;
}

int
Sportsman::resultscount()
{
    return results.size();
}

