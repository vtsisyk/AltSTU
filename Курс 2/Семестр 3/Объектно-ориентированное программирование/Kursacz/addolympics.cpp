#include "addolympics.h"
#include "ui_addolympics.h"
#include <QValidator>

AddOlympics::AddOlympics(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddOlympics)
{
    ui->setupUi(this);
    //ui->lineEdit->setCursorPosition(0);
    QRegExp city("^[a-zA-ZА-Яа-я]+(?:[\s-][a-zA-ZА-Яа-я]+)*$");
    QValidator *validator = new QRegExpValidator(city, this);

    ui->lineEdit->setValidator(validator);

}

AddOlympics::~AddOlympics()
{
    delete ui;
}
void
AddOlympics::done()
{

    if(ui->lineEdit->hasAcceptableInput()){
        close();
    /* сюда все проверки */
        emit accept();
    }
}

QString
AddOlympics::getname()
{
    return ui->lineEdit->text();
}
QString
AddOlympics::getseason()
{
    return ui->comboBox->currentText();
}
