#ifndef OLYMPICS_H
#define OLYMPICS_H

#include <string>
#include <vector>
#include "sportsman.h"
#include <QString>
using namespace std;

class Olympics
{
public:
    Olympics();
    Olympics(QString _name, QString _season);
    QString getname();
    int sportsmenscount();
    void addSportsman(Sportsman *_sportik);
    void records_set();
    Sportsman get_sportsman(int i);


protected:
    QString name;
    QString season;
    vector<Sportsman> sportmans;
    vector<Sportsman> meisters;
};

#endif // OLYMPICS_H
