#ifndef SPORTS_H
#define SPORTS_H
#include <string>
#include <vector>

#include "abstractsport.h"

class Sports : public AbstractSport
{
public:
    Sports();
    void setrecord(Sportsman _meister);
    Sportsman getmeister();
    double getrecord();
};

#endif // SPORTS_H
