#-------------------------------------------------
#
# Project created by QtCreator 2016-01-18T18:01:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kursacz
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sportsman.cpp \
    olympics.cpp \
    abstractsport.cpp \
    sports.cpp \
    aboutdialog.cpp \
    addsportsman.cpp \
    addolympics.cpp \
    records.cpp

HEADERS  += mainwindow.h \
    sportsman.h \
    olympics.h \
    abstractsport.h \
    sports.h \
    aboutdialog.h \
    addsportsman.h \
    addolympics.h \
    records.h

FORMS    += mainwindow.ui \
    aboutdialog.ui \
    addsportsman.ui \
    addolympics.ui \
    records.ui
