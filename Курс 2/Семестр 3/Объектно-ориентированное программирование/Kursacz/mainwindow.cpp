#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
 // olympicsdialog = new AddOlympics();

}

MainWindow::~MainWindow()
{
  delete ui;
}

void
MainWindow::showabout()
{
    aboutdialog.show();
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));

}

void
MainWindow::showsport()
{
    sportdialog = new AddSportsman(Olympiady);
    sportdialog->show();

    int result = sportdialog->exec();
    if (QDialog::Accepted == result)
    {
        for(int i = 0 ; i < Olympiady.size(); i++)
        {
            if(sportdialog->getname() == Olympiady.at(i).getname()){
                Olympiady.at(i).addSportsman(sportdialog->getsportik());
            }


        }
    }
}

/*
 * Новая олимпиада
 */
void
MainWindow::addolympics()
{
    QLineEdit *test;
        olympicsdialog = new AddOlympics();
        int result = olympicsdialog->exec();
        if (QDialog::Accepted == result)
        {
            //QLineEdit *test;

            /* тот же элемент? */
            bool same = 0;
            Olympiady.push_back(Olympics(olympicsdialog->getname(), olympicsdialog->getseason()));
            for(int i = 0; i< ui->comboBox->count(); i++)
                if(ui->comboBox->itemText(i)== Olympiady[Olympiady.size()-1].getname())
                    same = 1;
            if(!same)
                ui->comboBox->addItem(Olympiady[Olympiady.size()-1].getname());

        }
}
void
MainWindow::doit()
{
    QLineEdit *test;
    //test = olympicsdialog->gettext();
     //olympicsdialog->close();
     //QString test;
     ui->comboBox->addItem(test->text());

}
void
MainWindow::sort_by_country()
{
    /*берешь список спортсменов олимпиады*/
    /* выбираешь спортсменов из нужных стран */
    /*записываешь во второй  listview*/

}

void
MainWindow::records_set(){

    superrecord= new Records();
    superrecord->show();
    /*выводишь в первый лист список видов спорта */
}


void
MainWindow::sort_by_olympics()
{

    /* чистим cписок стран */
    ui->Left_List->clear();
    ui->Center_List->clear();
    ui->treeWidget->clear();
    for(int i = 0; i<  Olympiady.size() ; i++){
        for(int j = 0; j < Olympiady.at(i).sportsmenscount(); j++){
           if(Olympiady.at(i).getname() == ui->comboBox->currentText()){

               /* тот же элемент? */
               bool same = 0;
               for(int k = 0; k< ui->Left_List->count(); k++)
                   if(ui->Left_List->item(k)->text()== Olympiady.at(i).get_sportsman(j).getcountry())
                       same = 1;
               if(!same)
                   ui->Left_List->addItem(Olympiady.at(i).get_sportsman(j).getcountry());

           }
       }
  }

}

/*
 * Выводим список участников от стран
 */
void
MainWindow::country_selected(QListWidgetItem *country)
{
     ui->Center_List->clear();
    for(int i = 0; i<  Olympiady.size() ; i++){
        for(int j = 0; j < Olympiady.at(i).sportsmenscount(); j++){
            if(country->text()== Olympiady.at(i).get_sportsman(j).getcountry() &&
               Olympiady.at(i).getname() ==  ui->comboBox->currentText()){

                QString name = Olympiady.at(i).get_sportsman(j).getFIO().name;
                QString otczestwo = Olympiady.at(i).get_sportsman(j).getFIO().otczestwo;
                QString nazwisko = Olympiady.at(i).get_sportsman(j).getFIO().nazwisko;
                ui->Center_List->addItem(name + " "+ otczestwo +" " + nazwisko);
            }
        }
    }


}

void
MainWindow::sportsman_selected(QListWidgetItem *sportsman)
{

    ui->treeWidget->clear();
    /*На двойной клик - вывод текущего рекорда*/
    /*Захерачь вывод в всего этого дерьма */
    Sportsman sportik;
    QStringList pieces = sportsman->text().split(" ");
    for(int i = 0; i<  Olympiady.size() ; i++){
        for(int j = 0; j < Olympiady.at(i).sportsmenscount(); j++){
            if(Olympiady.at(i).get_sportsman(j).getFIO().name == pieces.at(0))
                sportik = Olympiady.at(i).get_sportsman(j);

        }
    }

    QTreeWidget * tree = ui->treeWidget;
    for(int i =0 ; i < sportik.resultscount(); i++)
    {
        QTreeWidgetItem * topLevel = new QTreeWidgetItem();
        topLevel->setText(0, sportik.getresults(i).sport);

        QTreeWidgetItem * item = new QTreeWidgetItem();
        item->setText(0,"Результат: " + QString::number(sportik.getresults(i).result));
        topLevel->addChild(item);

        tree->addTopLevelItem(topLevel);

    }


  //  QTreeWidgetItem * topLevel = new QTreeWidgetItem();
    //topLevel->setText(0, "This is top level");



}

/*
 * Сохранение
 */
void
MainWindow::savedata()
{

    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Открыть файл"),
                "/home/vlad/Dropbox/Курсач/Kursacz",
                "Text File (*.txt)");


/*
    QString name = QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
               "/home/vlad/Empty/Kursacz",
                "Text File (*.txt)");
    QMessageBox::information(this, tr("File Name"), name);

    QString filename = "Data.txt";
       QFile file(filename);
       if (file.open(QIODevice::ReadWrite)) {
           QTextStream stream(&file);
           stream << "something" << endl;

       for(int i = 0; i<  Olympiady.size() ; i++){
           for(int j = 0; j < Olympiady.at(i).sportsmenscount(); j++){

               QString name = Olympiady.at(i).get_sportsman(j).getFIO().name;
               QString otczestwo = Olympiady.at(i).get_sportsman(j).getFIO().otczestwo;
               QString nazwisko = Olympiady.at(i).get_sportsman(j).getFIO().nazwisko;
               stream << name << "\n" << otczestwo << "\n" <<  nazwisko << "\n";
           }
       }
       }
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);

    bool need_next = 0;
    QString name;
    QString otczestwo;
    QString nazwisko;
    QString country;
    QString sex;
    QString tmp;
    Sportsman *spo = new Sportsman();
    int i = 0;
    while(!in.atEnd()) {
        spo = new Sportsman();
        tmp = in.readLine();
        if(Olympiady.size() == 0){
            need_next = 0;
            Olympiady.push_back(Olympics(tmp, in.readLine()));
            ui->comboBox->addItem(tmp);
        }else if(Olympiady.at(i).getname() != tmp ){
            i++;
            Olympiady.push_back(Olympics(tmp, in.readLine()));
             ui->comboBox->addItem(tmp);
            need_next = 1;
        }else{
            in.readLine();
            need_next = 0;
        }

         name = in.readLine();
         otczestwo = in.readLine();
         nazwisko = in.readLine();
         country = in.readLine();
         sex = in.readLine().remove(QRegExp("[\\n\\t\\r]"));


         spo->setFIO(name, otczestwo, nazwisko);
         spo->setcountry(country);
         spo->setsex(sex);
         while(true){
             tmp = in.readLine();
             spo->addsports(tmp, in.readLine().toDouble());
             if(in.readLine()== "---")
                 break;

         }


         Olympiady.at(i).addSportsman(spo);


    }

    file.close();
       */
}
void
MainWindow::loaddata()
{

    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Открыть файл"),
                "/home/vlad/Dropbox/Курсач/Kursacz",
                "Text File (*.txt)");
    ui->comboBox->clear();

    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);

    bool need_next = 0;
    QString name;
    QString otczestwo;
    QString nazwisko;
    QString country;
    QString sex;
    QString tmp;
    Sportsman *spo = new Sportsman();
    int i = 0;
    while(!in.atEnd()) {
        spo = new Sportsman();
        tmp = in.readLine();
        if(Olympiady.size() == 0){
            need_next = 0;
            Olympiady.push_back(Olympics(tmp, in.readLine()));
            ui->comboBox->addItem(tmp);
        }else if(Olympiady.at(i).getname() != tmp ){
            i++;
            Olympiady.push_back(Olympics(tmp, in.readLine()));
             ui->comboBox->addItem(tmp);
            need_next = 1;
        }else{
            in.readLine();
            need_next = 0;
        }

         name = in.readLine();
         otczestwo = in.readLine();
         nazwisko = in.readLine();
         country = in.readLine();
         sex = in.readLine().remove(QRegExp("[\\n\\t\\r]"));


         spo->setFIO(name, otczestwo, nazwisko);
         spo->setcountry(country);
         spo->setsex(sex);
         while(true){
             tmp = in.readLine();
             spo->addsports(tmp, in.readLine().toDouble());
             if(in.readLine()== "---")
                 break;

         }


         Olympiady.at(i).addSportsman(spo);


    }

    file.close();


}
