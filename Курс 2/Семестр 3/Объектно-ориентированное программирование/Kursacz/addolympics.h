#ifndef ADDOLYMPICS_H
#define ADDOLYMPICS_H

#include <QDialog>
#include <QLineEdit>
namespace Ui {
class AddOlympics;
}

class AddOlympics : public QDialog
{
    Q_OBJECT

public:

    explicit AddOlympics(QWidget *parent = 0);
    ~AddOlympics();

public slots:
    void done();

public:
    QString getname();
    QString getseason();

private:
    Ui::AddOlympics *ui;

};

#endif // ADDOLYMPICS_H
