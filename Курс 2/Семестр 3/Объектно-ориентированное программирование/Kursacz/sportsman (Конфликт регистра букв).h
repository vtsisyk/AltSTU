#ifndef SPORTSMAN_H
#define SPORTSMAN_H
#include <string>
#include <vector>
using namespace std;
class Sportsman{

private:
        struct res{
            string sport;
            double result;
        };
        struct FIO{
            string name;
            string otczestwo;
            string nazwisko;
        };

        struct FIO fio;
        string country;
        vector<res> results;
        vector<string> sports;
        bool sex;

public:
        /*
         * Получаем ФИО
         */
        struct FIO getFIO(){
            return fio;
        }
        /*
         * Устанавливаем ФИО
         */
        void setFIO(string nm, string ot, string fam){
            fio.name = nm;
            fio.otczestwo = ot;
            fio.nazwisko = fam;
        }
        /*
         * Получаем страну участника
         */
        string getcountry(){
            return country;
        }
        /*
         * Устанавливаем страну участника
         */
        void setFIO(string _country){
            country = _country;
        }

        /*
         *  Получаем список видов спорта, в которых участник принимал участие
         */
        vector<string> getsports(){
            return sports;
        }
        /*
         * Добавляем вид спорта и результат
         */
        void addsports(string sport, double result){
            results.push_back(res());
            results[0].sport = sport;
            results[1].result = result;
        }

        /*
         *  Получаем результат
         */
        struct res getresults(int i){
            return results.at(i);
        }
        /*
         * Получить значение пола
         */
        bool getsex(){
            return sex;
        }
        /*
         * Получить значение пола
         * Пусть  0 - мужчина
         *        1 - женщина
         */
        void setsex(bool sx){
             sex = sx;
        }
};
#endif // SPORTSMAN_H
