#ifndef SPORTSMAN_H
#define SPORTSMAN_H

#include <string>
#include <vector>
#include <QString>
using namespace std;

class Sportsman
{

private:
    struct res{
        QString sport;
        double result;
    };
    struct FIO{
        QString name;
        QString otczestwo;
        QString nazwisko;
    };
    struct FIO fio;
    QString country;
    vector<res> results;
    QString sex;
public:
    Sportsman();
    struct FIO getFIO();
    void setFIO(QString nm, QString ot, QString fam);
    void setcountry(QString _country);
    QString getcountry();
    QString getsport(int i);
    void addsports(QString sport, double result);
    struct res getresults(int i);
    int resultscount();
    void dropresults();
    QString getsex();
    void setsex(QString sx);
};

#endif // SPORTSMAN_H
