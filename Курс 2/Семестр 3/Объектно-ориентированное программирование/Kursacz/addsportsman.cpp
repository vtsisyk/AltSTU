#include "addsportsman.h"
#include "ui_addsportsman.h"



QWidget *scrollWidget;

QHBoxLayout *con;
QVBoxLayout *dis;
QWidget *wi;
QValidator *namevalid;


QRegExp sport("^[a-zA-ZА-Яа-я]+(?:[\s-][a-zA-ZА-Яа-я]+)*$");
QRegExp resulte("(^(0\.[1-9]|[1-9][0-9]{0,2}(\.[1-9])?)$)+");






AddSportsman::AddSportsman(vector<Olympics> Olympiady, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddSportsman)
{
    ui->setupUi(this);

    /* Имя */
    QRegExp name("^[a-zA-ZА-Яа-я]+(?:[\s-][a-zA-ZА-Яа-я]+)*$");
    namevalid = new QRegExpValidator(name, this);
    ui->nameField->setValidator(namevalid);

    /* Отчество */
    QRegExp otczestwo("^[a-zA-ZА-Яа-я]+(?:[\s-][a-zA-ZА-Яа-я]+)*$");
    QValidator *otczestwovalid = new QRegExpValidator(otczestwo, this);
    ui->otczestwoField->setValidator(otczestwovalid);

    /* Фамилия (nazwisko(пол.) - фамилия) */
    QRegExp nazwisko("^[a-zA-ZА-Яа-я]+(?:[\s-][a-zA-ZА-Яа-я]+)*$");
    QValidator *nazwiskovalid = new QRegExpValidator(nazwisko, this);
    ui->nazwiskoField->setValidator(nazwiskovalid);

    /* Страна */
    QRegExp country("^[a-zA-ZА-Яа-я]+(?:[\s-][a-zA-ZА-Яа-я]+)*$");
    QValidator *countryvalid = new QRegExpValidator(country, this);
    ui->countryField->setValidator(countryvalid);

    /*выбирать из текстбокса олимпиаду :) */
       for(int i = 0; i < Olympiady.size(); i++)
           ui->comboBox->addItem(Olympiady[i].getname());


    wi =  new QWidget();
    scrollWidget = new QWidget;
    scrollWidget->setLayout(new QVBoxLayout);
    ui->scrollArea->setWidget(scrollWidget);
    dis = new QVBoxLayout();


}

AddSportsman::AddSportsman(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddSportsman)
{
    ui->setupUi(this);

    /* Имя */
    QRegExp name("([a-zA-ZА-Яа-я]{3,30}\s*)+");
    namevalid = new QRegExpValidator(name, this);
    ui->nameField->setValidator(namevalid);

    /* Отчество */
    QRegExp otczestwo("([a-zA-ZА-Яа-я]{3,30}\s*)+");
    QValidator *otczestwovalid = new QRegExpValidator(otczestwo, this);
    ui->otczestwoField->setValidator(otczestwovalid);

    /* Фамилия (nazwisko(пол.) - фамилия) */
    QRegExp nazwisko("([a-zA-ZА-Яа-я]{3,30}\s*)+");
    QValidator *nazwiskovalid = new QRegExpValidator(nazwisko, this);
    ui->nazwiskoField->setValidator(nazwiskovalid);

    /* Страна */
    QRegExp country("([a-zA-ZА-Яа-я]{3,30}\s*)+");
    QValidator *countryvalid = new QRegExpValidator(country, this);
    ui->countryField->setValidator(countryvalid);

    /*выбирать из текстбокса олимпиаду :) */


   // QLineEdit* central = new QLineEdit;

    //QScrollArea *scrollArea = new QScrollArea;
    wi =  new QWidget();
    scrollWidget = new QWidget;
    scrollWidget->setLayout(new QVBoxLayout);
    ui->scrollArea->setWidget(scrollWidget);
    dis = new QVBoxLayout();

   // wi->setLayout(dis);
   // scrollWidget->layout()->addWidget(wi);  // my custom widgets

}

AddSportsman::~AddSportsman()
{
    delete ui;
}
void
AddSportsman::addsomething()
{


    con = new QHBoxLayout();
    wi =  new QWidget();
    QLabel *sprtlbl = new QLabel("Спорт: ");
    QLineEdit *sprtle = new QLineEdit();
    QLabel *reslbl = new QLabel("  Результат: ");
    QLineEdit *resle = new QLineEdit();



    QValidator *sprtlevalid = new QRegExpValidator(sport, this);
    sprtle->setValidator(sprtlevalid);

    QValidator *reslevalid = new QRegExpValidator(resulte, this);
    resle->setValidator(reslevalid);


    con->addWidget(sprtlbl);
    con->addWidget(sprtle);
    con->addWidget(reslbl);
    con->addWidget(resle);
    //scrollWidget->addLayout(con);
    //dis->addLayout(con);
    wi->setLayout(con);
    scrollWidget->layout()->addWidget(wi);  // my custom widgets

}

void
AddSportsman::removesomething()
{
    QLayoutItem *item;
    if((item = scrollWidget->layout()->takeAt(scrollWidget->layout()->count() - 1)) !=0 ){
       // item = scrollWidget->layout()->takeAt(0);
        scrollWidget->layout()->removeWidget(item->widget());
        delete item->widget();
        scrollWidget->update();
        ui->scrollArea->update();
    }

}

int
AddSportsman::done()
{

    sportik = new Sportsman();
    bool valid = 0;
    QLayoutItem *item;
    QLayoutItem *item1;
    QLayoutItem *item2;
    QObjectList childat;

    int j = scrollWidget->layout()->count();
    for(int i = 0; i < j; i++ ){

        item = scrollWidget->layout()->itemAt(i);
        childat = item->widget()->children();
        QHBoxLayout *q = qobject_cast<QHBoxLayout*>(childat.at(0));
        item1 = q->layout()->itemAt(1);
        item2 = q->layout()->itemAt(3);
        QLineEdit *p = qobject_cast<QLineEdit*>(item1->widget());
        QLineEdit *r = qobject_cast<QLineEdit*>(item2->widget());
        //r->setText(QString("JOHN CENA"));
        bool test1 = p->hasAcceptableInput();
        bool test2 = r->hasAcceptableInput();

         if(p->hasAcceptableInput() && r->hasAcceptableInput()){
             valid = 1;
             sportik->addsports(p->text(), r->text().toDouble());
         }else{
             valid = 0;
             sportik->dropresults();
             return 1;
         }
    }
    if(ui->nameField->hasAcceptableInput() && ui->otczestwoField->hasAcceptableInput() &&
       ui->nazwiskoField->hasAcceptableInput() && ui->countryField->hasAcceptableInput() &&
       ui->comboBox->count() > 0 && (ui->radioButton->isChecked() || ui->radioButton_2->isChecked())){

        sportik->setFIO(ui->nameField->text(), ui->otczestwoField->text(), ui->nazwiskoField->text());
        sportik->setcountry(ui->countryField->text());
        if(ui->radioButton->isChecked())
            sportik->setsex("M");
        else
            sportik->setsex("F");
        close();
        emit accept();
    }else{
        sportik->dropresults();
    }

}

QString
AddSportsman::getname()
{
    return ui->comboBox->currentText();
}

Sportsman*
AddSportsman::getsportik()
{
    return sportik;
}
