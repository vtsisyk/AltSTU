#ifndef ADDSPORTSMAN_H
#define ADDSPORTSMAN_H

#include <QDialog>
#include <QRegExp>
#include <QLabel>
#include <QMainWindow>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "olympics.h"
#include "sportsman.h"

using namespace std;
namespace Ui {
class AddSportsman;
}

class AddSportsman : public QDialog
{
    Q_OBJECT

public:
    explicit AddSportsman( vector<Olympics> Olympiady, QWidget *parent = 0);
    explicit AddSportsman(QWidget *parent = 0);
    ~AddSportsman();


public slots:
    void addsomething();
    void removesomething();
    QString getname();
    int done();
public:
    Sportsman* getsportik();
private:
    Ui::AddSportsman *ui;
    Sportsman *sportik;
};

#endif // ADDSPORTSMAN_H
