package com.example.vlad.task19;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import  android.util.Config;
import android.view.Menu;
import android.widget.Toast;
import android.graphics.drawable.BitmapDrawable;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {
    private Button bt1;
    private Button bt2;
    private Button bt3;
    private ImageView iv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt1=(Button)findViewById(R.id.button);
        bt2=(Button)findViewById(R.id.button2);
        bt3=(Button)findViewById(R.id.button3);
        bt1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                iv1.setImageResource(R.drawable.dog);
                //ShowIm();
            }
        });

        bt2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                iv1.setImageResource(R.drawable.dog);
                ShowIm();
            }
        });

        bt3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                iv1.setImageResource(R.drawable.dog);
               ShowBoth();
            }
        });
        iv1=(ImageView)findViewById(R.id.imageView);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Intent intent = new Intent(this, ImageWork.class);
        startActivity(intent);
        return true;

    }


    public void ShowIm()
    {
        int w=iv1.getWidth();  // размеры ImageView
        int h=iv1.getHeight();
        Bitmap bm=Bitmap.createBitmap(w, h,  Bitmap.Config.ARGB_8888); // чистая битовая карта
        // ARGB_8888 - для прозрачности, R, G, B - по 8 бит. Прозрачность [0-255] -
        // 0 - прозрачная, 255 - не прозрачная
        Canvas cn = new Canvas(bm);  // холст от чистой битовой карты
        Bitmap bm1 = BitmapFactory.decodeResource(getResources(),
                R.drawable.dog); // битовая карта pic.jpg
        int width = bm1.getWidth();   // размеры pic.jpg
        int height = bm1.getHeight();
        Rect rs=new Rect();
        Rect rd=new Rect();
        rs.left=200;
        rs.top=240;
        rs.right=500;
        rs.bottom=950;  // прямоугольник из pic.jpg
        rd.left=50;
        rd.top=30;
        rd.right=550;
        rd.bottom=540;  // прямоугольник от ImageView

        cn.drawBitmap(bm1, rs, rd, null);  // рисование на холсте части изображения
        iv1.setImageDrawable(new BitmapDrawable(getResources(), bm));

    }
    public void ShowBoth(){

        int w=iv1.getWidth();  // размеры ImageView
        int h=iv1.getHeight();
        Bitmap bm=Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888); // чистая битовая карта

        Bitmap bm3 = BitmapFactory.decodeResource(getResources(),
                R.drawable.dog);

        Bitmap bm1= BitmapFactory.decodeResource(getResources(),
                R.drawable.shia);
        Bitmap bm2 = Bitmap.createScaledBitmap(bm1, w, h, true);

        Rect rs=new Rect();
        Rect rd=new Rect();
        Canvas cn = new Canvas(bm);
        int width = w/4;  // размер второго изображения
        int height = h/4;


        cn.drawBitmap(bm2, 0, 0, null);


        rs.left=400;
        rs.top=240;
        rs.right=500;
        rs.bottom=950;  // прямоугольник из pic.jpg

        rd.left=50;
        rd.top=30;
        rd.right=w/4;
        rd.bottom=h/4;  // прямоугольни

        cn.drawBitmap(bm3, rs, rd, null);  // рисование второго
        iv1.setImageDrawable(new BitmapDrawable(getResources(), bm));



    }

}


