package com.example.vlad.task19;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import  android.util.Config;
import android.view.Menu;
import android.widget.Toast;
import android.graphics.drawable.BitmapDrawable;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.content.Intent;
import android.view.MotionEvent;
import android.graphics.drawable.Drawable;
import android.graphics.Color;
import  	android.widget.RadioButton;

public class ImageWork extends AppCompatActivity {

    private ImageView iv1;
    private ImageView iv2;
    private Button bt4;


    double x,y;
    int OldX,OldY,MouseX,MouseY; // начальные и текущие координаты
    int Regim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_work);
        iv2=(ImageView)findViewById(R.id.imageView2);

        Drawable d = getResources().getDrawable(R.drawable.paren);
        iv1=(ImageView)findViewById(R.id.imageView1);
        int h1 = d.getIntrinsicHeight();
        int w1 = d.getIntrinsicWidth();
        Bitmap bm=Bitmap.createBitmap(w1, h1, Bitmap.Config.ARGB_8888);
        Bitmap bm1= BitmapFactory.decodeResource(getResources(),
                R.drawable.paren);
        Canvas cn = new Canvas(bm);

        Rect rs=new Rect();
        Rect rd=new Rect();
        rs.left=0;
        rs.top=0;
        rs.right=w1;
        rs.bottom=h1;  // прямоугольник из pic.jpg

        rd.left=0;
        rd.top=0;
        rd.right=w1;
        rd.bottom=h1;  // прямоугольник от ImageView

        cn.drawBitmap(bm1, rs, rd, null);  // рисование на холсте части изображения
        iv1.setImageDrawable(new BitmapDrawable(getResources(), bm));





       // iv1.setImageResource(R.drawable.paren);
       // iv2.setImageResource(R.drawable.dog);

        final DragRectView view = (DragRectView) findViewById(R.id.dragRect);

        if (null != view) {
            view.setOnUpCallback(new DragRectView.OnUpCallback() {
                @Override
                public void onRectFinished(final Rect rect) {
                 //  Toast.makeText(getApplicationContext(), "Rect is (" + rect.left + ", " + rect.top + ", " + rect.right + ", " + rect.bottom + ")",
                //          Toast.LENGTH_LONG).show();
                    ShowIm(rect);
                }

            });

        }

        iv2=(ImageView)findViewById(R.id.imageView2);


       // iv3.setImageResource(R.drawable.paren);
       // iv3=(ImageView)findViewById(R.id.imageView1);
    }



    public void ShowIm(final Rect rect)
    {

        RadioButton rb;

        rb = (RadioButton) findViewById(R.id.radioButton);
        boolean state = true;
        if(rb.isChecked())
            state = true;
        else
            state = false;


        int w=iv1.getWidth();  // размеры ImageView
        int h= iv1.getHeight();

        Drawable d = getResources().getDrawable(R.drawable.paren);
        int h1 = d.getIntrinsicHeight();
        int w1 = d.getIntrinsicWidth();
        Bitmap bm=Bitmap.createBitmap(w1 , h1,  Bitmap.Config.ARGB_8888); // чистая битовая карта
        // ARGB_8888 - для прозрачности, R, G, B - по 8 бит. Прозрачность [0-255] -
        // 0 - прозрачная, 255 - не прозрачная
        Canvas cn = new Canvas(bm);  // холст от чистой битовой карты
        Bitmap bm1 = BitmapFactory.decodeResource(getResources(), R.drawable.paren); // битовая карта pic.jpg
        Rect rs=new Rect();
        Rect rd=new Rect();
        int tmp;

        /* пофиксить выделение */
        rs.left=rect.left   -(w - w1)/2 ;
        rs.top=rect.top - (h - h1)/2 ; ;
        rs.right=rect.right  - (w - w1)/2 ;
        rs.bottom= rect.bottom  - (h - h1)/2   ;  // прямоугольник из pic.jpg

        rd.left=rect.left   ;
        rd.top=rect.top ; ;
        rd.right=rect.right  ;
        rd.bottom=rect.bottom    ;  // прямоугольник из pic.jpg

        if(state == false) {
            for (int i = rect.left; i < rect.right ; i++)
                for (int j = rect.top; j < rect.bottom ; j++) {
                    int x = Math.abs((int) (20 * Math.sin(2.0*Math.PI *(double) j / 30))) ;
                    int y = j;
                    int pixel = bm1.getPixel(i, j);

                    int redValue = Color.red(pixel);
                    int blueValue = Color.blue(pixel);
                    int greenValue = Color.green(pixel);
                    bm.setPixel(x, y, Color.rgb(redValue, greenValue, blueValue));

                }
        }else{

            for (int i = rect.left; i < rect.right ; i++)
                for (int j = rect.top; j < rect.bottom ; j++) {
                    int x = i ;
                    int y = Math.abs(j + (int) (20 * Math.sin(2.0*Math.PI *(double) i/ 30))) ;
                    int pixel = bm1.getPixel(i, j);

                    int redValue = Color.red(pixel);
                    int blueValue = Color.blue(pixel);
                    int greenValue = Color.green(pixel);
                    bm.setPixel(x, y, Color.rgb(redValue, greenValue, blueValue));

                }

        }
        System.out.println(rect.left + "  " + rect.top + " " + rect.right + " " + rect.bottom);



       // cn.drawBitmap(bm, rs, rd, null);  // рисование на холсте части изображения

        iv2.setImageDrawable(new BitmapDrawable(getResources(), bm));

        //iv2=(ImageView)findViewById(R.id.imageView2);
    }

}

