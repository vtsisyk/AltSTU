#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>


QImage image("../Task19/image.jpg");
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new Scene();
    scene->addPixmap(QPixmap::fromImage(image));
    scene_end = new Scene();
    ui->graphicsView->setScene(scene);
    ui->graphicsView_2->setScene(scene_end);
    connect(scene,SIGNAL(Get(QPoint,QPoint)),this,SLOT(Open(QPoint, QPoint)));
}
void MainWindow::Open(QPoint a, QPoint b){

    double k = 2;
    bool state;
    if(ui->radioButton->isChecked())
        state = 0 ;
    else
        state = 1;

    QPoint base;
    QImage *end_image = new QImage(abs(a.x()-b.x()),abs(a.y()-b.y()),QImage::Format_RGB16);
    QPoint z(abs(b.x()-a.x()),abs(b.y()-a.y()));
    if(a.x()<b.x()){
        if(a.y()<b.y()) base=a;
        else{
            base.setX(a.x());
            base.setY(b.y());
        }
    }
    else{
        if(a.y()<b.y()){
            base.setX(b.x());
            base.setY(a.y());
        }
        else base=b;
    }

    if(state == 0){
        for(int i = 0;i<z.x();i++){
            for(int j = 0;j<z.y();j++){
                int x = i;
                int y = j + 20*sin(2*3.14 * i/ 128);
                QColor alpha= image.pixel(base.x()+i,base.y()+j);
                end_image->setPixel(x,y,alpha.rgb());
            }
        }
    }else{
        for(int i = 0;i<z.x();i++){
            for(int j = 0;j<z.y();j++){
                int x = i + 20*sin(2*3.14 * j/ 30);
                int y = j;
                QColor alpha= image.pixel(base.x()+i,base.y()+j);
                end_image->setPixel(x,y,alpha.rgb());
            }
        }


    }
    scene_end->clear();
    scene_end->addPixmap(QPixmap::fromImage(*end_image));
    ui->graphicsView_2->setScene(scene_end);



}
MainWindow::~MainWindow()
{
    delete ui;
}
