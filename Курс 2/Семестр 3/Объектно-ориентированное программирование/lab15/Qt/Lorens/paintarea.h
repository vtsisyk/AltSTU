#ifndef PAINTAREA_H
#define PAINTAREA_H

#include <QWidget>
#include <QtGui/QPen>

class PaintArea : public QWidget
{
    Q_OBJECT

public:
    PaintArea(QWidget *parent = 0);

public slots:
    void setR(double r);
    void setS(double s);
    void setB(double b);
    void setH(double h);
    void setX0(double x0);
    void setY0(double y0);
    void setZ0(double z0);

protected:
    void paintEvent(QPaintEvent *event);


private:
    QPen pen;
    double m_s, m_r, m_b;
    double m_h;
    double m_x0, m_y0, m_z0;
};

#endif // PAINTAREA_H
