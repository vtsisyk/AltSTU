#-------------------------------------------------
#
# Project created by QtCreator 2016-01-11T08:23:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lorens
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    paintarea.cpp

HEADERS  += mainwindow.h \
    paintarea.h

FORMS    += mainwindow.ui
