#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	// https://wiki.qt.io/How_to_Center_a_Window_on_the_Screen
	setGeometry(
	    QStyle::alignedRect(
		Qt::LeftToRight,
		Qt::AlignCenter,
		size(),
		qApp->desktop()->availableGeometry()
	    )
	);

}

MainWindow::~MainWindow()
{
	delete ui;
}
