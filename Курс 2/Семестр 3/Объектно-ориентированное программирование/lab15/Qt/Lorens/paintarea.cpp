#include "paintarea.h"

#include <QtGui/QPainter>
#include <QDebug>

PaintArea::PaintArea(QWidget *parent)
    : QWidget(parent),
      pen(Qt::black, 1, Qt::SolidLine)
{
	/*
	 * При r=28; s=10; b=8/3 система ведет себя хаотически
	 */
	m_s = 10;
	m_r = 28;
	m_b = 8.0/3;
	m_h = 0.0004;
	m_x0 = 3;
	m_y0 = 2;
	m_z0 = 15;
}

void PaintArea::setR(double r)
{
	m_r = r;
	update();
}

void PaintArea::setS(double s)
{
	m_s = s;
	update();
}

void PaintArea::setB(double b)
{
	m_b = b;
	update();
}

void PaintArea::setH(double h)
{
	m_h = h;
	update();
}

void PaintArea::setX0(double x0)
{
	m_x0 = x0;
	update();
}

void PaintArea::setY0(double y0)
{
	m_y0 = y0;
	update();
}

void PaintArea::setZ0(double z0)
{
	m_z0 = z0;
	update();
}

void PaintArea::paintEvent(QPaintEvent * /* event */)
{
	QPainter painter(this);
	painter.setPen(pen);

	QRect bounds = visibleRegion().boundingRect();

	/*
	 * X'=-sX+sY
	 * Y'=-XZ+rX-Y
	 * Z'=XY-bZ
	 *
	*/
	double x = m_x0, y = m_y0, z = m_z0, x1, y1, z1;
	for(int i=0; i< 500000;++i) {
		x1 = x + m_s*(-x+y)*m_h;
		y1 = y + (m_r*x-y-z*x)*m_h;
		z1 = z + (-m_b*z+x*y)*m_h;

		x = x1;
		y = y1;
		z = z1;


		QPoint point((int)(15*(y - x * 0.3) + bounds.width() / 2),
			     (int)(8*(z + x * 0.3)));
		painter.drawPoint(point);
	}
}
