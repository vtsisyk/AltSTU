/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "paintarea.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QWidget *params;
    QGridLayout *gridLayout_3;
    QLabel *label_2;
    QLabel *label;
    QDoubleSpinBox *rSpinBox;
    QDoubleSpinBox *doubleSpinBox_2;
    QLabel *label_3;
    QDoubleSpinBox *doubleSpinBox_3;
    QLabel *label_4;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_5;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QDoubleSpinBox *doubleSpinBox_4;
    QDoubleSpinBox *doubleSpinBox_5;
    QDoubleSpinBox *doubleSpinBox_6;
    PaintArea *paintarea;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(907, 654);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        params = new QWidget(centralWidget);
        params->setObjectName(QStringLiteral("params"));
        params->setMaximumSize(QSize(16777215, 150));
        gridLayout_3 = new QGridLayout(params);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_2 = new QLabel(params);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_3->addWidget(label_2, 0, 0, 1, 1);

        label = new QLabel(params);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_3->addWidget(label, 1, 0, 1, 1);

        rSpinBox = new QDoubleSpinBox(params);
        rSpinBox->setObjectName(QStringLiteral("rSpinBox"));
        rSpinBox->setDecimals(0);
        rSpinBox->setMinimum(10);
        rSpinBox->setMaximum(40);
        rSpinBox->setValue(10);

        gridLayout_3->addWidget(rSpinBox, 0, 1, 1, 1);

        doubleSpinBox_2 = new QDoubleSpinBox(params);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setDecimals(0);
        doubleSpinBox_2->setMinimum(10);
        doubleSpinBox_2->setMaximum(40);
        doubleSpinBox_2->setValue(28);

        gridLayout_3->addWidget(doubleSpinBox_2, 1, 1, 1, 1);

        label_3 = new QLabel(params);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_3->addWidget(label_3, 2, 0, 1, 1);

        doubleSpinBox_3 = new QDoubleSpinBox(params);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setDecimals(1);
        doubleSpinBox_3->setMinimum(1);
        doubleSpinBox_3->setMaximum(10);
        doubleSpinBox_3->setSingleStep(0.1);
        doubleSpinBox_3->setValue(2.7);

        gridLayout_3->addWidget(doubleSpinBox_3, 2, 1, 1, 1);

        label_4 = new QLabel(params);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_3->addWidget(label_4, 3, 0, 1, 1);

        doubleSpinBox = new QDoubleSpinBox(params);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setDecimals(4);
        doubleSpinBox->setMinimum(0.0001);
        doubleSpinBox->setMaximum(10);
        doubleSpinBox->setSingleStep(0.0001);
        doubleSpinBox->setValue(0.0004);

        gridLayout_3->addWidget(doubleSpinBox, 3, 1, 1, 1);

        label_5 = new QLabel(params);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_3->addWidget(label_5, 4, 0, 1, 1);

        widget = new QWidget(params);
        widget->setObjectName(QStringLiteral("widget"));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        doubleSpinBox_4 = new QDoubleSpinBox(widget);
        doubleSpinBox_4->setObjectName(QStringLiteral("doubleSpinBox_4"));
        doubleSpinBox_4->setDecimals(2);
        doubleSpinBox_4->setValue(3);

        horizontalLayout->addWidget(doubleSpinBox_4);

        doubleSpinBox_5 = new QDoubleSpinBox(widget);
        doubleSpinBox_5->setObjectName(QStringLiteral("doubleSpinBox_5"));
        doubleSpinBox_5->setValue(2);

        horizontalLayout->addWidget(doubleSpinBox_5);

        doubleSpinBox_6 = new QDoubleSpinBox(widget);
        doubleSpinBox_6->setObjectName(QStringLiteral("doubleSpinBox_6"));
        doubleSpinBox_6->setValue(15);

        horizontalLayout->addWidget(doubleSpinBox_6);


        gridLayout_3->addWidget(widget, 4, 1, 1, 1);


        verticalLayout->addWidget(params);

        paintarea = new PaintArea(centralWidget);
        paintarea->setObjectName(QStringLiteral("paintarea"));

        verticalLayout->addWidget(paintarea);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);
        QObject::connect(rSpinBox, SIGNAL(valueChanged(double)), paintarea, SLOT(setS(double)));
        QObject::connect(doubleSpinBox_2, SIGNAL(valueChanged(double)), paintarea, SLOT(setR(double)));
        QObject::connect(doubleSpinBox_3, SIGNAL(valueChanged(double)), paintarea, SLOT(setB(double)));
        QObject::connect(doubleSpinBox, SIGNAL(valueChanged(double)), paintarea, SLOT(setH(double)));
        QObject::connect(doubleSpinBox_4, SIGNAL(valueChanged(double)), paintarea, SLOT(setX0(double)));
        QObject::connect(doubleSpinBox_5, SIGNAL(valueChanged(double)), paintarea, SLOT(setY0(double)));
        QObject::connect(doubleSpinBox_6, SIGNAL(valueChanged(double)), paintarea, SLOT(setZ0(double)));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label_2->setText(QApplication::translate("MainWindow", "s", 0));
        label->setText(QApplication::translate("MainWindow", "r", 0));
        label_3->setText(QApplication::translate("MainWindow", "b", 0));
        label_4->setText(QApplication::translate("MainWindow", "h", 0));
        label_5->setText(QApplication::translate("MainWindow", "x0, y0, z0", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
