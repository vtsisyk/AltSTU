/****************************************************************************
** Meta object code from reading C++ file 'paintarea.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "paintarea.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'paintarea.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PaintArea_t {
    QByteArrayData data[16];
    char stringdata0[66];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PaintArea_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PaintArea_t qt_meta_stringdata_PaintArea = {
    {
QT_MOC_LITERAL(0, 0, 9), // "PaintArea"
QT_MOC_LITERAL(1, 10, 4), // "setR"
QT_MOC_LITERAL(2, 15, 0), // ""
QT_MOC_LITERAL(3, 16, 1), // "r"
QT_MOC_LITERAL(4, 18, 4), // "setS"
QT_MOC_LITERAL(5, 23, 1), // "s"
QT_MOC_LITERAL(6, 25, 4), // "setB"
QT_MOC_LITERAL(7, 30, 1), // "b"
QT_MOC_LITERAL(8, 32, 4), // "setH"
QT_MOC_LITERAL(9, 37, 1), // "h"
QT_MOC_LITERAL(10, 39, 5), // "setX0"
QT_MOC_LITERAL(11, 45, 2), // "x0"
QT_MOC_LITERAL(12, 48, 5), // "setY0"
QT_MOC_LITERAL(13, 54, 2), // "y0"
QT_MOC_LITERAL(14, 57, 5), // "setZ0"
QT_MOC_LITERAL(15, 63, 2) // "z0"

    },
    "PaintArea\0setR\0\0r\0setS\0s\0setB\0b\0setH\0"
    "h\0setX0\0x0\0setY0\0y0\0setZ0\0z0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PaintArea[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x0a /* Public */,
       4,    1,   52,    2, 0x0a /* Public */,
       6,    1,   55,    2, 0x0a /* Public */,
       8,    1,   58,    2, 0x0a /* Public */,
      10,    1,   61,    2, 0x0a /* Public */,
      12,    1,   64,    2, 0x0a /* Public */,
      14,    1,   67,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void, QMetaType::Double,    7,
    QMetaType::Void, QMetaType::Double,    9,
    QMetaType::Void, QMetaType::Double,   11,
    QMetaType::Void, QMetaType::Double,   13,
    QMetaType::Void, QMetaType::Double,   15,

       0        // eod
};

void PaintArea::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PaintArea *_t = static_cast<PaintArea *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setR((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->setS((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->setB((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->setH((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->setX0((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->setY0((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->setZ0((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject PaintArea::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_PaintArea.data,
      qt_meta_data_PaintArea,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *PaintArea::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PaintArea::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_PaintArea.stringdata0))
        return static_cast<void*>(const_cast< PaintArea*>(this));
    return QWidget::qt_metacast(_clname);
}

int PaintArea::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
