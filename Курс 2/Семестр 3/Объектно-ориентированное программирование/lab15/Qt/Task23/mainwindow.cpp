#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QMessageBox>
#include <QStringListModel>
#include <QTextStream>
#include <QTreeView>
QStringList stringList1;
QStringList stringList;
QStringList articles;

QStringList authors[9];

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /* Загружаем данные */

    QStringListModel *model;

    model = new QStringListModel(this);


    QFile textfile("../Task23/praca.txt");
    if(!textfile.open(QIODevice::ReadOnly)){

        QMessageBox::information(0,"Error", textfile.errorString());
    }

    QTextStream textStream(&textfile);
    while(true){

        QString line = textStream.readLine();
        QStringList tmp = line.split(":");
        if(line.isNull())
            break;
        else{
            articles.append(tmp.at(1).trimmed());
            stringList.append(line.trimmed());
            stringList1.append(line.trimmed());
        }
    }

    authors[0].append("Einstein");
    int k = 0;
    while(!stringList.isEmpty()){


        for(int i = 0; i < authors[k].size(); i++){
            for(int j = 0; j < stringList.size(); j++){
                QStringList tmp = stringList.at(j).split(":");
                QStringList tmp1 = tmp.at(0).split(" ");

                for(int h = 0; h < tmp1.size(); h++){
                    if(tmp1.contains(authors[k].at(i)))
                        if(!tmp1.at(h).contains(authors[k].at(i)) && !tmp1.at(h).isEmpty() &&
                            !authors[k+1].contains(tmp1.at(h)))
                            authors[k+1].append(tmp1.at(h));

                }
            }
        }
        for(int i = 0; i < stringList.size(); i++)
        for(int j = 0; j < authors[k].size(); j++)
            if(stringList.at(i).contains(authors[k].at(j))){
                stringList.removeAt(i);
                if(i!= 0)
                    i--;
            }
        k++;

    }

    model->setStringList(articles);

    ui->listView->setModel(model);

    ui->listView->setEditTriggers(QAbstractItemView::AnyKeyPressed |
                                    QAbstractItemView::DoubleClicked);
    ui->listView->setSelectionMode(QAbstractItemView::ExtendedSelection);
}

void  MainWindow::Litwa(const QModelIndex &index){

   QStringList tmp;
   QStringList answer;
   QString tmp1;
    QModelIndexList list = ui->listView->selectionModel()->selectedIndexes();
    foreach(const QModelIndex &index, list)
         tmp.append( index.data(Qt::DisplayRole).toString());

    QStringListModel *model;

    model = new QStringListModel(this);

    for(int i =0 ; i < stringList1.size();i++ )
        if(stringList1.at(i).contains(tmp.at(0))){
            tmp1 = stringList1.at(i);
            break;

        }
    tmp = tmp1.split(":");
    tmp.removeAt(1);
    tmp1 = tmp.at(0);
    tmp.removeAt(0);
    tmp = tmp1.split(" ");
    int num = 100;
    int test =100;
    int k = 0;
    for(int i = 0; i < tmp.size(); i++)
        if(tmp.at(i).length() < 3)
            tmp.removeAt(i);
    for(int k = 0; k < tmp.size(); k++){
    for(int i = 0; i < 9; i++){
        for(int j =0; j < authors[i].size(); j++){
            if(authors[i].at(j).contains(tmp.at(k))){
                if(test > i){
                    num = i;
                }
            }

        }
        test = 100;

    }
        tmp1 = tmp.at(k) + " Группа: " + QString::number(num);
        answer.append(tmp1);
    }



    model->setStringList(answer);

    ui->listView_2->setModel(model);
}
MainWindow::~MainWindow()
{
    delete ui;
}
