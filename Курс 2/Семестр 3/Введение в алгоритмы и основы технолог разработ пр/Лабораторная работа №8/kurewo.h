#include <stack>
#include <stdio.h>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

struct kurewo {
	int key;
	int pr;

	kurewo *left;
	kurewo *right;
	kurewo(int k, int p);
};
typedef kurewo * pkurewo;
using namespace std;
void split(pkurewo root, pkurewo &l, pkurewo &r, int key);
void merge(pkurewo &root, pkurewo l, pkurewo r);
void add(pkurewo &root, pkurewo it);
void remove(pkurewo &root, int x);
void rm(pkurewo &root);
void print(pkurewo p, string filename);
void read(pkurewo &p, string filename);
