#include "kurewo.h"
kurewo::kurewo(int k, int p){
	key = k;
	pr = p;
	left = 0;
	right = 0;
}

/*	Функция разделения дерева. 
	Первый аргумент - дерево, которое надо разделить 
 	Второй - указатель на левое поддерево
 	Третий - указатель на правое поддерево
 	Четвертый - ключ 
 	
	Сначала проверяем, пустое ли дерево. 
	Затем запихиваем в стек узлы до тех пор, пока у очередного 
	узла не будет потомков.

	Пока стек не пуст мы вытаскиваем очередной узел, 
	проверяем больше ли его ключ заданного ключа. 
	Если да, в правое поддерево узла мы 
	записываем то правое результирующее поддерево, которое уже
	имеем. Обновляем значение правого поддерева. Аналогично поступаем
	в случае, когда ключ узла меньше заданного ключа. 
	Восстанавливаем значение исходного дерева. Конец функции.	
 	
 */
void split(pkurewo root, pkurewo &l, pkurewo &r, int key){

	if(!root)
		l = r = 0;

	std::stack<pkurewo> stk;
	//stk.push(root);
	while(root){
		stk.push(root);
		if(key < root->key)
			root = root->left;
		else 
			root = root->right;
	}

	l = r = 0;
	pkurewo tmp;
	while(!stk.empty()){
		tmp = stk.top();
		stk.pop();
		if(key < tmp->key){
			tmp->left = r;
			r = tmp;
		}else{ 
			tmp->right = l;
			l = tmp;
		}
	}
	root = tmp;
}

void merge(pkurewo &root, pkurewo l, pkurewo r)
{
	if (l == 0) 
		root = r;
	if (r == 0)
		root = l;

	std::stack<pkurewo> stk;
	while (l && r){
		if(l->pr > r->pr){
			stk.push(l);
			l = l->right;
		}else{
			stk.push(r);
			r = r->left;
		}
	}
	if (l == 0) 
		root = r;
	if (r == 0)
		root = l;
	while(!stk.empty()){
		pkurewo tmp = stk.top();
		stk.pop();
		if(root->key < tmp->key){
			tmp->left =  root;
		}else{	
			tmp->right =  root;
		}
		root = tmp;
	}
}

void add(pkurewo &root, pkurewo it){
	pkurewo l, r;
	split(root, l, r, it->key);
	pkurewo tmp = root;
	merge(tmp, l, it);
	merge(root, tmp,  r);
}

void remove(pkurewo &root, int x){

	pkurewo l, m, r;
 	split(root, l, r, x - 1);
	split(r, m,  r, x);
	return merge(root, l, r);
}
void rm(pkurewo &root){
        
       int num;
       while(1){
               cout << "введите число для удаления(-1 для выхода):";
               cin >> num;
               if(num == -1)
                       break;
               remove(root, num);
               
       }
}

void read(pkurewo &p, string filename){
	int a, b;
	p = 0;
	std::ifstream infile(filename.c_str());
	while (infile >> a >> b){
		add(p, new kurewo(a,b));
	}

}
void print(pkurewo p, string filename){
        std::ofstream out(filename.c_str());
        out<< "|" <<std::setw(5)<<"Л" << \
        std::setw(4)<<"|"<< std::setw(10) <<"К - П" <<\
        std::setw(3)<<"|" << std::setw(4);

        out<<"П" << std::setw(4) << "|\n";
        std::stack<pkurewo> stk;
        pkurewo current = p;
        bool done = false;
        while(!done){
                if(current){
                        stk.push(current);
                        current = current->right;
                }else{
                        if(stk.empty())
                                done = true;
                        else{
                                current = stk.top();
                                stk.pop();
                                if(current->left)
                                        out << "|"<< std::setw(4)<<current->left->key ;
                                else 
                                       out <<"|" <<  std::setw(4) <<"-";

                                out <<  std::setw(5) <<"| " <<std::setw(2) << current->key <<" - " << std::setw(2)\ 
								<< current->pr ;

                                if(current->right)
                                        out << std::setw(3) <<"|" << \
                                        std::setw(3)<<current->right->key << \
                                        std::setw(4) << "|\n";
                                else 
                                       	out << std::setw(3) <<"|"<< \
                                        std::setw(3)<<"-" <<std::setw(4) <<"|\n";
                                current = current->left;
                        }
                }
        }
        out<< "\n" ;
}

