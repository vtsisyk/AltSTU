#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

/*
 * Пятидиагональная матрица - значения находятся на главной диагонали
 * и двух диагоналях выше ниже главной. Для упрощения считаем, что число строк
 * и столбцов совпадает (матрица квадратная), т.к. для прямоугольных матриц
 * определение не особо имеет смысл. Для упрощения кода мы храним расширенную
 * матрицу с двумя дополнительными столбцами слева и справа.
 * Это позволяет существенно упросить код чтения и записи.
 */
struct fivediag {
	int dimension;
	double values[];
};

/*
 * Общее количество не нулевых элементов в расширенной матрице.
 */
static inline int
fivediag_extsize(int dimension)
{
	return 5 * dimension;
}

/*
 * Общее количество не нулевых элементов в пятидиагональной матрице.
 * Первый и последний ряд содержать 5 - 2 элементов.
 * Второй и предпоследний ряды содержат 5 - 1 элементов.
 */
static inline int
fivediag_size(int dimension)
{
	return 5 * dimension - 2 - 1 - 1 - 2;
}

struct fivediag *
fivediag_new(int dimension)
{
	/* Матрицы размером меньше 5 не поддерживаются для упрощения кода */
	assert(dimension >= 5);

	/*
	 * Создаем расширенную матрицу
	 * Для этого добавляем по два столбца слева и справа.
	 * Мы храним всего на 6 элментов больше, зато код _get()
	 * получается на порядок проще.
	 */
	int size = fivediag_extsize(dimension);
	struct fivediag *fivediag = (struct fivediag *)
		malloc(sizeof(struct fivediag) +
		sizeof(*fivediag->values) * size);
	if (fivediag == NULL)
		return NULL; /* Кончилась память */

	fivediag->dimension = dimension;
	for (int i = 0; i < size; i++)
		fivediag->values[i] = 0.0;
	return fivediag;
}

/*
 * Высчитывает порядковый номер элемента  в матрице по i,j и возвращает
 * значение.
 */
static inline double
fivediag_get(struct fivediag *fivediag, int i, int j)
{
	assert(i >= 0 && i < fivediag->dimension);
	assert(j >= 0 && j < fivediag->dimension);

	/* Порядковый номер в строке расширенной матрицы */
	int r = (j + 2) - i;
	if (r < 0 || r >= 5)
		return 0.0; /* выход за пределы диагоналей */

	int k = i * 5 + r;
	assert(k >= 0 && k < fivediag_extsize(fivediag->dimension));
	return fivediag->values[k];
}

static inline void
fivediag_set(struct fivediag *fivediag, int pos, double value)
{
	assert(pos >= 0 && pos < fivediag_size(fivediag->dimension));
	/* Пропускаем первые два элемента в первом ряду расширенной матрицы */
	pos += 2;
	/* Пропускаем элемент во втором ряду расширенной матрицы */
	if (pos >= 5)
		pos += 1;
	/* Пропускаем элемент в предпоследнем ряду расширенной матрицы */
	if (pos >= fivediag_extsize(fivediag->dimension) - 6)
		pos += 1;
	assert(pos < fivediag_extsize(fivediag->dimension));
	fivediag->values[pos] = value;
}

static inline int
fivediag_load(struct fivediag *fivediag, FILE *file)
{
	int size = fivediag_size(fivediag->dimension);
	for (int k = 0; k < size; k++) {
		double value = 0.0;
		if (fscanf(file, "%lf", &value) != 1)
			return -1;
		fivediag_set(fivediag, k, value);
	}
	return 0;
}

static inline void
fivediag_print(struct fivediag *fivediag, FILE *file)
{
	for (int i = 0; i < fivediag->dimension; i++) {
		for (int j = 0; j < fivediag->dimension; j++) {
			double value = fivediag_get(fivediag, i, j);
			fprintf(file, "%5.2lf ", value);
		}
		fprintf(file, "\n");
	}
}

struct block {
	int x, y;
	int width, height;
	double values[];
};

struct block *
block_new(int x, int y, int width, int height)
{
	int size = width * height;
	struct block *block = (struct block *)
		malloc(sizeof(*block) + sizeof(*block->values) * size);
	if (block == NULL)
		return NULL;
	block->x = x;
	block->y = y;
	block->width = width;
	block->height = height;
	for (int k = 0; k < size; k++)
		block->values[k] = 0.0;
	return block;
}

int
block_load(struct block *block, FILE *file)
{
	int size = block->width * block->height;
	for (int k = 0; k < size; k++) {
		double value = 0.0;
		if (fscanf(file, "%lf", &value) != 1)
			return -1;
		block->values[k] = value;
	}
	return 0;
}

double
block_get(struct block *block, int i, int j)
{
	i -= block->x;
	j -= block->y;
	if (i < 0 || i >= block->height)
		return 0.0;
	if (j < 0 || j >= block->width)
		return 0.0;
	return block->values[i * block->width + j];
}

void
block_print(struct block *block, int dimension, FILE *file)
{
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			double value = block_get(block, i, j);
			fprintf(file, "%5.2lf ", value);
		}
		fprintf(file, "\n");
	}
}

int
main(int argc, char *argv[])
{
	FILE *input = stdin;
	FILE *output = stdout;

	/*
	 * Читаем размерность. Поскольку пятидигональная матрица квадртная,
	 * то и блочная матрица тоже должна быть такого же размера для
	 * выполнения операции вычитания.
	 */
	int dimension;
	if (fscanf(input, "%d", &dimension) != 1 || dimension < 5) {
		fputs("Неправильный dimension!\n", stderr);
		return 1;
	}

	/*
	 * Читаем пятидиагональную матрицу и сохраняем её в сжатом виде
	 */
	struct fivediag *fivediag = fivediag_new(dimension);
	if (fivediag == NULL) {
		fputs("Не хватает памяти\n", stderr);
		return 1;
	 
	}
	if (fivediag_load(fivediag, stdin) != 0) {
		fputs("Ошибка чтения пятидиагональной матрицы\n", stderr);
		return 1;
	}

	fputs("Пятидиагональная матрица после чтения из файла:\n", output);
	fivediag_print(fivediag, output);

	int x, y, width, height;
	if (fscanf(input, "%d %d %d %d", &x, &y, &width, &height) != 4) {
		fputs("Ошибка чтения x, y, width, height блочной матрицы\n", stderr);
		return 1;
	}

	if (x < 0 || x + width > dimension ||
	    y < 0 || y + height > dimension) {
		fputs("Ошибочные размеры или позиция блочной матрицы\n", stderr);
		return 1;
	}

	struct block *block = block_new(x, y, width, height);
	if (block == NULL) {
		fputs("Не хватает памяти\n", stderr);
		return 1;
	}
	if (block_load(block, input) != 0) {
		fputs("Ошибка чтения блочной матрицы\n", stderr);
		return 1;
	}

	fputs("Блочная матрица после чтения из файла:\n", output);
	block_print(block, dimension, output);

	double c;
	if (fscanf(input, "%lf", &c) != 1) {
		fputs("Ошибка чтения С\n", stderr);
		return 1;
	}
	fprintf(output, "C = %0.2lf\n", c);
	fputs("Результат вычитания A - c * B:\n", output);
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			double a = fivediag_get(fivediag, i, j);
			double b = block_get(block, i, j);
			double value = a - c * b;
			fprintf(output, "%5.2lf ", value);
		}
		fprintf(output, "\n");
	}

	return 0;
}
