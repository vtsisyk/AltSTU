#include "avltree.h"

node::node(int k){
	key = k;
	left =  right = 0; 
	height = 1;
}

int height(node* p)
{
	if(p)
		return p->height;
	else 
		return 0;
}

int get_balance(node* p)
{
	return height(p->right) - height(p->left);
}

void fixheight(node* p)
{
	int hl = height(p->left);
	int hr = height(p->right);
	//p->height = (hl>hr?hl:hr)+1;	
	
	if(hl > hr)
		p->height = hl + 1;
	else 	
		p->height = hr + 1;
		
}

node* rotateright(node* p) 
{
	node* q = p->left;
	p->left = q->right;
	q->right = p;
	fixheight(p);
	fixheight(q);
	return q;
}

node* rotateleft(node* q) 
{
	node* p = q->right;
	q->right = p->left;
	p->left = q;
	fixheight(q);
	fixheight(p);
	return p;
}

node* balance(node* p)
{
	fixheight(p);
	if(get_balance(p) == 2){
		if(get_balance(p->right) < 0)
			p->right = rotateright(p->right);
		return rotateleft(p);
	}
	if( get_balance(p)==-2 ){
		if( get_balance(p->left) > 0 )
			p->left = rotateleft(p->left);
		return rotateright(p);
	}
	return p; 
}


node *insert(node *p, int k)
{
	if(!p)
		return new node(k);
	if(find(p,k))
		return p;

	std::stack<node *> stk;
	while(p){
		stk.push(p);
		if(k < p->key)
			p = p->left;
		else
			p = p->right;
	}
	
	node *tmp = new node (k);
	

	while(!stk.empty()){
		p = stk.top();
		stk.pop();
		if(p->key > tmp->key)
			p->left = tmp;
		else 
			p->right = tmp;
		tmp = balance(p);
	}
	return tmp;
}

node *find(node *p, int k)
{
	node *tmp = p;
	
	std::stack<node *> s;
  	node *current = tmp;
  	bool done = false;
  	
	while (!done) {
    if (current) {	
		s.push(current);
      	current = current->left;
    } else {	
		if (s.empty()) {
		done = true;
	} else {
		current = s.top();	
		s.pop();
		if(k == current->key)
			return current;
		current = current->right;
      }
    }
  }
	return 0;
} 


node *findmin(node *p)
{
	while(p->left)
		p = p->left;
	return p; 
}

node *removemin(node *p)
{
	if(p->left == 0)
		return p->right;

	std::stack<node *> stk;
	node *tmp = p;
	while(p->left != 0){
		stk.push(p);
		p = p->left;
		//p->left = removemin(p->left);
	}
	p = 0; /* или delete p */
	while(!stk.empty()){
		tmp = stk.top();
		tmp->left = p;
		stk.pop();	
		tmp = balance(tmp);
		p = tmp;
	}
	return tmp;
}

node *remove(node *p, int k)
{
	if(!p)
		return 0;
	bool found = 0;
	std::stack<node *> stk;
	while(!found && k != p->key){
		stk.push(p);
		if(k < p->key)
			p = p->left;
		else if(k > p->key)
			p = p->right;
		if(!p)
			found = 1;

	}
	node *tmp = 0;
	if(!p){
		while(!stk.empty()){
			p = stk.top();
			stk.pop();
		}
		return p;
	}
	node *q = p->left;
	node *r = p->right;
	delete p;
	
	if(!r)
		tmp = q;
	else{
		tmp = findmin(r);
		tmp->right = removemin(r);
		tmp->left = q;
		tmp = balance(tmp);
			
	}
	node *tmp1= tmp;
	while(!stk.empty()){
		tmp = stk.top();
		stk.pop();
		if(!tmp1){
			if(k < tmp->key)
				tmp->left = 0;
			else
			   tmp->right = 0;
			tmp1 = balance(tmp);
			continue;
		}
		if(tmp->key > tmp1->key)
			tmp->left = tmp1;
		else 
			tmp->right = tmp1;
		tmp1 = balance(tmp);
	}
	return tmp1;
}

node *read(){
	
	std::ifstream in("input.txt");
	int k;
	node *test = 0;
	while(in >> k){
		test = insert(test, k);	
	}
	return test;
}
void print(node *p, string filename){
	std::ofstream out(filename.c_str());
	out<< "|" <<std::setw(5)<<"Л" << \
	std::setw(4)<<"|"<< std::setw(10) <<"К -- Б" <<\
   	std::setw(3)<<"|" << std::setw(4);

	out<<"П" << std::setw(4) << "|\n";
	std::stack<node *> stk;
	node *current = p;
	bool done = false;	
	while(!done){
		if(current){
			stk.push(current);
			current = current->left;
		}else{
			if(stk.empty())
				done = true;
			else{
				current = stk.top();
				stk.pop();
				if(current->left)
					out << "|"<< std::setw(4)<<current->left->key ;
				else 
					out <<"|" <<  std::setw(4) <<"-";

				out <<  std::setw(5) <<"| " <<\
				std::setw(2) <<current->key <<" - " << std::setw(2)<<\
				height(current->left)- height(current->right);

				if(current->right)
					out << std::setw(3) <<"|" << \
					std::setw(3)<<current->right->key << \
					std::setw(4) << "|\n";
				else 
					out << std::setw(3) <<"|"<< \
					std::setw(3)<<"-" <<std::setw(4) <<"|\n";
				current = current->right;
			}	
		}
	}
	out<< "\n" ;
}
