#include <stack>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
struct node{
	
	int key;
	int height;
	node *left;
	node *right;
	node(int k);
};
using namespace std;
int height(node *p);
int get_balance(node* p);
void fixheight(node* p);
void print(node *p);
node* rotateright(node* p);
node* balance(node* p);
node *insert(node *p, int k);
node *findmin(node *p);
node *remove(node *p, int k);
node *find (node *p, int k);
void print(node *p, string filename);
node *read();
