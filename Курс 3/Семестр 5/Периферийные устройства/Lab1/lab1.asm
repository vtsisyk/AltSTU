stdin     equ 0
sys_exit  equ 1
stdout    equ 1
sys_read  equ 3
stderr    equ 3
sys_write equ 4


section .bss
  m      resb 10
  n      resb 10
  numLen equ $-m
  array  resb 10
section .data
  msg1:    db 'Введите размеры матрицы: '
  msg1Len: equ $-msg1

  ArrayID     dd    10,3
   arraylen equ ($ - ArrayID) / 4 + 1

section .text
  global _start

_start:

  ; выводим приглашающее сообщение
  mov  ecx, msg1
  mov  edx, msg1Len
  call DisplayText
  
  ; вводим размер первой матрицы
  mov  ecx, m
  mov  edx, numLen
  call ReadText


  mov  edx, arraylen
  mov  ecx, dword ArrayID
  call DisplayText

  
  ; теперь в esi "указатель" на начало массива
  mov  esi, ArrayID
  
  ; читаем число
  mov  ecx, esi
  mov  edx, numLen
  call ReadText

  ; смещаемся в массиве
  ; 4 потому что dword
  add esi, 4

  ; читаем число 
  mov  ecx, esi
  mov  edx, numLen
  call ReadText

  ; выводим раз
  mov  edx, arraylen
  mov  ecx, dword ArrayID
  call DisplayText
  
  ; выводим два
  mov  edx, arraylen
  mov  ecx, dword esi
  call DisplayText

  mov  eax, 1
  mov  ebx, 0
  int  80h

; считываем тут
ReadText:
  mov ebx, stdin
  mov eax, sys_read
  int 80h
ret

; выводим тут 
DisplayText:
  mov eax, sys_write
  mov ebx, stdout
  int 80h
ret
